<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('package_id')->default(0)->unsigned();
            $table->foreign('package_id')->references('id')->on('user_packages')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('promo_code_id')->default(0)->unsigned();
            $table->foreign('promo_code_id')->references('id')->on('promo_codes')->onDelete('cascade')->onUpdate('cascade');

            $table->integer('schedule_id')->unsigned();
            $table->foreign('schedule_id')->references('id')->on('schedules')->onDelete('cascade')->onUpdate('cascade');
            
            $table->integer('rate')->default(0);
            $table->text('review');
            $table->integer('status')->default(0);
            $table->text('doctor_notes');
            $table->text('doctor_report_problem');
            
            $table->string('kid_name')->default('');
            $table->date('kid_birthdate');
            $table->string('kid_gender')->default('male');
            $table->string('complain')->default('');
            $table->text('patient_notes');
            $table->string('duration_call')->default('');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
