<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('');
            $table->string('email')->default('');
            $table->string('password')->default('');
            $table->string('image')->default('');
            #default 0 is patient 1 is doctor 2 is admin
            $table->integer('type')->default(0);
            $table->integer('is_block')->default(0);

            $table->string('platform')->default("");
            $table->string('version')->default("");

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
