@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i>{{ $user->name }}'s Previous Calls
        </h1>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($orders->count())
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>USER_ID</th>
                            <th>RATE</th>
                            <th>STATUS</th>
                            <th>PACKAGE_ID</th>
                            <th>KID_NAME</th>
                            <th>KID_BIRTHDATE</th>
                            <th>COMPLAIN</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->user_id}}</td>
                                <td>{{$order->rate}}</td>
                    
                                <?php 
                                    if ($order->status == 0) {
                                       echo "<td> new request </td>";
                                    }elseif ($order->status == 1) {
                                        echo "<td> request accepted </td>";
                                    }elseif ($order->status == 4 OR $order->status == 6) {
                                        echo "<td> request canceled </td>";
                                    }elseif ($order->status == 3) {
                                        echo "<td> call ended </td>";
                                    }elseif ($order->status == 2) {
                                        echo "<td> call not respond </td>";
                                    }elseif ($order->status == 5) {
                                        echo "<td> on call </td>";
                                    }
                                ?>
                                <td>{{$order->package_id}}</td>
                                <td>{{$order->kid_name}}</td>
                                <td>{{$order->kid_birthdate}}</td>                   
                                <td>{{$order->complain}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ URL('previousCallsDetails', $order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection