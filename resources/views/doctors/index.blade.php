@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Doctors
            <a class="btn btn-success pull-right" href="{{ route('doctors.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection
<?php use App\Http\Controllers\DoctorController; ?>
@section('content')

    @if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            @if($doctors->count())
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Mobile</th>
                            <th>Password</th>
                            <th>Title</th>
                            <th>Num Of Calls</th>
                            <th>Rate</th>
                            <th>Calls Credit</th>
                            <th>Shift Credit</th>
                            <th>Block</th>
                            <th>Created_at</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($doctors as $doctor)
                            <tr>
                                <td>{{$doctor->info->id}}</td>
                                <td>{{$doctor->info->name}}</td>
                                <td>{{$doctor->info->email}}</td>
                                <td>{{$doctor->info->mobile}}</td>

                                <td>{{$doctor->password}}</td>
                                <td>{{$doctor->title}}</td>
                                <td>{{$doctor->CreditDoctor["calls_credit"]/2}}</td>
                                <td><?php  (new DoctorController)->SomeDocDetails($doctor->id); ?></td>
                                <td>{{$doctor->CreditDoctor["calls_credit"]}}</td>
                                <td>{{$doctor->shifts * 10}}</td>
                                <td><h4 id="user_{{$doctor->info->id}}" onclick="AjaxBlock('{{$doctor->info->id}}')">
                                    <?php if($doctor->info->is_block == 0){echo '<i class="fa fa-unlock-alt" aria-hidden="true"></i>';}else{echo'<i class="fa fa-lock" aria-hidden="true"></i>';} ?>
                                </h4></td>
                                 <td>{{$doctor->created_at}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('doctors.show', $doctor->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a><br><br>
                                    <a class="btn btn-xs btn-warning" href="{{ route('doctors.edit', $doctor->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a><br><br>
                                    <form action="{{ route('doctors.destroy', $doctor->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

<script>
function AjaxBlock(id){

    $.get(URL+"/AjaxBlock/"+id, function(data, status){
        document.getElementById("user_"+id).innerHTML = data;
    });
}
</script>
@endsection
<!-- ======================================================================== -->
