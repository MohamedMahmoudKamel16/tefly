@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Doctors / Edit #{{$doctor->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('doctors.update', $doctor->info->id) }}" method="POST"  enctype='multipart/form-data' >
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $doctor->info->name : old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('email')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="email-field">E-mail</label>
                    <input type="text" id="email-field" name="email" class="form-control" value="{{ is_null(old("email")) ? $doctor->info->email : old("email") }}"/>
                       @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first("email") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('mobile')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="mobile-field">mobile</label>
                    <input type="text" id="mobile-field" name="mobile" class="form-control" value="{{ is_null(old("mobile")) ? $doctor->info->mobile : old("mobile") }}"/>
                       @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('password')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="password-field">Password</label>
                    <input type="text" id="password-field" name="password" class="form-control" value="{{ is_null(old("password")) ? $doctor->password : old("password") }}"/>
                       @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first("password") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ is_null(old("title")) ? $doctor->title : old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                 
                    <div class="form-group @if($errors->has('bio')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="bio-field">Bio</label>
                    <textarea class="form-control" id="bio-field" rows="4" name="bio">{{ is_null(old("bio")) ? $doctor->bio : old("bio") }}</textarea>
                       @if($errors->has("bio"))
                        <span class="help-block">{{ $errors->first("bio") }}</span>
                       @endif
                    </div>

                       <div  class="form-group @if($errors->has('clinic_address')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="clinic_address-field">Clinic Address</label>
                    <input type="text" id="clinic_address-field" name="clinic_address" class="form-control" value="{{ is_null(old("clinic_address")) ? $doctor->clinic_address : old("clinic_address") }}"/>
                       @if($errors->has("clinic_address"))
                        <span class="help-block">{{ $errors->first("clinic_address") }}</span>
                       @endif
                    </div>
            

                <div  class="form-group @if($errors->has('clinic_phone')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="clinic_phone-field">Clinic Phone</label>
                    <input type="text" id="clinic_phone-field" name="clinic_phone" class="form-control" value="{{ is_null(old("clinic_phone")) ? $doctor->clinic_phone : old("clinic_phone") }}"/>
                       @if($errors->has("clinic_phone"))
                        <span class="help-block">{{ $errors->first("clinic_phone") }}</span>
                       @endif
                </div>


                <div  class="form-group @if($errors->has('contact_phone')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="contact_phone-field">contact phone</label>
                    <input type="text" id="clinic_phone-field" name="contact_phone" class="form-control" value="{{ is_null(old("contact_phone")) ? $doctor->contact_phone : old("contact_phone") }}"/>
                       @if($errors->has("contact_phone"))
                        <span class="help-block">{{ $errors->first("contact_phone") }}</span>
                       @endif
                </div>

            
                <div  class="form-group @if($errors->has('website_link')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="website_link-field">Website Link</label>
                    <input type="text" id="website_link-field" name="website_link" class="form-control" value="{{ is_null(old("website_link")) ? $doctor->website_link : old("website_link") }}"/>
                       @if($errors->has("website_link"))
                        <span class="help-block">{{ $errors->first("website_link") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('image')) has-error @endif">
                       <label   class="col-sm-2 control-label"  for="image-field">Image</label>
                    <input type="file" accept="image/*" id="image-field" name="image" class="form-control" value="{{ old("image") }}" />
                       @if($errors->has("image"))
                        <span class="help-block">{{ $errors->first("image") }}</span>
                       @endif
                       @if($doctor->info->image !="")
                       <IMG class="img-responsive center-block" style="margin-top: 10px;" width="450" height="260" id="myImg" src="{{$doctor->info->image}}" alt="{{$doctor->info->name}}" />
                       @else
                       <IMG class="img-responsive center-block" style="margin-top: 10px;" width="450" height="260" id="myImg" src="#" alt="your image" style="display: none;" />
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('doctors.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script>
   // $('#myImg').hide();
    $(function () {
        $(":file").change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
            }
        });
    });

  function imageIsLoaded(e) {

    $('#myImg').attr('src', e.target.result);
   // $('#myImg').show();

  };
  </script>
@endsection
