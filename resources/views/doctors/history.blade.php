@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h3>Doctor #{{$user->name}}</h3>
        <?php $credit_calls_value = 0; ?> 
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($doctors->count())
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Week Start</th>
                            <th>Week End</th>
                            <th>Shifts Credit</th>
                            <th>Calls Credit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($credit_shifts as $credit_shift)
                            <tr>
                                <th>{{@$credit_shift->startweekDate}}</th>
                                <th>{{@$credit_shift->weekDate}}</th>
                                <th>{{@$credit_shift->count_shifts * 10}}</th>
                                
                                @foreach ($credit_calls as $credit_call)
                                    @if ($credit_shift->weekDate == $credit_call->weekDate)
                                       <?php $credit_calls_value = $credit_call->calls_credit;break; ?> 
                                    @else
                                       <?php $credit_calls_value = 0; ?> 
                                    @endif
                                @endforeach
                                <th><?php echo $credit_calls_value; ?> </th> 
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>
@endsection




