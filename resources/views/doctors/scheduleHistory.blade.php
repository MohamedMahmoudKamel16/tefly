@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h3>
            @if(!empty($From))
                <center>From : {{ $From }} / To : {{ $To }}</center>
            @endif  
            <i class="glyphicon glyphicon-align-justify"></i> {{ $user->name }} Schedule
            <div class="dropdown">
                <button class="dropbtn">Weeks History</button>
                <div class="dropdown-content scrollable-menu">
                    @foreach($NewWeeks as $week)
                        <center><a href="{{ url('showScheduleHistory/'.$user->id.'/'.$week) }}">{{$week}}</a></center>
                    @endforeach     
                </div>
            </div>
        </h3>

    </div>
@endsection

@section('content')
<!-- ===================================================================================================================================================== -->
<div class="row">
    <div class="col-md-12">
            <table  class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Day</th>
                        <th>Durations</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($FinalResults as $FinalResult)

                    <tr> 
                        <td>{{ $FinalResult["day"] }}</td>
                        <td>
                            @foreach ( $FinalResult["durations"] as $Durations )

                                <p>{{ $Durations["start_time"] }} - {{ $Durations["end_time"] }}</p>
                            
                            @endforeach 
                        </td>
                    </tr>
                    
                @endforeach
                </tbody>
            </table>
        <div class="form-group col-md-12">
          <a class="btn btn-link" href="{{ route('doctors.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
        </div>

    </div>
</div>
@endsection


