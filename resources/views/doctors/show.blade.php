<?php use App\OrderDoctor; ?>
@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h3>Doctor #{{$user->name}}</h3>


        <form action="{{ route('doctors.destroy', $doctor->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('doctors.edit', $doctor->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
       
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
             <div class="form-group col-md-4">
                 <img  width="200" style="display: block;" height="200" src="{{$user->image}}"/>
                 </div>

                <div class="form-group col-md-4">
                     <p class="form-control-static"><a class="btn btn-xs btn-primary" href="{{ url('totalCreditHistory', $user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> credit History</a></p>
                </div>

                <div class="form-group col-md-4">
                     <p class="form-control-static"><a class="btn btn-xs btn-primary" href="{{ url('showScheduleHistory/'.$user->id.'/0') }}"><i class="glyphicon glyphicon-eye-open"></i> Schedule History</a></p>
                </div>

                <div class="form-group col-md-4">
                     <p class="form-control-static"><a class="btn btn-xs btn-primary" href="{{ url('previousCalls', $user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> Previous Calls</a></p>
                </div>

                <div class="form-group col-md-4">
                     <label class="control-label" for="first_name">NAME</label>
                     <p class="form-control-static">{{$user->name}}</p>
                </div>
                <div class="form-group col-md-4">
                     <label class="control-label"  for="email">E-mail</label>
                     <p class="form-control-static">{{$user->email}}</p>
                </div>

                <div class="form-group col-md-4">
                     <label class="control-label"  for="mobile">Doctor Rate</label>
                     <p class="form-control-static">{{ round (@$DoctorRate) }}</p>
                </div>

                <div class="form-group col-md-4">
                     <label class="control-label"  for="mobile">Calls Credit</label>
                     <p class="form-control-static">{{$doctor->CreditDoctor["calls_credit"]}}</p>
                </div>


                <div class="form-group col-md-4">
                     <label class="control-label"  for="mobile">num of calls</label>
                     <p class="form-control-static">{{$doctor->CreditDoctor["calls_credit"]/2}}</p>
                </div>

                <div class="form-group col-md-4">
                     <label class="control-label"  for="mobile">Total calls credit</label>
                     <p class="form-control-static">{{$TotalCallsCredit}}</p>
                </div>


                <div class="form-group col-md-4">
                     <label class="control-label"  for="mobile">Shift Credit</label>
                     <p class="form-control-static">{{$doctor->shifts * 10}}</p>
                </div>


                <div class="form-group col-md-4">
                     <label class="control-label"  for="mobile">Total Shift Credit</label>
                     <p class="form-control-static">{{ ($doctor->shifts * 10) + ($ScheduleDoctorsHistoryCredit) }}</p>
                </div>


                <div class="form-group col-md-4">
                     <label class="control-label"  for="mobile">Mobile</label>
                     <p class="form-control-static">{{$user->mobile}}</p>
                </div>
                   
                    <div class="form-group col-md-4">
                     <label class="control-label"  for="password">PASSWORD</label>
                     <p class="form-control-static">{{$doctor->password}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label class="control-label"  for="title">TITLE</label>
                     <p class="form-control-static">{{$doctor->title}}</p>
                </div>
                    
                    <div class="form-group col-md-4">
                     <label class="control-label"  for="bio">Website Link</label>
                     <p class="form-control-static">{{$doctor->website_link}}</p>
                </div>

                <div class="form-group col-md-4">
                     <label class="control-label"  for="clinic_address">Clinic Address</label>
                     <p class="form-control-static">{{$doctor->clinic_address}}</p>
                </div>
                <div class="form-group col-md-4">
                     <label class="control-label"  for="clinic_phone">Clinc Phone</label>
                     <p class="form-control-static">{{$doctor->clinic_phone}}</p>
                </div>

                <div class="form-group col-md-4">
                     <label class="control-label"  for="contact_phone">contact phone</label>
                     <p class="form-control-static">{{$doctor->contact_phone}}</p>
                </div>

                 <div class="form-group col-md-4">
                     <label class="control-label"  for="bio">Requests not accepted on his shift</label>
                     <p class="form-control-static">{{ OrderDoctor::where('user_id' , $user->id)->where('onShift',1)->where('status' , 10)->count() }}</p>
                </div>


                 <div class="form-group col-md-4">
                     <label class="control-label"  for="bio">Finished Calls</label>
                     <p class="form-control-static">{{ OrderDoctor::where('user_id' , $user->id)->where('status' , 3)->count() }}</p>
                </div>


                 <div class="form-group col-md-4">
                     <label class="control-label"  for="bio">No respond calls</label>
                     <p class="form-control-static">{{ OrderDoctor::where('user_id' , $user->id)->where('status' , 2)->count() }}</p>
                </div>


                <div class="form-group col-md-4">
                     <label class="control-label"  for="bio">cancelled by patient calls</label>
                     <p class="form-control-static">{{ OrderDoctor::where('user_id' , $user->id)->where('status' , 4)->count() }}</p>
                </div>


                 <div class="form-group col-md-4">
                     <label class="control-label"  for="bio">BIO</label>
                     <p class="form-control-static">{{$doctor->bio}}</p>
                </div>


                 <div class="form-group col-md-4">
                     <label class="control-label"  for="bio">Created at</label>
                     <p class="form-control-static">{{$doctor->created_at}}</p>
                </div>

            </form>
        </div>
    </div>
<!-- ===================================================================================================================================================== -->
<div class="row">
    <div class="col-md-12">
        <h3  class="page-header">Doctor Schedule</h3>
            <table  class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Day</th>
                        <th>Durations</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($FinalResults as $FinalResult)

                    <tr> 
                        <td>{{ $FinalResult["day"] }}</td>
                        <td>
                            @foreach ( $FinalResult["durations"] as $Durations )

                                <p>{{ $Durations["start_time"] }} - {{ $Durations["end_time"] }}</p>
                            
                            @endforeach 
                        </td>
                    </tr>
                    
                @endforeach
                </tbody>
            </table>
        <div class="form-group col-md-12">
          <a class="btn btn-link" href="{{ route('doctors.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
        </div>

    </div>
</div>
@endsection