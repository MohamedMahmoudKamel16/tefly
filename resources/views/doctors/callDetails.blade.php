@extends('layouts.admin')
@section('header')
    <div class="page-header">
            <H1> Order #{{$order->id}} Details</H1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <form action="#">
                <div class="form-group col-md-4">
                    <label for="nome">ID</label>
                    <p class="form-control-static">{{$order->id}}</p>
                </div>
                <div class="form-group col-md-4">
                     <label for="user_id">USER_ID</label>
                     <p class="form-control-static">{{$order->user_id}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="rate">RATE</label>
                     <p class="form-control-static">{{$order->rate}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="review">REVIEW</label>
                     <p class="form-control-static">{{$order->review}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$order->status}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="doctor_notes">DOCTOR_NOTES</label>
                     <p class="form-control-static">{{$order->doctor_notes}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="doctor_report_problem">DOCTOR_REPORT_PROBLEM</label>
                     <p class="form-control-static">{{$order->doctor_report_problem}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="package_id">PACKAGE_ID</label>
                     <p class="form-control-static">{{$order->package_id}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="kid_name">KID_NAME</label>
                     <p class="form-control-static">{{$order->kid_name}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="kid_birthdate">KID_BIRTHDATE</label>
                     <p class="form-control-static">{{$order->kid_birthdate}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="kid_gender">KID_GENDER</label>
                     <p class="form-control-static">{{$order->kid_gender}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="complain">COMPLAIN</label>
                     <p class="form-control-static">{{$order->complain}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="patient_notes">PATIENT_NOTES</label>
                     <p class="form-control-static">{{$order->patient_notes}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="duration_call">DURATION_CALL</label>
                     <p class="form-control-static">{{$order->duration_call}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('doctors.index') }}"><i class="glyphicon glyphicon-backward"></i>Back</a>

        </div>
    </div>

@endsection