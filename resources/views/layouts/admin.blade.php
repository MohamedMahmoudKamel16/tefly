<?php 
use App\Order;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title>Teflly</title>

<!-- Bootstrap core CSS -->
<link href="{{URL::asset('public/assets/css/bootstrap.min.css')}}" rel="stylesheet">
<!--external css-->
<link href="{{URL::asset('public/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />


<!-- Custom styles for this template -->
<link href="{{URL::asset('public/assets/css/style.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/css/style-responsive.css')}}" rel="stylesheet">

<link type="text/css" href="{{URL::asset('public/assets/css/jquery.dataTables.min.css')}}"/>

<link type="text/css" href="{{URL::asset('public/assets/css/dataTables.bootstrap.min.css')}}"/>


<!-- Custom styles for this template -->
<link href="{{URL::asset('public/assets/css/style.css')}}" rel="stylesheet">
<link href="{{URL::asset('public/assets/css/style-responsive.css')}}" rel="stylesheet">

<link type="text/css" href="{{URL::asset('public/assets/css/jquery.dataTables.min.css')}}"/>

<link type="text/css" href="{{URL::asset('public/assets/css/dataTables.bootstrap.min.css')}}"/>

<style type="text/css">
  table.dataTable thead .sorting_asc {
  background: url("http://cdn.datatables.net/1.10.0/images/sort_asc.png") no-repeat center right;
  }
  table.dataTable thead .sorting_desc {
  background: url("http://cdn.datatables.net/1.10.0/images/sort_desc.png") no-repeat center right;
  }
  table.dataTable thead .sorting {
  background: url("http://cdn.datatables.net/1.10.0/images/sort_both.png") no-repeat center right;
  }

.btn-primary {

    color: #fff;
    background-color: #aecb06;
    border-color: #aecb06;
}

.btn-warning {
    color: #fff;
    background-color: #009be2;
    border-color: #009be2;
}
@font-face {
    font-family: Cairo;
    src: url(Cairo-Regular.ttf);
}

</style>

  </head>

  <body>

    <section id="container" >
      <!--header start-->
      <header class="header black-bg">
        <div class="sidebar-toggle-box">
          <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="{{URL::asset('public/')}}" class="logo"><b>Teflly ADMIN PANEL</b></a>
        <!--logo end-->
        <div class="top-menu" style="margin-top:15px;">
            <ul class="nav pull-right top-menu">
                    
          			<li> 
          			<a class="top-menu"  href="{{ url('/logout') }}"
          			    onclick="event.preventDefault();
          			             document.getElementById('logout-form').submit();">
          			    Logout
          			</a>
          			
          			<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;" >
          			    {{ csrf_field() }}
          			</form>
          			</li>
      		</ul>
      	</div>
      </header>
      <!--header end-->

      <!--sidebar start-->
      <aside>
        <div id="sidebar"  class="nav-collapse ">
          <!-- sidebar menu start-->
          <ul class="sidebar-menu" id="nav-accordion">

            <p class="centered"><a href=' {{ URL("/AdminDashboard")  }}'><img src="{{URL::asset('public/images/logo.png')}}" class="img-circle" width="60"></a></p>
            <h5 class="centered">Teflly</h5>

            <li class="mt"  hidden>
              <a id="dash" class="item-nav" href="{{URL::asset('/')}}" >
                <i class="fa fa-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>

            <li class="{{ Request::is('doctors') ? 'class="active"' : 'menu' }}" >
              <a class="item-nav"   href="{{URL::asset('doctors')}}">
                <!-- <i class="fa fa-stethoscope"></i> -->
                <img src="{{URL::asset('public/images/stethoscope.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px" >Doctors</span>
              </a>
              
            </li>

             <li class="sub-menu">
              <a class="item-nav"  href="{{URL::asset('patients')}}">
                <!-- <i class="fa fa-child"></i> -->
                <img src="{{URL::asset('public/images/mother-with-son.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Patient</span>
              </a>
              
            </li>

            <li class="sub-menu">
              <a class="item-nav"  href="{{URL::asset('orders')}}">
                <!-- <i class="fa fa-reorder"></i> -->
                <img src="{{URL::asset('public/images/requests.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Requests ( <?php echo '<span style = "color:#009ee3; font-size:16px;" >'.Order::where('status' , 0 )->count()."</span>"; ?> )</span>
              </a>
              
            </li>

            <li class="sub-menu">
              <a class="item-nav"  href="{{URL::asset('packages')}}">
               <!--  <i class="fa fa-heart-o"></i> -->
               <img src="{{URL::asset('public/images/wallet.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Packages</span>
              </a>
              
            </li>



             <li class="menu" >
              <a href="{{URL::asset('schedules')}}">
                <!-- <i class="fa fa-meh-o" aria-hidden="true"></i> -->
                <img src="{{URL::asset('public/images/schedule.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Schedules</span>
              </a>
            </li>

             <li class="{{ Request::is('promo_codes') ? 'class="active"' : 'menu' }}" >
              <a class="item-nav"   href="{{URL::asset('promo_codes')}}">
                <!-- <i class="fa fa-tag"></i> -->
                <img src="{{URL::asset('public/images/promo.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Promo Code</span>
              </a>
              
            </li>


             <li class="{{ Request::is('CountryCity') ? 'class="active"' : 'menu' }}" >
              <a class="item-nav"   href="{{URL::asset('CountryCity')}}">
                <!-- <i class="fa fa-tag"></i> -->
                <img src="{{URL::asset('public/images/cities.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Cities</span>
              </a>
            </li>

            <li class="menu">
              <a href="{{URL::asset('notifications/create')}}" >
                <!-- <i class="fa fa-bell-o"></i> -->
                <img src="{{URL::asset('public/images/notification.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Notifications</span>
              </a>
          
            </li>
            <li class="menu">
      		        <a  href="{{URL::asset('feedback')}}">
                <!-- <i class="fa fa-comment-o"></i> -->
                <img src="{{URL::asset('public/images/feedback.png')}}" >
                <span style="font-family: Cairo; font-size: 12pt; margin-left: 10px">Feedbacks </span>
              </a>

            </li>

           
          </ul>
          <!-- sidebar menu end-->
        </div>
      </aside>
      <!--sidebar end-->

      <!--main content start-->
      <section id="main-content" >
          <section class="wrapper main-chart site-min-height" >
                <div class="panel-body">
                  <div class="dataTable_wrapper">
              <div id="content"></div>
            <div class="loading">
              @yield('header')
             @yield('content')


            </div>

                  </div>
                  </div>

        </section><!-- /wrapper  -->
      </section><!-- /MAIN CONTENT -->

  <!--main content end-->
  <!--footer start-->
  <footer class="site-footer">
    <div class="text-center">
      2017 - Teflly
      <a href="" class="go-top">
        <i class="fa fa-angle-up"></i>
      </a>
    </div>
  </footer>
  <!--footer end-->
</section>
<!-- js placed at the end of the document so the pages load faster -->
<script src="{{URL::asset('public/assets/js/jquery.js')}}"></script>
<script src="{{URL::asset('public/assets/js/bootstrap.min.js')}}"></script>
<script class="include" type="text/javascript" src="{{URL::asset('public/assets/js/jquery.dcjqaccordion.2.7.js')}}"></script>
<script src="{{URL::asset('public/assets/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{URL::asset('public/assets/js/jquery.nicescroll.js')}}" type="text/javascript"></script>


<!--custom switch-->
<script src="{{URL::asset('public/assets/js/bootstrap-switch.js')}}"></script>

<script src="{{URL::asset('public/assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/js/dataTables.bootstrap.min.js')}}"></script>

<!--custom switch-->
<script src="{{URL::asset('public/assets/js/bootstrap-switch.js')}}"></script>
<script src="{{URL::asset('public/assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('public/assets/jquery/dist/jquery.min.js')}}"></script>
<script src="{{URL::asset('public/assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('public/assets/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::asset('public/assets/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{URL::asset('public/assets/build/js/custom.min.js')}}"></script>


<script type="text/javascript">
$(document).ready(function() {

    $('#datatable').DataTable( {
        "destroy": true,
        "paging":   true,
        "info":     true,
        "aaSorting": []
    });

});
</script>

<script type="text/javascript">
  
  var URL = "{{ url('/')}}";

</script>

</body>
</html>
@yield('scripts')