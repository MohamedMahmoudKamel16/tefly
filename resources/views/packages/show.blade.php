@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h3>Packages / Show #{{$package->id}}</h3>
        <form action="{{ route('packages.destroy', $package->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('packages.edit', $package->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
               
                <div class="form-group">
                     <label class="col-sm-2 control-label" for="name">NAME</label>
                     <p class="form-control-static">{{$package->name}}</p>
                </div>
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="type">TYPE</label>
                     <p class="form-control-static">{{$package->type}}</p>
                </div>
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="price">PRICE</label>
                     <p class="form-control-static">{{$package->price}}</p>
                </div>
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="no_calls">NO_CALLS</label>
                     <p class="form-control-static">{{$package->no_calls}}</p>
                </div>
                    <div class="form-group">
                     <label class="col-sm-2 control-label" for="duration">DURATION</label>
                     <p class="form-control-static">{{$package->duration}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('packages.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection