@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Packages
        </h3>

    </div>
@endsection

@section('content')
@if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            @if($packages->count())
                <table  class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>NAME</th>
                        <th>TYPE</th>
                        <th>PRICE</th>
                        <th>NO_CALLS</th>
                        <th>DURATION</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($packages as $package)
                            <tr>
                                <td>{{$package->id}}</td>
                                <td>{{$package->name}}</td>
                    <td>{{$package->type}}</td>
                    <td>{{$package->price}}</td>
                    <td>{{$package->no_calls}}</td>
                    <td>{{$package->duration}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('packages.show', $package->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('packages.edit', $package->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('packages.destroy', $package->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $packages->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection