@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> Packages / Edit #{{$package->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('packages.update', $package->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label class="col-sm-2 control-label" for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $package->name : old("name") }}"/>
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('type')) has-error @endif">
                       <label class="col-sm-2 control-label" for="type-field">Type</label>
                    <input type="text" id="type-field" name="type" class="form-control" value="{{ is_null(old("type")) ? $package->type : old("type") }}"/>
                       @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('price')) has-error @endif">
                       <label class="col-sm-2 control-label" for="price-field">Price</label>
                    <input type="text" id="price-field" name="price" class="form-control" value="{{ is_null(old("price")) ? $package->price : old("price") }}"/>
                       @if($errors->has("price"))
                        <span class="help-block">{{ $errors->first("price") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('no_calls')) has-error @endif">
                       <label class="col-sm-2 control-label" for="no_calls-field">No_calls</label>
                    <input type="text" id="no_calls-field" name="no_calls" class="form-control" value="{{ is_null(old("no_calls")) ? $package->no_calls : old("no_calls") }}"/>
                       @if($errors->has("no_calls"))
                        <span class="help-block">{{ $errors->first("no_calls") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('duration')) has-error @endif">
                       <label class="col-sm-2 control-label" for="duration-field">Duration</label>
                    <input type="text" id="duration-field" name="duration" class="form-control" value="{{ is_null(old("duration")) ? $package->duration : old("duration") }}"/>
                       @if($errors->has("duration"))
                        <span class="help-block">{{ $errors->first("duration") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('packages.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
