<html>

	<head>
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	</head>

	<body class="container">
		<h4 class="text-center"> Payment Process </h4>
		<form method="POST" action={{ url('create_payment') }}>

			{{ csrf_field() }}
			
			<input type="hidden" name="mobile_number" value="{{ $mobile_number }}" />
			<input type="hidden" name="productId" value={{ $type == 1 ? '1_call' : '10_calls' }} />
			<div class="form-group">
				<label> Service Provider: </label>
				<select name="operatorCode" class="form-control">
					<option value="60201"> Orange </option>
					<option value="60202"> Vodafone </option>
					<option value="60203"> Etisalat </option>
				</select>
			
			<br/><br/>
			<div class="form-group">
				<label> Mobile Number: </label>
				<input type="text" name="msisdn" value="{{ $mobile_number }}" class="form-control" />
			</div>
			<br/><br/>

			<input type="submit" value="Submit" class="btn btn-success" />
		</form>

	</body>

</html>
