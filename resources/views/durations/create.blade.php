@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Durations / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('durations.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('start_time')) has-error @endif">
                       <label for="start_time-field">Start_time</label>
                    <input type="time" id="start_time-field" name="start_time" class="form-control" value="{{ old("start_time") }}"/>
                       @if($errors->has("start_time"))
                        <span class="help-block">{{ $errors->first("start_time") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('end_time')) has-error @endif">
                       <label for="end_time-field">End_time</label>
                    <input type="time" id="end_time-field" name="end_time" class="form-control" value="{{ old("end_time") }}"/>
                       @if($errors->has("end_time"))
                        <span class="help-block">{{ $errors->first("end_time") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('durations.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
