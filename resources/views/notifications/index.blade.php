@extends('layouts.admin')

@section('content')
  <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> Notifications from Users
        </h3>

    </div>
    <div class="row">
        <div class="col-md-12">
            @if($notificationss->count())
            <div>
                @foreach($notificationss as $notification)
                   
                    @if($notification->viewed_at == NULL)
                        <p style="color: #2c1c54;font-weight: bold;background-color: #ffc320;font-size: 15px;text-align: center;">
                    @else
                        <p style="font-weight: bold;font-size: 15px;text-align: center;">
                    @endif

                            {{$notification->message}} at {{$notification->created_at}}

                        </p>
                    
                @endforeach
                    

            </div>
               <div class="row">
        <div class="col-md-12">
                {!! $notificationss->render() !!}
                </div>
                </div>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
<script type="text/javascript">
    

     var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];

        $.ajax({
            url: baseUrl+"/read_notification",
            type: 'GET',
            data: {},
            success: function(data) {
          //     console.log(data)         
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });

</script>

@endsection
