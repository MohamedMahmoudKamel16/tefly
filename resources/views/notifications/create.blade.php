@extends('layouts.admin')

@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i>Send  Notification </h3>
    </div>
@endsection

@section('content')
    @include('error')
     <div>
             @if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
          </div>

    <div class="row">
        <div class="col-md-12">
<form action="{{ URL::asset('notifications/store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('title')) has-error @endif">
                       <label class="control-label col-sm-2"  for="title-field">Title</label>
                    <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}"/>
                       @if($errors->has("title"))
                        <span class="help-block">{{ $errors->first("title") }}</span>
                       @endif
                    </div>
                    
                    <div class="form-group @if($errors->has('type')) has-error @endif">
                    <label class="control-label col-sm-2"  for="type-field">User/General</label>
                    <select id="type-field" name="type" class="form-control" >
                      <option {{ old("type") == 0 ? 'selected' : '' }} value="0">general</option>
                      <option  {{ old("type") == 1 ? 'selected' : '' }} value="1">user</option>
                      <option  {{ old("type") == 2 ? 'selected' : '' }} value="3">android users</option>
                      <option  {{ old("type") == 3 ? 'selected' : '' }} value="4">ios users</option>
                    
                    </select>
                       @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                       @endif
     
                    </div>

                    
                     <div  {{ old("type") == 1 ? 'hidden' : '' }}  id="id" class="form-group @if($errors->has('user_id')) has-error @endif">
                       <label class="control-label col-sm-2"  for="title-field">User Mobile</label>
                    <input list="searchResult" type="text" id="user_id-field" name="user_id" class="form-control" value="{{ old("user_id") }}"/>
                    <datalist id="searchResult">
                    
                </datalist>
                       @if($errors->has("user_id"))
                        <span class="help-block">{{ $errors->first("user_id") }}</span>
                       @endif
                    </div>

                    <div class="form-group @if($errors->has('userType')) has-error @endif">
                    <label class="control-label col-sm-2"  for="type-field">User Type</label>
                    <select id="mySelect" onchange="myFunction()" name="userType" class="form-control" >
                      <option value="0">Patient</option>
                      <option value="1">Doctor</option>
                    </select>
                       @if($errors->has("userType"))
                        <span class="help-block">{{ $errors->first("userType") }}</span>
                       @endif
                    </div>


                    <div class="form-group @if($errors->has('type_not')) has-error @endif">
                    <label class="control-label col-sm-2"  for="type_not-field">Notification Type</label>
                    <select id="type_not-field" name="type_not" class="form-control" >

                      <option  value="1">general</option>

                          <!-- doctor notifications -->
                          <option id="d1" value="2" style="display: none;">new request</option>
                          <option id="d2" value="4" style="display: none;">patient cancel order</option>
                          <option id="d3" value="3" style="display: none;">schedule</option>

                          <!-- patient notifications -->
                          <option id="p1" value="4">Doctor not respond</option>
                          <option id="p2" value="3">previous calls</option>
                          <option id="p3" value="2">user account</option>

                    </select>
                       @if($errors->has("type_not"))
                        <span class="help-block">{{ $errors->first("type_not") }}</span>
                       @endif
                       
                       
                    </div>
                    
                    <div class="form-group @if($errors->has('body')) has-error @endif">
                       <label class="control-label col-sm-2" for="body-field">Body</label>
                    <textarea class="form-control" id="body-field" rows="4" name="body">{{ old("body") }}</textarea>
                       @if($errors->has("body"))
                        <span class="help-block">{{ $errors->first("body") }}</span>
                       @endif
                    </div>
                <div class="">
                    <button type="submit" class="btn btn-primary">Create</button>
                    
                </div>
            </form>
   </div>
    </div>
@endsection

@section('scripts')
<script>

function myFunction() {

    var x = document.getElementById("mySelect").value;

    if (x == 0) { /* patient */

      document.getElementById("p1").style.display = "block";
      document.getElementById("p2").style.display = "block";
      document.getElementById("p3").style.display = "block";

      document.getElementById("d1").style.display = "none";
      document.getElementById("d2").style.display = "none";
      document.getElementById("d3").style.display = "none";


     }else{

      document.getElementById("d1").style.display = "block";
      document.getElementById("d2").style.display = "block";
      document.getElementById("d3").style.display = "block";

      document.getElementById("p1").style.display = "none";
      document.getElementById("p2").style.display = "none";
      document.getElementById("p3").style.display = "none";

     }

}

</script>


<script>
  
  $(function(){
    if($("#type-field").val() == "1"){
        $("#id").show();
      
      }else{
        $("#id").hide();
      
      }
    $("#type-field").on("change",function(){
    
      if($("#type-field").val() == "1"){
        $("#id").show();
      
      }else{
        $("#id").hide();
      
      }
  
    });
  $("#user_id-field").keyup(function(e) {
        e.preventDefault();
        var serchKy = $('#user_id-field').val();
        var getUrl = window.location;
  var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
  if(serchKy != ""){

     $.ajax({
        //    url: baseUrl+"/searchCustomer/"+$('#user_id-field').val(),
            url: URL+"/searchCustomer/"+$('#user_id-field').val(),
            type: 'GET',
            data: {},
            success: function(data) {
            
               var datalist = $("#searchResult");
                var tablSearc = "";
                datalist.empty();

                $.each(data, function(i, content) {
                        document.getElementById('searchResult').innerHTML += "<option value='" + content.mobile + "'>" + content.name +"</option>";

                });
         
            },
            error: function(data) {
                console.log('Error:', data);
            }
        });

  }
       

    });
  
  })
</script>

@endsection