<?php
use App\User;
use App\Package;
use App\PromoCode;
use Carbon\Carbon;
?>
@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">

        <form action="{{ url('filterOrders') }}" method="GET" style="display: inline;">

            <input class="btn-group" type="hidden" name="user_id" value="{{$user}}">
            <input class="btn-group" type="date"   name="created_at" value="">

            <select class="btn-group" name="statusChoice">
                <?php $statuses = [0 => 'All', 10 => 'Not Accepted', 1 => 'Accepted', 4 => 'Cancelled', 2 => 'No Respond', 3 => 'Finished']; ?>
                @foreach($statuses as $key => $value)
                    <option value="{{ $key }}" {{ (isset($status) && $key == $status) ? 'selected' : '' }}> {{ $value }} </option>
                @endforeach
            </select>

            <div class="btn-group" role="group" aria-label="...">
                <button type="submit" class="btn btn-primary ">Filter</button>
            </div>
        </form>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($orders->count())
                <table id="" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <td>Patient Name</td>
                            <th>provisional Diagnosis</th>
                            <th>Doctor Notes</th>
                            <td>Doctors On Shift</td>
                            <td>Doctor Accept</td>
                            <th>RATE</th>
                            <th>STATUS</th>
                            <th>Package Name</th>
                            <th>Promo Code</th>
                            <th>Delivery Status</th>
                            <th>Created_at</th>
                            <th>Accepted_at</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td><a href="{{ route('patients.show', $order->patient_request->id) }}">{{$order->patient_request->name}}</a></td>
                                <td>{{$order->doctor_report_problem}}</td>
                                <td>{{$order->doctor_notes}}</td>
                                <td>

                                @foreach($order->doctors_on_shift as $doctors)

                                        <a href="{{ route('doctors.show', $doctors->id) }}"> <?php echo User::where('id' , $doctors->user_id )->value("name"); ?></a><br>

                                @endforeach
                                </td>

                                <td>
                                @foreach($order->doctor_accept as $accept_doctor)
                                    <a href="{{ route('doctors.show', $accept_doctor->id) }}"> <?php echo User::where('id' , $accept_doctor->user_id )->value("name"); ?></a><br>
                                @endforeach
                                </td>

                                <td>{{$order->rate}}</td>
                                <?php

                                    if ($order->status == 0) {
                                       echo "<td> not accepted </td>";
                                    }elseif ($order->status == 1) {
                                        echo "<td> accepted </td>";
                                    }elseif ($order->status == 4 OR $order->status == 6) {
                                        echo "<td> cancelled </td>";
                                    }elseif ($order->status == 3) {
                                        echo "<td> Finished </td>";
                                    }elseif ($order->status == 2) {
                                        echo "<td> no respond </td>";
                                    }else{
                                        echo "<td>  </td>";
                                    }

                                ?>
                                <td><a  href="{{ route('packages.show', $order->packageType) }}">{{ Package::where('id' , $order->packageType )->value("name") }}</a></td>
                                <td><a  href="{{ route('promo_codes.show', $order->promo_code_id) }}">{{ PromoCode::where('id' , $order->promo_code_id )->value("code") }}</a></td>
                                <td>{{$order->deliveryStatus}}</td>
                                <td>{{$order->created_at}}</td>
                                <td><?php if($order->accepted_at){echo \Carbon\Carbon::parse($order->accepted_at)->format('d F Y h:i A'); } ?></td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('orders.show', $order->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a><br><br>
                                    <a class="btn btn-xs btn-warning" href="{{ route('orders.edit', $order->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a><br><br>
                                    <form action="{{ route('orders.destroy', $order->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
		<div class="text-center">
			{{ $orders->links() }}
		</div>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection
