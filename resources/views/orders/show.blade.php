@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h1>Orders / Show #{{$order->id}}</h1>
        <form action="{{ route('orders.destroy', $order->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('orders.edit', $order->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group col-md-4">
                    <label for="nome">ID</label>
                    <p class="form-control-static">{{$order->id}}</p>
                </div>
                <div class="form-group col-md-4">
                     <label for="user_id">USER_ID</label>
                     <p class="form-control-static">{{$order->user_id}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="rate">RATE</label>
                     <p class="form-control-static">{{$order->rate}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="review">REVIEW</label>
                     <p class="form-control-static">{{$order->review}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">
                        <?php 
                            if ($order->status == 0) {
                               echo "<td> new request </td>";
                            }elseif ($order->status == 1) {
                                echo "<td> request accepted </td>";
                            }elseif ($order->status == 4 OR $order->status == 6) {
                                echo "<td> request canceled </td>";
                            }elseif ($order->status == 3) {
                                echo "<td> call ended </td>";
                            }elseif ($order->status == 2) {
                                echo "<td> call not respond </td>";
                            }elseif ($order->status == 5) {
                                echo "<td> on call </td>";
                            }
                        ?>                         
                     </p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="doctor_notes">DOCTOR_NOTES</label>
                     <p class="form-control-static">{{$order->doctor_notes}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="doctor_report_problem">Provisional Diagnosis</label>
                     <p class="form-control-static">{{$order->doctor_report_problem}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="package_id">PACKAGE_ID</label>
                     <p class="form-control-static">{{$order->package_id}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="kid_name">KID_NAME</label>
                     <p class="form-control-static">{{$order->kid_name}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="kid_birthdate">KID_BIRTHDATE</label>
                     <p class="form-control-static">{{$order->kid_birthdate}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="kid_gender">KID_GENDER</label>
                     <p class="form-control-static">{{$order->kid_gender}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="complain">COMPLAIN</label>
                     <p class="form-control-static">{{$order->complain}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label for="patient_notes">PATIENT_NOTES</label>
                     <p class="form-control-static">{{$order->patient_notes}}</p>
                </div>
                </div>
                    <div class="form-group">
                     <label for="status" class="control-label">Created at</label>
                     <p class="form-control-static">{{$order->created_at}}</p>
                </div>
                </div>
                    <div class="form-group">
                     <label for="status" class="control-label">Finished at</label>
                     <p class="form-control-static">{{$order->finished_at}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('orders.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection