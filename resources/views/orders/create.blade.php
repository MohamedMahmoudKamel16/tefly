@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-plus"></i> Orders / Create </h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('orders.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('user_id')) has-error @endif">
                       <label for="user_id-field">User_id</label>
                    <input type="text" id="user_id-field" name="user_id" class="form-control" value="{{ old("user_id") }}"/>
                       @if($errors->has("user_id"))
                        <span class="help-block">{{ $errors->first("user_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('rate')) has-error @endif">
                       <label for="rate-field">Rate</label>
                    <input type="text" id="rate-field" name="rate" class="form-control" value="{{ old("rate") }}"/>
                       @if($errors->has("rate"))
                        <span class="help-block">{{ $errors->first("rate") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('review')) has-error @endif">
                       <label for="review-field">Review</label>
                    <textarea class="form-control" id="review-field" rows="3" name="review">{{ old("review") }}</textarea>
                       @if($errors->has("review"))
                        <span class="help-block">{{ $errors->first("review") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('status')) has-error @endif">
                       <label for="status-field">Status</label>
                    <input type="text" id="status-field" name="status" class="form-control" value="{{ old("status") }}"/>
                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('doctor_notes')) has-error @endif">
                       <label for="doctor_notes-field">Doctor_notes</label>
                    <textarea class="form-control" id="doctor_notes-field" rows="3" name="doctor_notes">{{ old("doctor_notes") }}</textarea>
                       @if($errors->has("doctor_notes"))
                        <span class="help-block">{{ $errors->first("doctor_notes") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('doctor_report_problem')) has-error @endif">
                       <label for="doctor_report_problem-field">Doctor_report_problem</label>
                    <textarea class="form-control" id="doctor_report_problem-field" rows="3" name="doctor_report_problem">{{ old("doctor_report_problem") }}</textarea>
                       @if($errors->has("doctor_report_problem"))
                        <span class="help-block">{{ $errors->first("doctor_report_problem") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('package_id')) has-error @endif">
                       <label for="package_id-field">Package_id</label>
                    <input type="text" id="package_id-field" name="package_id" class="form-control" value="{{ old("package_id") }}"/>
                       @if($errors->has("package_id"))
                        <span class="help-block">{{ $errors->first("package_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('kid_name')) has-error @endif">
                       <label for="kid_name-field">Kid_name</label>
                    <input type="text" id="kid_name-field" name="kid_name" class="form-control" value="{{ old("kid_name") }}"/>
                       @if($errors->has("kid_name"))
                        <span class="help-block">{{ $errors->first("kid_name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('kid_birthdate')) has-error @endif">
                       <label for="kid_birthdate-field">Kid_birthdate</label>
                    <input type="text" id="kid_birthdate-field" name="kid_birthdate" class="form-control date-picker" value="{{ old("kid_birthdate") }}"/>
                       @if($errors->has("kid_birthdate"))
                        <span class="help-block">{{ $errors->first("kid_birthdate") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('kid_gender')) has-error @endif">
                       <label for="kid_gender-field">Kid_gender</label>
                    <input type="text" id="kid_gender-field" name="kid_gender" class="form-control" value="{{ old("kid_gender") }}"/>
                       @if($errors->has("kid_gender"))
                        <span class="help-block">{{ $errors->first("kid_gender") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('complain')) has-error @endif">
                       <label for="complain-field">Complain</label>
                    <input type="text" id="complain-field" name="complain" class="form-control" value="{{ old("complain") }}"/>
                       @if($errors->has("complain"))
                        <span class="help-block">{{ $errors->first("complain") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('patient_notes')) has-error @endif">
                       <label for="patient_notes-field">Patient_notes</label>
                    <textarea class="form-control" id="patient_notes-field" rows="3" name="patient_notes">{{ old("patient_notes") }}</textarea>
                       @if($errors->has("patient_notes"))
                        <span class="help-block">{{ $errors->first("patient_notes") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('duration_call')) has-error @endif">
                       <label for="duration_call-field">Duration_call</label>
                    <input type="text" id="duration_call-field" name="duration_call" class="form-control" value="{{ old("duration_call") }}"/>
                       @if($errors->has("duration_call"))
                        <span class="help-block">{{ $errors->first("duration_call") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('orders.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
