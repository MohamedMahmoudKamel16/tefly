@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h3>
            <i class="glyphicon glyphicon-align-justify"></i> PromoCodes
            <a class="btn btn-success pull-right" href="{{ route('promo_codes.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h3>

    </div>
@endsection
<?php  use App\UserPackage; ?>
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($promo_codes->count())
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>CODE</th>
                        <th>STATUS</th>
                        <th>TYPE</th>
                        <th>DISCOUNT</th>
                        <th>Use Count</th>
                        <th>Created_at</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($promo_codes as $promo_code)
                            <tr>
                                <td>{{$promo_code->id}}</td>
                                <td>{{$promo_code->code}}</td>
                    <td>@if( $promo_code->status == 0 ) Available @else Not Available @endif</td>
                    <td>@if( $promo_code->type == 0 ) Free @else Discount @endif</td>
                    <td>{{$promo_code->discount}}</td>
                    <td>{{ UserPackage::where('promo_code_id' , $promo_code->id )->count() }}</td>
                     <td>{{$promo_code->created_at}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('promo_codes.show', $promo_code->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('promo_codes.edit', $promo_code->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('promo_codes.destroy', $promo_code->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection