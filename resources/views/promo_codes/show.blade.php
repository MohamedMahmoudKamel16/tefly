@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h3>PromoCodes / Show #{{$promo_code->id}}</h3>
        <form action="{{ route('promo_codes.destroy', $promo_code->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('promo_codes.edit', $promo_code->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="code">CODE</label>
                     <p class="form-control-static">{{$promo_code->code}}</p>
                </div>
                    <div class="form-group">
                     <label for="status">STATUS</label>
                     <p class="form-control-static">{{$promo_code->status}}</p>
                </div>
                </div>
                    <div class="form-group">
                     <label for="status" class="control-label">Created at</label>
                     <p class="form-control-static">{{$promo_code->created_at}}</p>
                </div>

            </form>

            <a class="btn btn-link" href="{{ route('promo_codes.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection