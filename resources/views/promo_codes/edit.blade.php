@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> PromoCodes / Edit #{{$promo_code->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('promo_codes.update', $promo_code->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('code')) has-error @endif">
                       <label class="col-sm-2 control-label" for="code-field">Code</label>
                    <input type="text" id="code-field" name="code" class="form-control" value="{{ is_null(old("code")) ? $promo_code->code : old("code") }}"/>
                       @if($errors->has("code"))
                        <span class="help-block">{{ $errors->first("code") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('status')) has-error @endif">
                       <label class="col-sm-2 control-label" for="status-field">Status</label>
                    
                      <select id="status-field" name="status" class="form-control" >
                        <option {{ is_null(old("status")) ? $promo_code->status : old("status") == 0 ? 'selected' : '' }} value="0">Available</option>
                        <option {{ is_null(old("status")) ? $promo_code->status : old("status") == 1 ? 'selected' : '' }} value="1">Not Available</option>
                    </select>

                       @if($errors->has("status"))
                        <span class="help-block">{{ $errors->first("status") }}</span>
                       @endif
                    </div>
                     <div class="form-group @if($errors->has('type')) has-error @endif">
                       <label class="col-sm-2 control-label" for="type-field">type</label>
                    <select id="type-field" name="type" class="form-control" >
                        <option {{ is_null(old("type")) ? $promo_code->type : old("type") == 0 ? 'selected' : '' }} value="0">Free</option>
                        <option {{ is_null(old("type")) ? $promo_code->type : old("type") == 1 ? 'selected' : '' }} value="1">Discount</option>
                    </select>
                       @if($errors->has("type"))
                        <span class="help-block">{{ $errors->first("type") }}</span>
                       @endif
                    </div>
                     <div id="discount" class="form-group @if($errors->has('discount')) has-error @endif">
                       <label class="col-sm-2 control-label" for="discount-field">discount</label>
                    <input type="number" id="discount-field" name="discount" class="form-control" value="{{ is_null(old("discount")) ? $promo_code->discount : old("discount") }}"/>
                       @if($errors->has("discount"))
                        <span class="help-block">{{ $errors->first("discount") }}</span>
                       @endif
                    </div>

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('promo_codes.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
