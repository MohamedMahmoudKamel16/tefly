@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h3>Cities / Show #{{$CountryCity->id}}</h3>
        <form action="{{ route('CountryCity.destroy', $CountryCity->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('CountryCity.edit', $CountryCity->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static">{{$CountryCity->id}}</p>
                </div>
                <div class="form-group">
                     <label for="code">City Name</label>
                     <p class="form-control-static">{{$CountryCity->name}}</p>
                </div>
                    <div class="form-group">
                     <label for="status">Country Name</label>
                     <p class="form-control-static">{{$CountryCity->Country->name}}</p>
                </div>

                </div>
                    <div class="form-group">
                     <label for="status">Created at</label>
                     <p class="form-control-static">{{$CountryCity->created_at}}</p>
                </div>

            </form>

            <a class="btn btn-link" href="{{ route('CountryCity.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection