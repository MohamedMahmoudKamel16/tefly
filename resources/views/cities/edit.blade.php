@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> Cities / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

           <form action="{{ route('CountryCity.update', $CountryCity->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label class="col-sm-2 control-label" for="code-field">City Name</label>
                       <input required="please enter city name" type="text" id="code-field" name="name" class="form-control" value="{{ is_null(old("name")) ? $CountryCity->name : old("name") }}" />
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                  </div>


                  <div class="form-group @if($errors->has('country_id')) has-error @endif">
                     <label class="col-sm-2 control-label" for="status-field">Country</label>
                    <select id="status-field" name="country_id" class="form-control" >
                      @foreach($countries as $country)
                        @if($country->id == $CountryCity->country_id)
                          <option value="{{$country->id}}" selected>{{$country->name}}</option>
                        @else
                          <option value="{{$country->id}}">{{$country->name}}</option>
                        @endif  
                      @endforeach
                    </select>
                     @if($errors->has("country_id"))
                      <span class="help-block">{{ $errors->first("country_id") }}</span>
                     @endif
                  </div>

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('CountryCity.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
<!--   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
  if($("#type-field").val() == "1"){
        $("#discount").show();
      }else{
        $("#discount").hide();

      }
    $("#type-field").on("change",function(){
      if($("#type-field").val() == "1"){
        $("#discount").show();
      }else{
        $("#discount").hide();

      }
    });
  </script> -->
@endsection
