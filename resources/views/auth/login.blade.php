<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Tefli</title>

    <!-- Bootstrap core CSS -->
    <link href="{{URL::asset('public/assets/css/bootstrap.css')}}" rel="stylesheet">
    <!--external css-->
    <link href="{{URL::asset('public/assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="{{URL::asset('public/assets/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('public/assets/css/style-responsive.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')}}"></script>
    <![endif]-->
<style type="text/css">
    
    .form-control {
    display: block;
        width: 100%;
    height: 44px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.32857143;
    color: #555;
    background-color: #fff;
}
</style>
  </head>

  <body>


      <div id="login-page">
        <div class="container">
        <form class="form-horizontal form-login" role="form" method="POST" action="{{ url('/login') }}" autocomplete="off">
                        {{ csrf_field() }}
                <h2 class="form-login-heading">sign in now</h2>
                <div class="login-wrap">
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                     <input type="text" name="email" id="email" class="form-control" placeholder="Email" autocomplete="off">
                    <br>
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" autocomplete="off">
                    <h3></h3>
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                </div>
                    <button class="btn btn-theme btn-block" href="index.html" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                    <hr>
                    

                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
        
                </div>
    
        
              </form>       
        
        </div>
      </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{URL::asset('public/assets/js/jquery.js')}}"></script>
    <script src="{{URL::asset('public/assets/js/bootstrap.min.js')}}"></script>

    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="{{URL::asset('public/assets/js/jquery.backstretch.min.js')}}"></script>
    <script>
        $.backstretch("{{URL::asset('public/assets/img/login-bg.jpg')}}", {speed: 500});
    </script>


  </body>
</html>

