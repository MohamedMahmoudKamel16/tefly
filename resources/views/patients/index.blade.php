@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i> Patients
            <a class="btn btn-success pull-right" href="{{ route('patients.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
        </h1>

    </div>
@endsection
<?php use App\Order; ?>
@section('content')
@if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            @if($patients->count())
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>E-mail</th>
                            <th>Mobile</th>
                            <th>Num Of Calls</th>
                            <th>NO_KIDS</th>
                            <th>Block</th>
                            <th>Created_at</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($patients as $patient)
                            <tr>
                                <td>{{$patient->info->id}}</td>
                                <td>{{$patient->info->name}}</td>
                                <td>{{$patient->info->email}}</td>
                                <td>{{$patient->info->mobile}}</td>
                                <td><?php echo  Order::where('user_id' , $patient->info->id )->orderBy('id', 'desc')->count(); ?></td>
                                <td>{{$patient->no_kids}}</td>
                                <td><h4 id="user_{{$patient->info->id}}" onclick="patientAjaxBlock('{{$patient->info->id}}')">
                                    <?php if($patient->info->is_block == 0){echo '<i class="fa fa-unlock-alt" aria-hidden="true"></i>';}else{echo'<i class="fa fa-lock" aria-hidden="true"></i>';} ?>
                                </h4></td>
                                 <td>{{$patient->created_at}}</td>
                                <td class="text-right">
                                    <a class="btn btn-xs btn-primary" href="{{ route('patients.show', $patient->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
                                    <a class="btn btn-xs btn-warning" href="{{ route('patients.edit', $patient->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('patients.destroy', $patient->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>


<script>   
function patientAjaxBlock(id){

    $.get(URL+"/patientAjaxBlock/"+id, function(data, status){
        document.getElementById("user_"+id).innerHTML = data;
    });
}
</script>
@endsection