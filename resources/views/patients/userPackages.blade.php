@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h1>
            <i class="glyphicon glyphicon-align-justify"></i>{{ $user->name }}'s Package History
        </h1>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($packages->count())
                <table id="datatable" class="table table-striped table-bpackageed">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Package Name</th>
                            <th>Has Promo Code ?</th>
                            <th>Num Of Remain Calls</th>
                            <th>Package Status</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($packages as $package)
                            <tr>
                                <td>{{$package->id}}</td>
                                <td>{{$package->package_name}}</td>
                                <td>@if( empty($package->promo_code_id) ) No @else Yes @endif</td>
                                <td>{{$package->num_calls}}</td>
                                <td>@if($package->status == 1) Finished @else UnFinished @endif</td>
                                <td><a class="btn btn-xs btn-warning" href="{{ URL('EditPackageHistory', $package->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

    <a class="btn btn-link" href="{{ route('patients.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

@endsection