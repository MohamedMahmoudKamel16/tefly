@extends('layouts.admin')
@section('header')
<div class="page-header">
        <h3>Patient #{{$user->name}}</h3>
        <form action="{{ route('patients.destroy', $patient->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('patients.edit', $patient->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                
                <div class="form-group col-md-4">
                     <p class="form-control-static"><a class="btn btn-xs btn-primary" href="{{ url('patientpreviousCalls', $user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> Previous Calls</a></p>
                </div>

                <div class="form-group col-md-4">
                     <p class="form-control-static"><a class="btn btn-xs btn-primary" href="{{ url('PackageHistory', $user->id) }}"><i class="glyphicon glyphicon-eye-open"></i> Package History</a></p>
                </div>

                <div class="form-group col-md-4">
                     <label class=" control-label" for="name">Num Of Calls</label>
                     <p class="form-control-static">{{$numOfCalls}}</p>
                </div>

                <div class="form-group col-md-4">
                     <label class=" control-label" for="name">Device Type</label>
                     <p class="form-control-static">{{$platform}}</p>
                </div>


                <div class="form-group col-md-4">
                     <label class=" control-label" for="name">NAME</label>
                     <p class="form-control-static">{{$user->name}}</p>
                </div>
                    
                 <div class="form-group col-md-4">
                     <label class=" control-label" for="email">EMAIL</label>
                     <p class="form-control-static">{{$user->email}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label class=" control-label" for="country">COUNTRY</label>
                     <p class="form-control-static">{{$patient->country}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label class=" control-label" for="mobile">MOBILE</label>
                     <p class="form-control-static">{{$user->mobile}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label class=" control-label" for="city">CITY</label>
                     <p class="form-control-static">{{$patient->city}}</p>
                </div>
                    <div class="form-group col-md-4">
                     <label class=" control-label" for="no_kids">NO_KIDS</label>
                     <p class="form-control-static">{{$patient->no_kids}}</p>
                </div>
                   
                </div>
                    <div class="form-group">
                     <label for="status" class="control-label">Created at</label>
                     <p class="form-control-static">{{$patient->created_at}}</p>
                </div>

                   
            </form>

            <a class="btn btn-link" href="{{ route('patients.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection