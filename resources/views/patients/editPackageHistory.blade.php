@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i> Edit Package History</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ URL('storeEditPackageHistory') }}" method="POST" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="package_id" value="{{$packages->id}}" >

                <div class="form-group @if($errors->has('num_calls')) has-error @endif">
                       <label class="col-sm-2 control-label" for="name-field">Remain Calls</label>
                    <input type="number" id="name-field" name="num_calls" class="form-control" value="{{ is_null(old("num_calls")) ? $packages->num_calls : old("num_calls") }}"/>
                       @if($errors->has("num_calls"))
                        <span class="help-block">{{ $errors->first("num_calls") }}</span>
                       @endif
                </div>
                   
                <div class="form-group @if($errors->has('status')) has-error @endif">
                   <label class="col-sm-2 control-label" for="status-field">status</label>
                  <select id="status-field" name="status" class="form-control" >
                    @if($packages->status == 1)   
                      <option value="1" selected>Finished</option>
                    @else
                      <option value="1">Finished</option>
                    @endif


                    @if($packages->status == 0)   
                      <option value="0" selected>UnFinished</option>
                    @else
                      <option value="0">UnFinished</option>
                    @endif
                  </select>
                   @if($errors->has("status"))
                    <span class="help-block">{{ $errors->first("status") }}</span>
                   @endif
                </div>


                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
