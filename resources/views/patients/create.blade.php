@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> Patients / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('patients.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group @if($errors->has('name')) has-error @endif">
                       <label class="col-sm-2 control-label"  for="name-field">Name</label>
                    <input type="text" id="name-field" name="name" class="form-control" value="{{ old("name") }}" required="required" />
                       @if($errors->has("name"))
                        <span class="help-block">{{ $errors->first("name") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('email')) has-error @endif">
                       <label class="col-sm-2 control-label" for="email-field">E-mail</label>
                    <input type="text" id="email-field" name="email" class="form-control" value="{{ old("email") }}" required="required"/>
                       @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first("email") }}</span>
                       @endif
                    </div>
                     <div class="form-group @if($errors->has('mobile')) has-error @endif">
                       <label class="col-sm-2 control-label" for="mobile-field">Mobile</label>
                    <input type="text" id="mobile-field" name="mobile" class="form-control" value="{{ old("mobile") }}" required="required"/>
                       @if($errors->has("mobile"))
                        <span class="help-block">{{ $errors->first("mobile") }}</span>
                       @endif
                    </div>
                   

                    <div class="form-group @if($errors->has('password')) has-error @endif">
                       <label class="col-sm-2 control-label" for="password-field">Password</label>
                    <input type="text" id="password-field" name="password" class="form-control" value="{{ old("password") }}"/>
                       @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first("password") }}</span>
                       @endif
                    </div>

                  <div class="form-group @if($errors->has('country')) has-error @endif">
                     <label class="col-sm-2 control-label" for="status-field">Country</label>
                    <select id="status-field" name="country" class="form-control" >
                      <option value="1">Egypt</option>
                    </select>
                     @if($errors->has("country"))
                      <span class="help-block">{{ $errors->first("country") }}</span>
                     @endif
                  </div>

                  <div class="form-group @if($errors->has('city')) has-error @endif">
                     <label class="col-sm-2 control-label" for="status-field">city</label>
                    <select id="status-field" name="city" class="form-control" >
                    @foreach($cities as $city)
                      <option value="{{$city->name}}">{{$city->name}}</option>
                    @endforeach  
                    </select>
                     @if($errors->has("city"))
                      <span class="help-block">{{ $errors->first("city") }}</span>
                     @endif
                  </div>


                    <div class="form-group @if($errors->has('no_kids')) has-error @endif">
                       <label class="col-sm-2 control-label" for="no_kids-field">No_kids</label>
                    <input type="number" min="1" id="no_kids-field" name="no_kids" class="form-control" value="{{ old("no_kids") }}"/>
                       @if($errors->has("no_kids"))
                        <span class="help-block">{{ $errors->first("no_kids") }}</span>
                       @endif
                    </div>
                   

                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('patients.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
