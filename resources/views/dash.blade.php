@extends('layouts.admin')
<style type="text/css">
    .custome{

        height: 150px;
        background-color: #fefefe;
        margin: 5px;
        box-shadow: 1px;
    }

    .custome img {
        
        margin-top: 18px;
        width: 110px;
    }

    .title{
        
        padding-left: 100px;
        font-size: 15px;
    }

    .title2{
        
        padding-left: 80px;
        font-size: 13px;
    }

    .number{
        
        margin-left: 80px;
        font-size: 30px;
    }

</style>
@section('header')
<div class="page-header" style="border-bottom: 1px solid #c8d1d8;">
        <h1>DashBoard Admin panel</h1>
</div>
@endsection

@section('content')
    <div class="row">  

        <div class="col-lg-3 col-lg-offset-1 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/stars.png')}}" class="img-circle" >
            <span class="number"><strong>{{ round($AvgDoctorRate) }}</strong></span><br>
            <span class="title" >Average rate of all doctors</span>
        </div>

        <div class="col-lg-3 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/patients.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $numPatient }}</strong></span><br>
            <span class="title" >number of patients</span>
        </div>

        <div class="col-lg-3 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/doctors.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $numDoctors }}</strong></span><br>
            <span class="title" >number of doctors</span>
        </div>

    </div>

    <div class="row">
        
        <div class="col-lg-3 col-lg-offset-1 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/requests (3).png')}}" class="img-circle" >
            <span class="number"><strong>{{ $numRequests }}</strong></span><br>
            <span class="title" >number of requests</span>
        </div>


        <div class="col-lg-3  custome">
            <img src="{{URL::asset('public/images/dashboardIcons/finishedcalls.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $numFinished }}</strong></span><br>
            <span class="title" >number of finished calls</span>
        </div>

        <div class="col-lg-3  custome">
            <img src="{{URL::asset('public/images/dashboardIcons/cancelledcalls.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $numCanceledByPatient }}</strong></span><br>
            <span class="title" >Canceled Requests</span>
        </div>

    </div>

    <div class="row">
        
        <div class="col-lg-3 col-lg-offset-1 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/minutes.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $numMintues}}</strong></span><br>
            <span class="title" >number of mintues</span>
        </div>


        <div class="col-lg-3 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/Max.png')}}" class="img-circle" >
            <span><strong>{{ $maxCallDocName }}</strong></span><br>
            <span class="title" >max. number of calls</span>
        </div>


        <div class="col-lg-3 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/Min.png')}}" class="img-circle" >
            <span><strong>{{ $minCallDocName }}</strong></span><br>
            <span class="title" >min. number of calls</span>
        </div>

    </div>   

    <div class="row">  

        <div class="col-lg-3 col-lg-offset-1 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/patients.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $MonthlySubscribe }}</strong></span><br>
            <span class="title2" >monthly subscription this month</span>
        </div>


        <div class="col-lg-3 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/patients.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $DailySubscribe }}</strong></span><br>
            <span class="title2" >daily subscription this month</span>
        </div>


        <div class="col-lg-3 custome">
            <img src="{{URL::asset('public/images/dashboardIcons/cancelledcalls.png')}}" class="img-circle" >
            <span class="number"><strong>{{ $numNotRespondByDoctor }}</strong></span><br>
            <span class="title2" >No Respond Requests</span>
        </div>


    </div>


@endsection