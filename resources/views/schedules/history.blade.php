@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h3>
            @if(!empty($From))
                <center>From : {{ $From }} / To : {{ $To }}</center>
            @endif  
            <i class="glyphicon glyphicon-align-justify"></i> Schedules
            <div class="dropdown">
                <button class="dropbtn">Weeks History</button>
                <div class="dropdown-content scrollable-menu">
                    @foreach($NewWeeks as $week)
                        <center><a href="{{ url('viewScheduleHistory',$week) }}">{{$week}}</a></center>
                    @endforeach     
                </div>
            </div>
        </h3>

    </div>
@endsection

@section('content')
@if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            @if($schedules->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                        <th>DAY</th>
                       @foreach($durations as $duration)
                        <th>{{$duration->start_time}} - {{$duration->end_time}}</th>

                       @endforeach
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($schedules as $schedule)
                            <tr>
                                <td style="color: blue;">{{$schedule->day}}</td>
                                 @for($i = 0 ; $i <count($durations);$i++)

                                    <td>
                                        @foreach($ScheduleDoctorsHistory as $Doctor)

                                            @if($Doctor->day->day == $schedule->day AND $durations[$i]->start_time == $Doctor->time->start_time)
                                                <p>{{ $Doctor->user->name }}</p>
                                            @endif

                                        @endforeach
                                    </td>

                                 @endfor        
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $schedules->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection


