@extends('layouts.admin')

@section('header')
    <div class="page-header clearfix">
        <h3>
        @if(!empty($From))
            <center>From : {{ $From }} / To : {{ $To }}</center>
        @endif    
            <i class="glyphicon glyphicon-align-justify"></i> Schedules
            
            <div class="dropdown">
                <button class="dropbtn">Weeks History</button>
                <div class="dropdown-content">
                    @foreach($NewWeeks as $week)
                       <center><a href="{{ url('viewScheduleHistory',$week) }}">{{$week}}</a></center>
                    @endforeach    
                </div>
            </div>
        </h3>

    </div>
@endsection

@section('content')
@if ($message = Session::get('message'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
      </div>
    @endif
    <div class="row">
        <div class="col-md-12">
            @if($schedules->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                        <th>DAY</th>
                       @foreach($durations as $duration)
                        <th>{{$duration->start_time}} - {{$duration->end_time}}</th>

                       @endforeach
                
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($schedules as $schedule)
                            <tr>
                                <td style="color: blue;">{{$schedule->day}}</td>
                                @for($i = 0 ; $i <count($durations);$i++)

                                <td>

                                    @foreach($schedule->doctors as $doctor)
                                        @if($durations[$i]->start_time == $doctor->time->start_time )
                                        <p>{{$doctor->user->name}}</p>
                                        @endif
                                    @endforeach
                                </td>
                                @endfor
                                <td class="text-right">
                                  
                                    <a class="btn btn-xs btn-warning" href="{{ route('schedules.edit', $schedule->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                               <!--     <form action="{{ route('schedules.destroy', $schedule->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                    -->
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $schedules->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection
