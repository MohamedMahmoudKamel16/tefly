@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-edit"></i>Edit Schedules {{$schedule->day}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('schedules.update', $schedule->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                  <?php $count = 1; ?>

                  @if(count($schedules))
                    @foreach($schedules as $schedulee)

                     @if( $count % 2 != 0 )
                        <div class="row col-md-12" style="margin-top: 15px;margin-bottom: 15px;">
                         <div class="form-group @if($errors->has('duration_id')) has-error @endif">
                          <div class="col-md-4">
                            <input type="text" disabled value="{{$schedulee->time->start_time}} - {{$schedulee->time->end_time}}" name="duration_id[]" class="form-control" >
                           @if($errors->has("schedule_id"))
                            <span class="help-block">{{ $errors->first("duration_id") }}</span>
                           @endif
                           </div>
                     @endif
                      
                       <div class="col-md-4">
                         <select id="doctor-field" name="doctor[]" class="form-control" >
                          @foreach($doctors as $doctor)
                            <option  value="{{$doctor->id}}"  {{ $schedulee->user_id == $doctor->id ? 'selected' : '' }}>{{$doctor->name}} - {{$doctor->mobile}}</option>
                           @endforeach
                         </select>
                         @if($errors->has("doctor"))
                          <span class="help-block">{{ $errors->first("doctor") }}</span>
                         @endif
                       </div>




                     @if( $count % 2 == 0 )
                         </div>
                      </div>
                     @endif

                     <?php $count++; ?>
                    @endforeach
                  @endif
                <div class="row">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('schedules.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
