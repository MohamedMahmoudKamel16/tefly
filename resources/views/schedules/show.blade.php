@extends('layout')
@section('header')
<div class="page-header">
        <h1>Schedules / Show #{{$schedule->id}}</h1>
        <form action="{{ route('schedules.destroy', $schedule->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('schedules.edit', $schedule->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="day">DAY</label>
                     <p class="form-control-static">{{$schedule->day}}</p>
                </div>
                    <div class="form-group">
                     <label for="day_code">DAY_CODE</label>
                     <p class="form-control-static">{{$schedule->day_code}}</p>
                </div>
                    <div class="form-group">
                     <label for="start_time">START_TIME</label>
                     <p class="form-control-static">{{$schedule->start_time}}</p>
                </div>
                    <div class="form-group">
                     <label for="end_time">END_TIME</label>
                     <p class="form-control-static">{{$schedule->end_time}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('schedules.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection