<?php 
use App\Patient;
?>
@extends('layouts.admin')

@section('header')

@endsection

@section('content')
	    <div class="page-header clearfix">
        
        <h3>
        <i class="glyphicon glyphicon-align-justify"></i> Feedback
    </h3>
    </div>
            
    <div class="row">
        <div class="col-md-12 table-responsive" >
            @if($feedbacks->count())
                <table id="datatable" class="table table-striped table-bordered" style="table-layout: fixed; width: 100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th >SUBJECT</th>
                            <th>BODY</th>
                            <th>EMAIL</th>
                            <th>Created_at</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($feedbacks as $feedback)
                            <tr>
                                <td>{{$feedback->id}}</td>
                                <td>{{$feedback->subject}}</td>
                    <td><div style="word-wrap: break-word; " >{{$feedback->body}} </div></td>
                    <?php $user_id = Patient::where('user_id' , $feedback->user->id)->value("id"); ?>
                    <td>@if($feedback->user)<a href="{{URL::asset('patients/'.$user_id)}}">{{$feedback->email}}</a> @else {{$feedback->email}} @endif</td>
                    <td>{{$feedback->created_at}}</td>
                                <td class="text-right">
                                  
                                    <form action="{{ route('feedback.destroy', $feedback->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection