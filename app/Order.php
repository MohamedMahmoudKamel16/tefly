<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use JWTAuth;
class Order extends Model
{
    //
     protected $hidden = [
        'updated_at','user_id','duration_id','schedule_id'
    ];

  //  protected $dateFormat = 'd F Y';
    protected $appends = array('doctor','patient','patient_request','packageType','doctors_on_shift' , 'doctor_accept');


    public function doctors()
    {
      return $this->belongsToMany(\App\User::class, 'order_doctors', 'order_id', 'user_id');
    }

    public function getDoctorsOnShiftAttribute()
    {

      $doctor_ids = OrderDoctor::limit(2)->where("order_id",$this->id)->where('onShift' , 1)->orderBy('created_at','ASC')->pluck('user_id')->toArray();
      $doctors     = Doctor::whereIn('user_id',$doctor_ids)->get();
      return $doctors;

    }


    public function getDoctorAcceptAttribute()
    {

      // $doctor_id = OrderDoctor::where("order_id",$this->id)->where('status' , 1)->orderBy('created_at','ASC')->value('user_id');
      // $doctor     = Doctor::where('user_id',$doctor_id)->first();
      // return $doctor;

      $doctor_ids = OrderDoctor::limit(2)->where("order_id",$this->id)->where('is_accept' , 1)->orderBy('created_at','ASC')->pluck('user_id')->toArray();
      $doctors     = Doctor::whereIn('user_id',$doctor_ids)->get();
      return $doctors;


    }



    public function getDoctorAttribute()
    {

         $id= OrderDoctor::where("order_id",$this->id)->where("status",3)->value('user_id');

         $user= User::where('id',$id)->select('id','name','email','image','mobile')->first();
         if(count($user) == 0)
         {
         		$user = JWTAuth::parseToken()->authenticate();
         }
         	$doctor = Doctor::where('user_id',$user->id)->first();

    		if($doctor)
    		{
              $user->id = $doctor->id;
    			    $user->bio = $doctor->bio;
    	        $user->title = $doctor->title;
    	        $user->clinic_address = $doctor->clinic_address;
    	        $user->clinic_phone = $doctor->clinic_phone;
    	        $user->website_link = $doctor->website_link;
    		}else{
    			$user->bio = "";
    	        $user->title = "";
    	        $user->clinic_address = "";
    	        $user->clinic_phone = "";
    	        $user->website_link = "";
    		}
    		return $user;

    }

    public function getPatientAttribute()
    {

       $user= User::where('id',$this->user_id)->select('id','name','email','mobile')->first();
       $patient = Patient::where('user_id',$user->id)->first();
    		if($patient)
    		{
    			$user->country = $patient->country;
    	        $user->city = $patient->city;
    	        $user->no_kids = $patient->no_kids;
    		}else{
    			$user->country = "";
    	        $user->city = "";
    	        $user->no_kids = 0;
    		}
    		return $user;

    }


    public function getPatientRequestAttribute()
    {

       $user= User::where('id',$this->user_id)->select('id','name','email','mobile')->first();
       $patient = Patient::where('user_id',$user->id)->first();
        if($patient)
        {
          $user->id = $patient->id;
          $user->country = $patient->country;
              $user->city = $patient->city;
              $user->no_kids = $patient->no_kids;
        }else{
          $user->country = "";
              $user->city = "";
              $user->no_kids = 0;
        }
        return $user;

    }


    public function getPackageTypeAttribute()
    {

       $package_id = UserPackage::where("id",$this->package_id)->value("package_id");
       //$price = Package::where("id",$package_id)->value("price");
		return $package_id;
    }



    public function getcreatedAtAttribute($value)
    {
       return \Carbon\Carbon::parse($value)->format('d F Y h:i A');
    }

}
