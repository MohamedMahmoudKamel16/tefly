<?php

namespace App;
use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','mobile','is_block','image','version','platform','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','type','updated_at'
    ];
     #hash password
    public function setPasswordAttribute($pass)
    {
           $this->attributes['password'] = Hash::make($pass);
    }


    public function orders()
    {
        return $this->belongsToMany(\App\Order::class, 'order_doctors', 'user_id', 'order_id');
    }

    public function acceptedOrders()
    {
        return $this->orders()->wherePivot('is_accept', 1)->count();
    }

    public function unacceptedOrders()
    {
        return $this->orders()->wherePivot('is_accept', 0)->count();
    }

    public function getImageAttribute($value)
    {
        if($value != ""){
            $value=url("/")."/public/images/users/".$value;
        }
        return $value;
    }
    public function getcreatedAtAttribute($value)
    {
       return \Carbon\Carbon::parse($value)->format('d F Y h:i A');
    }

}
