<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{

    /**
     * [$fillable description]
     * @var [type]
     */
    public $fillable = ['user_id', 'order_id', 'mobile_number', 'provider', 'package', 'pincode', 'transaction_id', 'status'];
}
