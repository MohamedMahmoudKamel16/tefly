<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model
{
    //
    protected $hidden =['updated_at','user_id','id'];
    // public $timestamps = false;
    protected $appends = array('no_calls','package_name','discount');

    public function getNoCallsAttribute()
    {
        
        if (!empty($this->getDiscountAttribute())) {

            $TotalNumCalls =  ($this->GetPackageNoCalls() + $this->getDiscountAttribute());
        }else{
            $TotalNumCalls =  ($this->GetPackageNoCalls());
        }
        
        return $TotalNumCalls; 
    }

    public function getPackageNameAttribute()
    {
        $package_name = Package::where("id",$this->package_id)->value("name");
        return $package_name; 
    }

    public function GetPackageNoCalls()
    {
        $no_calls = Package::where("id",$this->package_id)->value("no_calls");
        return $no_calls; 
    }

    public function getDiscountAttribute()
    {
        $discount = PromoCode::where("id",$this->promo_code_id)->value("discount");
        return $discount; 
    }


}
