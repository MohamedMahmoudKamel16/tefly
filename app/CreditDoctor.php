<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditDoctor extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'credit_doctors';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'calls_credit'
    ];

    


}
