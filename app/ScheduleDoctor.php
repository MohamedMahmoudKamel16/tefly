<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleDoctor extends Model
{
    protected $table = 'schedule_doctors';
    protected $fillable = [
        'user_id', 'duration_id', 'schedule_id',
    ];

    protected $appends =['time','user','day'];
    public function getTimeAttribute()
    {

       return Duration::where('id',$this->duration_id)->first();
    }

    public function getDayAttribute()
    {

       return Schedule::where('id',$this->schedule_id)->first();
    }

    public function getUserAttribute()
    {

       return User::where('id',$this->user_id)->first();
    }
}
