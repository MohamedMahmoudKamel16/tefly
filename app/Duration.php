<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
    //
    protected $hidden =['updated_at','created_at','id'];

    public function getStartTimeAttribute($value)
    {

       return \Carbon\Carbon::parse($value)->format('H:i');
    }

    public function getEndTimeAttribute($value)
    {

       return \Carbon\Carbon::parse($value)->format('H:i');
    }
}
