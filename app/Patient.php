<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    public function info()
    {
        return   $this->belongsTo('App\User','user_id');
    }
}
