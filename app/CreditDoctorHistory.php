<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditDoctorHistory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'credit_doctors_history';
     public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'calls_credit', 'weekDate'   
    ];

    


}
