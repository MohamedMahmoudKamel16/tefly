<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Duration;
use App\User;
use App\Day;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $days= Day::all();
        View::share('days', $days);

        $doctors= User::where('type',1)->where('is_block',0)->get();
        View::share('doctors', $doctors);

        $durations= Duration::orderBy('start_time','asc')->get();
        View::share('durations', $durations);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
