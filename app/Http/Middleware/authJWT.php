<?php

namespace App\Http\Middleware;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
//use Request;
use App\Http\Requests;
use JWTAuthException;

use Closure;
use JWTAuth;
use Exception;
use App\User;
class authJWT
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
       try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {

                return response()->json(array('msg'=>'user_not_found'), 450);
            }
            if ( $user = JWTAuth::parseToken()->authenticate() && $user->is_block == 1) {

                return response()->json(array('msg'=>'This user blocked from App'), 456);
            }

        }catch (TokenExpiredException $e) {
    
                return response()->json(array('msg'=>'token_expired'), 451); 

        }catch (TokenInvalidException $e) {


                return response()->json(['msg'=>'token_invalid'], 452);

         //   return $next($request);


        }catch(JWTAuthException $e){
            return response()->json(['msg'=>'Something is wrong'],453);


        }
        catch(Exception $e){

            return response()->json(['msg'=>'unauthorize'],454);

        }
	
	
        return $next($request);

    }

}
