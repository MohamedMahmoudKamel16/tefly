<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UserPackage;
use App\CreditDoctor;
use App\PromoCode;
use App\Package;
use App\Order;
use App\User;
use App\OrderDoctor;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Jobs\SendNotification;
use Illuminate\Support\Facades\Input;
use App\Doctor;

class OrderController extends Controller {

//=============================================================================================================
	#get orders assigned to doctor but not any one accept [DONE]
	public function get_doctor_new_orders(Request $request)
	{
		if(! isset($request->take)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->offset)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{

			#doctor
			$admin     = (new AuthController)->get_user();
			$order_ids = OrderDoctor::where('user_id',$admin->id)->whereIn('status',[0,1, 7])->pluck('order_id')->toArray();
			#status = 3 order is done
			$orders = Order::whereIn('id',$order_ids)->whereIn('status',[0,1])->skip($request->offset)->take($request->take)->orderBy('created_at', 'DESC')->get();
			$user      = User::where('id',$admin->id)->where('type' , 1)->first();


			if (count($user) > 0) {

				$DoctorOrdersIds = OrderDoctor::where('user_id',$user->id)->pluck('order_id')->toArray();
				$DoctorRate      = Order::whereIn('id' , $DoctorOrdersIds )->where('status',3)->avg('rate');

				$DoctorCredit    = CreditDoctor::where('user_id' , $user->id)->value("calls_credit");
				$DoctorNumCalls  = ( (int)$DoctorCredit ) / 2;
				// $DoctorNumCalls  = Order::whereIn('id' , $DoctorOrdersIds )->where('status',3)->count();
				// $DoctorCredit    = $DoctorNumCalls * 2;

				if ($DoctorRate == null) {
					$DoctorRate = 0;
				}

				$user->doctorRate = round($DoctorRate);
				$user->doctorNumCalls = $DoctorNumCalls;
				$user->doctorCredit = $DoctorCredit;
			}



			if (count($orders)>0) {
				$statusCode = 200;
				$response  = array('msg' => 'done' , 'calls'=>$orders , 'user'=>$user);
		        return response()->json($response, $statusCode);
			}else{
				$statusCode = 201;
				$response  = array('msg' => 'no data','calls'=>$orders , 'user'=>$user);
		        return response()->json($response, $statusCode);
			}

		}

	}
//=============================================================================================================
	#get doctor orders [DONE]
	public function get_doctor_orders(Request $request)
	{
		if(! isset($request->take)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->offset)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{
			#doctor
			$admin = (new AuthController)->get_user();
			$order_ids=OrderDoctor::where('user_id',$admin->id)->whereIn('status',[2,3,4,7])->pluck('order_id')->toArray();

			#status = 3 order is done
			$orders = Order::whereIn('id',$order_ids)->skip($request->offset)->take($request->take)->orderBy('created_at','DESC')->get();
			if (count($orders)>0) {
				$statusCode = 200;
				$response  = array('msg' => 'done','calls'=>$orders);
		        return response()->json($response, $statusCode);
			}else{
				$statusCode = 201;
				$response  = array('msg' => 'no data','calls'=>$orders);
		        return response()->json($response, $statusCode);
			}

		}

	}



	#get patient orders [DONE]
	public function get_patient_orders(Request $request)
	{
		if(! isset($request->take)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->offset)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{
			#patient
			$admin = (new AuthController)->get_user();
			#status = 3 order is done
			$orders = Order::where('user_id',$admin->id)->whereIn('status',[2, 3, 4, 6, 7])->skip($request->offset)->take($request->take)->orderBy('created_at','DESC')->get();



			if (count($orders)>0) {
				$statusCode = 200;
				$response  = array('msg' => 'done','calls'=>$orders);
		        return response()->json($response, $statusCode);
			}else{
				$statusCode = 201;
				$response  = array('msg' => 'no data','calls'=>$orders);
		        return response()->json($response, $statusCode);
			}

		}

	}
//================================================================================================================================
	public function index()
	{
		$orders = Order::orderBy('id', 'desc')->paginate(20);
		//dd($orders[10]->doctor_accept);
		$user = 0;
		return view('orders.index', compact('user','orders'));
	}
//================================================================================================================================
	public function filterOrders()
	{

		if (Input::get('user_id') == 0) {

			if (Input::get('statusChoice') == 0) {
				return redirect()->to('orders');
			}

			if (Input::get('created_at') != null && Input::get('statusChoice') != null) {

				$CarbonFormate = explode("-", Input::get('created_at'));
				$created_at = Carbon::create($CarbonFormate[0],$CarbonFormate[1],$CarbonFormate[2],0)->format('Y-m-d');
				$orders = Order::whereDate('created_at' , '=' , $created_at)->where('status' , Input::get('statusChoice'))->orderBy('id', 'DESC')->get();

			}elseif (Input::get('created_at') == null && Input::get('statusChoice') != null) {

				$orders = Order::where('status' , Input::get('statusChoice'))->orderBy('id', 'DESC')->get();

			}elseif (Input::get('created_at') != null && Input::get('statusChoice') == null) {

				$CarbonFormate = explode("-", Input::get('created_at'));
				$created_at    = Carbon::create($CarbonFormate[0],$CarbonFormate[1],$CarbonFormate[2],0)->format('Y-m-d');
				$orders        = Order::whereDate('created_at' , '=' , $created_at)->orderBy('id', 'DESC')->get();
			}

		}else{

			// $userDetails = User::where('id' , $user_id)->first();

			// if ($userDetails->type == 0) { #patients order

			// 	if ($status == -1) {

			// 		$orders = Order::where('user_id' , $user_id)->orderBy('id', 'DESC')->get();

			// 	}elseif ($status == 4) {

			// 		$orders = Order::where('user_id' , $user_id)->whereIn('status' , [4,6])->orderBy('id', 'DESC')->get();

			// 	}else{

			// 		$orders = Order::where('user_id' , $user_id)->where('status' , $status)->orderBy('id', 'DESC')->get();
			// 	}

			// }else{ #doctor orders


			// 	if ($status == -1) {

			// 		$DoctorOrdersIds  = OrderDoctor::where('user_id',$user_id)->pluck('order_id')->toArray();
			// 		$orders = Order::whereIn('id' , $DoctorOrdersIds )->orderBy('id', 'DESC')->get();


			// 	}elseif ($status == 4) {

			// 		$DoctorOrdersIds  = OrderDoctor::where('user_id',$user_id)->whereIn('status' , [4,6])->pluck('order_id')->toArray();
			// 		$orders = Order::whereIn('id' , $DoctorOrdersIds )->whereIn('status' , [4,6])->orderBy('id', 'DESC')->get();

			// 	}else{

			// 		$DoctorOrdersIds  = OrderDoctor::where('user_id',$user_id)->where('status' , $status)->pluck('order_id')->toArray();
			// 		$orders = Order::whereIn('id' , $DoctorOrdersIds )->where('status' , $status)->orderBy('id', 'DESC')->get();

			// 	}

			// }
		}

		$user = Input::get('user_id');
		$status = Input::get('statusChoice');
		//dd($orders[10]->doctor_accept);
		return view('orders.index', compact('user', 'orders', 'status'));
	}
//================================================================================================================================
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('orders.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$order = new Order();

		$order->user_id = $request->input("user_id");
        $order->rate = $request->input("rate");
        $order->review = $request->input("review");
        $order->status = $request->input("status");
        $order->doctor_notes = $request->input("doctor_notes");
        $order->doctor_report_problem = $request->input("doctor_report_problem");
        $order->package_id = $request->input("package_id");
        $order->kid_name = $request->input("kid_name");
        $order->kid_birthdate = $request->input("kid_birthdate");
        $order->kid_gender = $request->input("kid_gender");
        $order->complain = $request->input("complain");
        $order->patient_notes = $request->input("patient_notes");
        $order->duration_call = $request->input("duration_call");

		$order->save();

		return redirect()->route('orders.index')->with('message', 'Item created successfully.');
	}

//============================================================================================================================
#accept_order [DONE]
	public function accept_order(Request $request)
	{
		if(! isset($request->order_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{
			#doctor that accept request
			$admin = (new AuthController)->get_user();
			$order = Order::where('id', $request->order_id)->first();
			//dd($order->status);
			if(! $order){
				$statusCode = 421;
				$response  = array('msg' => 'This request does not exist! ');
		        return response()->json($response, $statusCode);

			}elseif($order->status != 0){

				$statusCode = 422;
				$response  = array('msg' => 'This request does already taken by another doctor! ');
		        return response()->json($response, $statusCode);
			}else{

			// ==============================<< new notification >>================================

				$contact_phone = $admin->mobile;
				$mobile_number = Doctor::where('user_id', $admin->id)->first()->contact_phone; // contact number not personal number

				#get tokens register id
				$tokens = (new UserDeviceController)->get_tokens_by_ids([$order->user_id]);

				#send notification to doctor on sheft
				$title   = "انتظر إتصال الطبيب";
				$message = "سيقوم الطبيب بالتواصل معكي عن طريق الهاتف من هذا الرقم : " . $mobile_number;
				$type = 1;
				
				// a work around to send the doctor info with order
				$order->doctor_name = $admin->name;
				$order->doctor_num = $mobile_number;

				$notification = (new NotificationController)->send_notification($tokens, $title, $message, $type, $order);

				// delete those properties as they were usful to notifications only
				unset($order->doctor_name);
				unset($order->doctor_num);
			// ==============================<< new notification >>================================

				$pateint = User::where('id' , $order->user_id)->first();
				$pateint->user_status = 1;
				$pateint->save();

				$order->status = 1;
				$order->accepted_at = date("Y-m-d H:i:s");
				$order->save();

				$order_doctors = OrderDoctor::where('order_id',$order->id)->get();

				foreach ($order_doctors as $order_doctor) {

					if ($order_doctor->user_id == $admin->id) {

						$order_doctor->status=1;
						$order_doctor->is_accept=1;
						$order_doctor->save();
					}else{

						$order_doctor->status=10;#request accepted by another doctor.
						$order_doctor->save();
					}
				}

				$statusCode = 200;
				$response  = array('msg' => 'Done!');
		        return response()->json($response, $statusCode);
			}

		}
	}

//============================================================================================================================

	#doctor cancel patient order
	public function not_respond_order(Request $request)
	{
		if(! isset($request->order_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{
			#doctor that accept request
			$admin = (new AuthController)->get_user();
			$order = Order::where('id',$request->order_id)->first();
			if(! $order){
				$statusCode = 421;
				$response  = array('msg' => 'This request does not exist! ');
		        return response()->json($response, $statusCode);

			}else{

				if ($order->status == 4 OR $order->status == 6) {

					$statusCode = 422;
					$response  = array('msg' => 'this request is already canceled by patient');
			        return response()->json($response, $statusCode);

				}else{

					$pateint = User::where('id' , $order->user_id )->first();
					$pateint->user_status = 0;
					$pateint->save();

					$order_doctors = OrderDoctor::where('order_id',$request->order_id)->where('user_id',$admin->id)->get();

					if (count($order_doctors) > 0) {

						$order->status = 2;
						$order->save();
					}

					foreach ($order_doctors as $order_doctor) {

						$order_doctor->status = 2;
						$order_doctor->save();
					}

					$patient_id = Order::where('id',$request->order_id)->pluck('user_id')->toArray();
					#get tokens register id
					$tokens = (new UserDeviceController)->get_tokens_by_ids($patient_id);
					//dd($tokens);
					#send notification to doctor on sheft
					$title   = "لم يتم الرد";
					$message = "حاول الطبيب الاتصال بك ولم يتم الرد";
					$type    = 4;
					$notification = (new NotificationController)->send_notification($tokens,$title,$message,$type,null);


					$statusCode = 200;
					$response  = array('msg' => 'done');
			        return response()->json($response, $statusCode);
			    }

			}

		}

	}
//==========================================================================================================================

	#patient rate request after finishing request[DONE]
	public function rate_order(Request $request)
	{
		if(! isset($request->order_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->rate)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->review)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{

			$admin = (new AuthController)->get_user();
			$pateint = User::where('id' , $admin->id )->first();
			$pateint->user_status = 0;
			$pateint->save();

			#patient that review request
			$admin = (new AuthController)->get_user();
			$order = Order::where('id',$request->order_id)->first();
			if(! $order){
				$statusCode = 421;
				$response  = array('msg' => 'This request does not exist! ');
		        return response()->json($response, $statusCode);

			}else{
				$order->rate = $request->rate;
				$order->review = $request->review;
				$order->save();

				$statusCode = 200;
				$response  = array('msg' => 'done');
		        return response()->json($response, $statusCode);
			}

		}
	}

	#patient cancel request[DONE]
	public function cancel_order(Request $request)
	{
		if(! isset($request->order_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{
			#patient that cancel request
			$admin = (new AuthController)->get_user();
			$order = Order::where('id',$request->order_id)->first();
			if(! $order){
				$statusCode = 421;
				$response  = array('msg' => 'This request does not exist! ');
		        return response()->json($response, $statusCode);

			}else{


				if ($order->status == 2) {

					$statusCode = 422;
					$response  = array('msg' => 'this request is already canceled by doctor.');
			        return response()->json($response, $statusCode);

				}else{

					$pateint = User::where('id' , $admin->id )->first();
					$pateint->user_status = 0;
					$pateint->save();


					$doctors_ids = OrderDoctor::where('order_id',$request->order_id)->where('status',1)->pluck('user_id')->toArray();

					if ( count($doctors_ids) == 0) {
						$doctors_ids = OrderDoctor::where('order_id',$request->order_id)->where('status',0)->pluck('user_id')->toArray();
					}

					#get tokens register id
					$tokens = (new UserDeviceController)->get_tokens_by_ids($doctors_ids);

					#send notification to doctor on sheft
					$title   = "request cancelled";
					$message = "\"".$admin->name."\" cancelled the new consultation request made on \"".$order->created_at."\" ";

					$type    = 4;
					$notification = (new NotificationController)->send_notification($tokens,$title,$message,$type,null);


						if ($order->status == 0) {

							$order->status = 6;
							$order->save();

						}elseif ($order->status == 1) {

							$order->status = 4;
							$order->save();
						}

					$order_doctors = OrderDoctor::where('order_id',$request->order_id)->get();

					foreach ($order_doctors as $order_doctor) {

						if ($order_doctor->status == 0) {

							$order_doctor->status = 6;
							$order_doctor->save();

						}elseif ($order_doctor->status == 1) {

							$order_doctor->status = 4;
							$order_doctor->save();
						}

					}

					$statusCode = 200;
					$response  = array('msg' => 'done');
			        return response()->json($response, $statusCode);

			    }

			}

		}
	}
//==========================================================================================
	#request on call [DONE]
	// public function oncall_order(Request $request)
	// {
	// 	if(! isset($request->order_id)){

	// 		$statusCode = 420;
	// 		$response   = array('msg' => 'Invalid Format! ');
	//         return response()->json($response, $statusCode);

	// 	}else{

	// 		$admin = (new AuthController)->get_user();
	// 		$order = Order::where('id',$request->order_id)->first();
	// 		$real_package_id = UserPackage::where('id' , $order->package_id)->value('package_id');
	// 		$PromoCode = PromoCode::where('id',$order->promo_code_id)->first();
	// 		$package=  UserPackage::where('user_id',$order->user_id)->where('status',0)->first();

	// 		if(! $order){

	// 			$statusCode = 421;
	// 			$response  = array('msg' => 'This request does not exist! ');
	// 	        return response()->json($response, $statusCode);

	// 		}elseif( $package == NULL OR $package->num_calls == 0){

	// 			$statusCode = 201;
	// 			$response  = array('msg' => 'the backage is ended');
	// 	        return response()->json($response, $statusCode);

	// 		}else{

	// 			if ($real_package_id == 2 AND $order->promo_code_id == NULL ) {

	// 				$order->patient_cost = $order->patient_cost + 10;

	// 			}elseif ($real_package_id == 2 AND $PromoCode->type == 0 ) {

	// 				$order->patient_cost = 0;

	// 			}elseif ($real_package_id == 1 AND $order->promo_code_id == NULL ) {

	// 				$order->patient_cost = $order->patient_cost + 5;

	// 			}elseif ($real_package_id == 1 AND $PromoCode->type == 1) {

	// 				if ($package->num_calls > 10) {

	// 					$order->patient_cost = $order->patient_cost;
	// 				}else{

	// 					$order->patient_cost = $order->patient_cost + 5;
	// 				}
	// 			}

	// 			$order->doctor_credit = $order->doctor_credit + 2;
	// 			$order->status = 5;
	// 			$order->save();


	// 			$doctor_id    = OrderDoctor::whereIn('status', [1,5])->where('order_id',$order->id)->value('user_id');
	// 			$order_doctor = OrderDoctor::where('user_id',$doctor_id )->where('order_id',$order->id)->first();
	// 			if(count($order_doctor)<1){
	// 				$order_doctor = new OrderDoctor();
	// 			}

	// 			$order_doctor->user_id=$admin->id;
	// 			$order_doctor->order_id=$order->id;
	// 			$order_doctor->status=5;
	// 			$order_doctor->save();

	// 			#minus package num of calls

	// 			$package->num_calls = $package->num_calls-1;
	// 			if ($package->num_calls == 0) {
	// 			 	$package->status = 1;
	// 			 }
	// 			$package->save();

	// 			#add 2-LE credit to Doctor.
	// 			$DoctorCredit = CreditDoctor::where('user_id' , $doctor_id )->first();
	// 			if (count($DoctorCredit) == 0) {

	// 				$DoctorCredit = new CreditDoctor();
	// 				$DoctorCredit->user_id = $doctor_id ;
	// 				$DoctorCredit->calls_credit = 2;
	// 				$DoctorCredit->save();
	// 			}else{

	// 				$DoctorCredit->calls_credit = $DoctorCredit->calls_credit+2;
	// 				$DoctorCredit->save();
	// 			}

	// 			$statusCode = 200;
	// 			$response  = array('msg' => 'done');
	// 	        return response()->json($response, $statusCode);
	// 		}

	// 	}
	// }
//==========================================================================================

	public function executeQueue(){

		echo shell_exec('php artisan queue:listen');
	}


	public function doctor_finish_order(Request $request)
	{
		if(! isset($request->order_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	    return response()->json($response, $statusCode);
		}elseif(! isset($request->duration_call)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	    return response()->json($response, $statusCode);
		}else{
			#doctor that report call
			$admin = (new AuthController)->get_user();
			$order = Order::where('id', $request->order_id)->first();
			if(! $order){
				$statusCode = 421;
				$response  = array('msg' => 'This request does not exist! ');
		    return response()->json($response, $statusCode);
			}else{
				$pateint = User::where('id', $order->user_id)->first();
				$pateint->user_status = 2;
				$pateint->save();

				$order->status = 7; // 7 means that order done but no report yet
				$order->duration_call = $request->duration_call;
				$order->finished_at = date("Y-m-d H:i:s");
				$order->save();

				$order_doctor = OrderDoctor::where('user_id', $admin->id)->where('order_id', $order->id)->first();

				if(count($order_doctor) < 1){
					$order_doctor = new OrderDoctor();
				}

				$order_doctor->user_id = $admin->id;
				$order_doctor->order_id = $order->id;
				$order_doctor->status = 7; // 7 means that order done but no report yet
				$order_doctor->save();

				//if (!empty($request->doctor_notes)) {

					$patient_id = Order::where('id', $request->order_id)->pluck('user_id')->toArray();
					$tokens = (new UserDeviceController)->get_tokens_by_ids($patient_id);
					$title   = "انتهت مكالمة طفلي";
					$message = "تم انهاء مكالمة طفلي بنجاح انتظر ملاحظات الطبيب";
					$type    = 9;
					$notification = (new NotificationController)->send_notification($tokens,$title,$message,$type,null);
				//}


			//================================<< Credit Logic >>===============================================
				$real_package_id = UserPackage::where('id' , $order->package_id)->value('package_id');
				$PromoCode 		 = PromoCode::where('id',$order->promo_code_id)->first();
				$package 		 = UserPackage::where('user_id',$order->user_id)->where('status',0)->first();
				$packagePrice    = Package::where('id' , $real_package_id)->first();
				$addedValue      = ($packagePrice->price / $packagePrice->no_calls);

				if( $package == NULL OR $package->num_calls == 0){

					$statusCode = 201;
					$response  = array('msg' => 'the backage is ended');
			        return response()->json($response, $statusCode);

				}else{

					if ($real_package_id == 2 AND $order->promo_code_id == NULL ) {

						$order->patient_cost = $order->patient_cost + $addedValue ;

					}elseif ($real_package_id == 2 AND $PromoCode->type == 0 ) {

						$order->patient_cost = 0;

					}elseif ($real_package_id == 1 AND $order->promo_code_id == NULL ) {

						$order->patient_cost = $order->patient_cost + $addedValue ;

					}elseif ($real_package_id == 1 AND $PromoCode->type == 1) {

						if ($package->num_calls > 10) {

							$order->patient_cost = $order->patient_cost;
						}else{

							$order->patient_cost = $order->patient_cost + $addedValue ;
						}
					}

					$order->doctor_credit = $order->doctor_credit + 2;
					$order->save();

					// $doctor_id    = OrderDoctor::whereIn('status', [1])->where('order_id',$order->id)->value('user_id');

					#minus package num of calls
					$package->num_calls = $package->num_calls-1;

					if ($package->num_calls == 0) {
					 	$package->status = 1;
					 }
					$package->save();

					#add 2-LE credit to Doctor.
					$DoctorCredit = CreditDoctor::where('user_id' , $admin->id )->first();
					if (count($DoctorCredit) == 0) {

						$DoctorCredit = new CreditDoctor();
						$DoctorCredit->user_id = $admin->id ;
						$DoctorCredit->calls_credit = 2;
						$DoctorCredit->save();
					}else{

						$DoctorCredit->calls_credit = $DoctorCredit->calls_credit+2;
						$DoctorCredit->save();
					}
				}
			//================================<< Credit Logic >>===============================================

				$statusCode = 200;
				$response  = array('msg' => 'done');
		        return response()->json($response, $statusCode);
			}

		}
	}

//================================================================================================================================================
	#doctor add report or notes of patient after finishing request
	public function doctor_report_order(Request $request)
	{
		if(! isset($request->order_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->doctor_report_problem)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->doctor_notes)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{
			#doctor that report call
			$admin = (new AuthController)->get_user();
			$order = Order::where('id',$request->order_id)->first();
			if(! $order){
				$statusCode = 421;
				$response  = array('msg' => 'This request does not exist! ');
		    return response()->json($response, $statusCode);
			}else{
				$order->status = 3;
				$order->doctor_report_problem = $request->doctor_report_problem;
				$order->doctor_notes = $request->doctor_notes;
				$order->save();

				$order_doctor = OrderDoctor::where('user_id', $admin->id)->where('order_id', $order->id)->first();

				if(count($order_doctor) < 1){
					$order_doctor = new OrderDoctor();
				}

				$order_doctor->user_id = $admin->id;
				$order_doctor->order_id = $order->id;
				$order_doctor->status = 3;
				$order_doctor->save();

				$patient_id = Order::where('id', $request->order_id)->pluck('user_id')->toArray();
				$tokens = (new UserDeviceController)->get_tokens_by_ids($patient_id);
				$title   = "ملاحظات طبيب طفلي";
				$message = "اضغط لتصفح ملاحظات طبيب طفلي على المكالمة السابقة";
				$type    = 3;
				$notification = (new NotificationController)->send_notification($tokens, $title, $message, $type, NULL);

				$statusCode = 200;
				$response  = array('msg' => 'done');
		        return response()->json($response, $statusCode);
			}

		}
	}
//================================================================================================================================================
	#validate promocode old function
	public function validate_promocode(Request $request)
	{
		if(! isset($request->promo_code)){
				$statusCode = 420;
				$response  = array('msg' => 'Invalid Format! ');
		        return response()->json($response, $statusCode);

			}else{

				$admin = (new AuthController)->get_user();
				$promo_code = PromoCode::where('code',$request->promo_code)->first();

				if ($promo_code) {

					$promoUsed =  UserPackage::where('user_id',$admin->id)->where('promo_code_id',$promo_code->id)->where('status',1)->first();
				}


				if(! $promo_code ){

					$statusCode = 423;
					$response  = array('msg' => 'This promo code does not exist! ');
			        return response()->json($response, $statusCode);

				}elseif (count($promoUsed) == 1) {

					$statusCode = 423;
					$response  = array('msg' => 'User use this PromoCode before');
			        return response()->json($response, $statusCode);

				}elseif ($promo_code->status == 1) { # promocode not available now

					$statusCode = 423;
					$response  = array('msg' => 'This promo code does not available now ');
			        return response()->json($response, $statusCode);

				}else{

					$statusCode = 200;
					$response  = array('msg' => 'Done! ','code'=>$promo_code);
			        return response()->json($response, $statusCode);
				}

			}
	}

//=============================<< fawzy edits >>=========================================

	#patient can make a new order
	public function new_order(Request $request)
	{

		if(! isset($request->package_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->kid_name)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->kid_birthdate)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->kid_gender)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->complain)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->patient_notes)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->promo_code)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{
			$admin = (new AuthController)->get_user();

			$pateint = User::where('id' , $admin->id )->first();
			$pateint->user_status = 1;
			$pateint->save();

			$check_pack = Package::where('id',$request->package_id)->first();
			if(! $check_pack){
				$statusCode = 421;
				$response  = array('msg' => 'This package does not exist! ');
		        return response()->json($response, $statusCode);

			}
			if($request->promo_code !="")
			{
				$promo_code = PromoCode::where('code',$request->promo_code)->first();
				if(! $promo_code ){
					$statusCode = 423;
					$response  = array('msg' => 'This promo code does not exist! ');
			        return response()->json($response, $statusCode);

				}
			}

			DB::beginTransaction();
    		try{

				$package=  UserPackage::where('user_id',$admin->id)->where('package_id',$request->package_id)->where('status',0)->orderBy('created_at','DESC')->first();


				if ($package == null) { #if patient hasn't package or finished change (status = 1) and create new one .

		        	if ($request->package_id == 2) {

			        	$package = new UserPackage();
			        	$package->package_id =$request->package_id;
			        	$package->user_id =$admin->id;
			        	if (!empty($promo_code->id)) {

			        		$package->promo_code_id = $promo_code->id;
			        	}else{
			        		$package->promo_code_id = null;
			        	}
			        	$package->num_calls = 1;
			        	$package->status = 0;
			        	$package->save();

			        }else{

			        	$package = new UserPackage();
			        	$package->package_id =$request->package_id;
			        	if (!empty($promo_code->id)) {

			        		$package->promo_code_id = $promo_code->id;
			        	}else{
			        		$package->promo_code_id = null;
			        	}

			        	$TotalNumCalls = $package->getNoCallsAttribute();
			        	$package->user_id =$admin->id;
			        	$package->num_calls = $TotalNumCalls;
			        	$package->save();
			        }

				}else{

					if ($package->num_calls == 0) {

						$package->status = 1;
			        	$package->save();
					}
		        }
		        #get schedule :D
		        $schedule = (new ScheduleController)->get_doctor_by_schedule();
		    //    dd($schedule);die();
		        if(count($schedule)>0){
					$order = new Order();

					$order->user_id = $admin->id;
					$order->deliveryStatus = "shift";
			        $order->rate = 0;
			        $order->review = "";
			        #pending
			        $order->status = 0;
			        $order->doctor_notes = "";
			        $order->doctor_report_problem = "";


			        #packag_id refer to user_package_id
			        $order->package_id = $package->id;
			        $order->kid_name = $request->kid_name;
			        $order->kid_birthdate = $request->kid_birthdate;
			        $order->kid_gender = $request->kid_gender;
			        $order->complain = $request->complain;
			        $order->patient_notes = $request->patient_notes;
			        $order->duration_call = "";
			        $order->schedule_id = $schedule->schedule_id;
			        $order->duration_id = $schedule->duration_id;

			        #promo_code_id
			        if($request->promo_code !="")
					{
			      		$order->promo_code_id = $promo_code->id;
			      	}else{
			      		$order->promo_code_id=null;
			      	}

					$order->save();
				}else{
					$statusCode = 422;
					$response  = array('msg' => 'This day does not have schedule! ');
			        return response()->json($response, $statusCode);
				}
					#after make order we will send notification to doctor on schedule
					$doctors_ids = (new ScheduleDoctorController)->get_doctor_schedule($schedule->schedule_id,$schedule->duration_id);

					foreach ($doctors_ids as $doctor_id) {
						$order_doctor = OrderDoctor::where('user_id',$doctor_id)->where('order_id',$order->id)->first();
						if(count($order_doctor)<1){
							$order_doctor = new OrderDoctor();
							$order_doctor->user_id=$doctor_id;
							$order_doctor->order_id=$order->id;
							$order_doctor->onShift=1;

							#create order to two doctors
							$order_doctor->status=0;
							$order_doctor->save();
						}
					}



					DB::commit();

					#get tokens register id
					$tokens = (new UserDeviceController)->get_tokens_by_ids($doctors_ids);
					#send notification to doctor on sheft
					$title   = "new request";
					$message = "\"".$admin->name."\" requested a consultation from you.";
					$type = 2;

					$notification = (new NotificationController)->send_notification($tokens,$title,$message,$type,$order);

					#after 15 minutes will send notification to all doctor in app
					//$this->dispatch( (new SendNotification($order->id))->delay(60 * 1) );

			        $job = ( new SendNotification($order->id , $admin->name) )->delay(Carbon::now()->addMinutes(15));
			        dispatch($job);

			        //shell_exec('php artisan queue:listen'); # php artisan queue:work


					$statusCode = 200;
	        		$response  = array('msg' => 'Done.' , 'order_id'=> $order->id);
					return response()->json($response, $statusCode);

	        }catch (Exception $e) {

			    DB::rollback();
			    $statusCode = 404;
    			$response  = array('msg' => 'Error!');
    			return response()->json($response, $statusCode);
			    // something went wrong
			}

		}


	}

//===================================================================================================
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$order = Order::findOrFail($id);

		return view('orders.show', compact('order'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$order = Order::findOrFail($id);

		return view('orders.edit', compact('order'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$order = Order::findOrFail($id);

		$order->user_id = $request->input("user_id");
        $order->rate = $request->input("rate");
        $order->review = $request->input("review");
        $order->status = $request->input("status");
        $order->doctor_notes = $request->input("doctor_notes");
        $order->doctor_report_problem = $request->input("doctor_report_problem");
        $order->package_id = $request->input("package_id");
        $order->kid_name = $request->input("kid_name");
        $order->kid_birthdate = $request->input("kid_birthdate");
        $order->kid_gender = $request->input("kid_gender");
        $order->complain = $request->input("complain");
        $order->patient_notes = $request->input("patient_notes");

        //$order->duration_call = $request->input("duration_call");

		$order->save();

		return redirect()->route('orders.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$order = Order::findOrFail($id);
		$order->delete();

		return redirect()->route('orders.index')->with('message', 'Item deleted successfully.');
	}

}
