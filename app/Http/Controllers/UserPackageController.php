<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserPackage;
class UserPackageController extends Controller
{
    //
    public function get_current_package($user)
	{
		$package = UserPackage::where('user_id', $user->id)->where("status",0)->first();
		//$package = Package::findOrFail($package->id);
		return $package;
	}

	public function check_current_package(Request $request)
	{
		$admin = (new AuthController)->get_user();
		$package = UserPackage::where('user_id', $admin->id)->where("status",0)->first();
		//$package = Package::findOrFail($package->id);
		//$package->setHidden(['no_calls']);
		if ($package) {
			$statusCode = 200;
			$response  = array('msg' => 'Done! ',"package"=>$package);
	        return response()->json($response, $statusCode);
		}else{
			$statusCode = 201;
			$response  = array('msg' => 'no data! ');
	        return response()->json($response, $statusCode);
		}

	}
}
