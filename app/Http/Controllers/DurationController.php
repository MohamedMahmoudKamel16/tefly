<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Duration;
use Illuminate\Http\Request;

class DurationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$durations = Duration::orderBy('id', 'desc')->paginate(10);

		return view('durations.index', compact('durations'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('durations.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$duration = new Duration();

		$duration->start_time = $request->input("start_time");
        $duration->end_time = $request->input("end_time");

		$duration->save();

		return redirect()->route('durations.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$duration = Duration::findOrFail($id);

		return view('durations.show', compact('duration'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$duration = Duration::findOrFail($id);

		return view('durations.edit', compact('duration'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$duration = Duration::findOrFail($id);

		$duration->start_time = $request->input("start_time");
        $duration->end_time = $request->input("end_time");

		$duration->save();

		return redirect()->route('durations.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$duration = Duration::findOrFail($id);
		$duration->delete();

		return redirect()->route('durations.index')->with('message', 'Item deleted successfully.');
	}

}
