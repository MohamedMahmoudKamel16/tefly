<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\OrderDoctor;
use App\Order;
use App\UserPackage;
use App\Patient;
use App\User;
use App\UserDevice;
use App\City;
use App\Country;


use DB;

class PatientController extends Controller {


//=========================<< Fawzy edits >>>==============================================
	public function AjaxBlock($id){

		$User = User::where('id' , $id)->first();

		if ($User->is_block == 0) {

			$User->is_block = 1;
			$User->save();
			echo '<i class="fa fa-lock" aria-hidden="true"></i>';

		}elseif ($User->is_block == 1) {

			$User->is_block = 0;
			$User->save();
			echo '<i class="fa fa-unlock-alt" aria-hidden="true"></i>';
		}

	}
//==================================================================================================================================================================
	public function previousCalls($patient_id)
	{
		$userDetails  = User::findOrFail($patient_id);
		$orders       = Order::where('user_id' , $patient_id )->orderBy('id', 'desc')->get();
		$user 		  = $userDetails->id;
		// return view('patients.calls', compact('user','orders'));
		return view('orders.index', compact('user','orders'));
	}
//==================================================================================================================================================================
	public function previousCallsDetails($order_id)
	{
		$order = Order::findOrFail($order_id);

		// return view( 'patients.callDetails', compact('order') );
		return view('orders.show', compact('order'));
	}
//==================================================================================================================================================================
	public function PackageHistory($patient_id){

		$user  	  = User::findOrFail($patient_id);
		$packages = UserPackage::where('user_id' , $patient_id )->orderBy('id', 'desc')->get();


		return view('patients.userPackages', compact('user','packages'));	

	}
//==================================================================================================================================================================
	public function EditPackageHistory($id){

		$packages = UserPackage::where('id' , $id )->orderBy('id', 'DESC')->first();
		
		return view('patients.editPackageHistory', compact('packages'));	

	}
//==================================================================================================================================================================
	public function storeEditPackageHistory(Request $request){

		$UserPackage = UserPackage::where('id' , $request->input("package_id") )->first();
		$UserPackage->num_calls = $request->input("num_calls");
		$UserPackage->status = $request->input("status");
		$UserPackage->save();

		$user  	  = User::findOrFail($UserPackage->user_id);
		$packages = UserPackage::where('user_id' , $UserPackage->user_id)->orderBy('id', 'desc')->get();
		return view('patients.userPackages', compact('user','packages'));	
	}
//==================================================================================================================================================================
	public function index()
	{
		$patients = Patient::orderBy('id', 'desc')->get();

		return view('patients.index', compact('patients'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{	
		$countries = Country::get();
		$cities    = City::get();
		return view('patients.create',compact('cities'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{

		$this->validate($request, [
	        'mobile' => 'required|unique:users|max:255',
	        'email' => 'required|unique:users|max:255',
	        'name' => 'required',
    	]);


		DB::beginTransaction();
    	try{
	   		$user = new User();
	        $user->name = $request->input("name");
	        $user->password = $request->input("password");
	        $user->email = $request->input("email");
	        $user->mobile = $request->input("mobile");
	        $user->type = 0;
	        $user->is_block = 0;
			$user->image="";

	   		$user->save();

	   		$patient = new Patient();
	        $patient->user_id = $user->id;
	        $patient->country = $request->input("country");
	        $patient->city = $request->input("city");
	        $patient->no_kids = $request->input("no_kids");
			$patient->save();
			DB::commit();
			return redirect()->route('patients.index')->with('message', 'Item created successfully.');
        }catch (Exception $e) {
		    DB::rollback();
		    return redirect()->route('patients.index')->with('message', 'There are some problem.');
		}
		

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$patient 	 = Patient::findOrFail($id);
		$user 		 = User::findOrFail($patient->user_id);
		$numOfCalls  = Order::where('user_id' , $patient->user_id )->orderBy('id', 'desc')->count();
		$platform    = UserDevice::where('user_id' , $patient->user_id )->value("platform");


		return view('patients.show', compact('patient','user','numOfCalls','platform'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$patient = Patient::findOrFail($id);
		$countries = Country::get();
		$cities    = City::get();

		return view('patients.edit', compact('patient','cities'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
	        'mobile' => 'required|max:255|unique:users,mobile,'.$id,
	        'email' => 'required|max:255|unique:users,email,'.$id,
	        'name' => 'required',
    	]);


		DB::beginTransaction();
    	try{
	   		$user = User::where('id',$id)->first();
	        $user->name = $request->input("name");
	     //   $user->password = $request->input("password");
	        $user->email = $request->input("email");
	        $user->mobile = $request->input("mobile");
			
	   		$user->save();

	   		$patient = Patient::where('user_id',$id)->first();
	   		if(!$patient){
	   			$patient = new Patient();
	   		}
	        
	        $patient->user_id = $user->id;
	        $patient->country = $request->input("country");
	        $patient->city = $request->input("city");
	        $patient->no_kids = $request->input("no_kids");
			$patient->save();
			DB::commit();
			return redirect()->route('patients.index')->with('message', 'Item created successfully.');
        }catch (Exception $e) {
		    DB::rollback();
		    return redirect()->route('patients.index')->with('message', 'There are some problem.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$patient = Patient::findOrFail($id);
		$patient->delete();

		return redirect()->route('patients.index')->with('message', 'Item deleted successfully.');
	}

}
