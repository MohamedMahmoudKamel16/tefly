<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$feedbacks = Feedback::orderBy('id', 'desc')->get();

		return view('feedback.index', compact('feedbacks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('feedback.create');
	}
	
	
	public function send_feedback(Request $request)
	{

		if(! isset($request->email)){
    		$statusCode = 420;
			$response  = array('msg' => 'Invalid Format!');
			return response()->json($response, $statusCode);

		}elseif(! isset($request->subject)){
			$statusCode = 420;
    		$response  = array('msg' => 'Invalid Format !');
    		return response()->json($response, $statusCode);

		}elseif(! isset($request->body)){
			$statusCode = 420;
    		$response  = array('msg' => 'Invalid Format !');
    		return response()->json($response, $statusCode);

		}else{
			$feedback = new Feedback();

			$feedback->subject = $request->subject;
	        $feedback->body = $request->body;
	        $feedback->email = $request->email;

			$feedback->save();
			$statusCode = 200;
    		$response  = array('msg' => 'Done!');
    		return response()->json($response, $statusCode);
		}

	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response send_feedback
	 */
	public function store(Request $request)
	{
		$feedback = new Feedback();

		$feedback->subject = $request->input("subject");
	        $feedback->body = $request->input("body");
	        $feedback->email = $request->input("email");

		$feedback->save();

		return redirect()->route('feedback.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$feedback = Feedback::findOrFail($id);

		return view('feedback.show', compact('feedback'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$feedback = Feedback::findOrFail($id);

		return view('feedback.edit', compact('feedback'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$feedback = Feedback::findOrFail($id);

		$feedback->subject = $request->input("subject");
        $feedback->body = $request->input("body");
        $feedback->email = $request->input("email");

		$feedback->save();

		return redirect()->route('feedback.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$feedback = Feedback::findOrFail($id);
		$feedback->delete();

		return redirect()->route('feedback.index')->with('message', 'Item deleted successfully.');
	}

}
