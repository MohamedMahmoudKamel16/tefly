<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
//use Request;
use App\Http\Requests;
use JWTAuthException;

use App\Order;
use App\OrderDoctor;
use App\Doctor;
use App\Patient;
use App\UserDevice;
use App\UserPackage;
use App\User;
use JWTAuth;
use App\Address;
use App\pachage;
use Mail;
use File;
use DB;
use App\CreditDoctor;

class AuthController extends Controller
{

#list users
	public function index()
	{
		$users = User::orderBy('id', 'desc')->get();

		return view('auth.index', compact('users'));

	}

	public function search_users($key){
		$users = User::where('mobile', 'LIKE', $key."%")->get();
		//return $users;
		return $users;
	}
	public function create()
	{
		return view('auth.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{

		 $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'email|max:255|unique:users',
            'mobile' => 'required|string|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
		$user = new User();

		$user->name = $request->input("name");
		$user->email = $request->input("email");
		$user->mobile = $request->input("mobile");
		$user->password = $request->input("password");
		$user->is_admin = 0;
		$user->is_block = 0;
		//$user->email = $request->input("email");
		$user->save();

		return redirect()->route('users.index')->with('message', 'User added successfully.');
	}

	public function block_user(Request $request)
	{
		$user = User::findOrFail($request->user_id);

	    if($user->is_block == 1){
	        $user->is_block = 0;
	        $msg = " user is unblocked successfully";


	    } else {
	        $user->is_block = 1;
	        $msg = "user is blocked  successfully";

	    }

	 //   session(['message' => $msg]);

	    return response()->json([
	      'data' => [
	        'success' => $user->save(),
	        'message'=> $msg,
	      ]
	    ]);

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);

		return view('auth.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::findOrFail($id);

		return view('auth.edit', compact('user'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{

		 $this->validate($request, [
            'name' => 'string|max:255',
            'email' => 'email|max:255|unique:users,email,'.$id,
            'mobile' => 'required|string|max:255|unique:users,mobile,'.$id,
        ]);
		$user = User::findOrFail($id);

		$user->name = $request->input("name");
		$user->email = $request->input("email");
		$user->mobile = $request->input("mobile");
		//$user->password = $request->input("password");

		$user->save();

		return redirect()->route('users.index')->with('message', 'User is updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$user->delete();

		return redirect()->route('users.index')->with('message', 'Item deleted successfully.');
	}
	/* End ADMin*/

    #forget password
	public function forgetPassword(Request $request)
	{
    $rand = '';

    if(! isset($request->email)){
      $statusCode = 420;
      $response  = array('msg' => 'Invalid Format! ');
      return response()->json($response, $statusCode);
    }

		$user = User::where('email',$request->email)->where('type', 0)->first();
		if (! $user) {
      return Response()->json(['msg' => 'Email does not exist !'], 421);
		}

    $rand   = str_random(9);
    $emails = [$request->email];
    Mail::send([], [], function($message) use ($emails, $rand)
    {
        $message->from('support@e7gezlyapp.com')->to($emails)->subject('Forget Password')->setBody("Your Password is changed to " . $rand);
    });
    $user->password = $rand;
    $user->save();

    return Response()->json(['msg' => 'please check your email !'], 200);
	}


	public function register(Request $request)
	{
		$values=$request->all();

		#set status code by default 200
		$statusCode=200;
		$response = array();

		try {

			if(! isset($request->email)){
        			$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->mobile)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format !');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->password)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format !');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->name)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->country)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->city)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->no_kids)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->register_id)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->device_id)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->platform)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->version)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}else{
				#get all data from user
				$values=$request->all();

				$check_attribute="mobile";
				$check_data=$request->mobile;

				$check_attribute1="email";
				$check_data1=$request->email;

				$userCheck = User::where($check_attribute,$check_data)->first();
				$userCheck1 = User::where($check_attribute1,$check_data1)->first();

			//	var_dump($userCheck);die();
				if( count($userCheck) >0 && $check_data != "" ){
					$statusCode = 421;
					$response=array("msg"=>"This ".$check_attribute." already exist!");
					return response()->json($response, $statusCode);

				}elseif ( count($userCheck1) >0 ) {

						$statusCode = 422;
						$response=array("msg"=>"This ".$check_attribute1." already exist!");
						return response()->json($response, $statusCode);


				}else{

					$values['is_block'] = 0;

					DB::beginTransaction();
    				try{
						$user = User::create($values);
						if(! empty($user))
						{

							$patient = new Patient();
							$patient->user_id=$user->id;
					        $patient->country = $request->country;
					        $patient->city = $request->city;
					        $patient->no_kids = $request->no_kids;
							$patient->save();
							$check_device=UserDevice::where('user_id',$user->id)->where('device_id',$request->device_id)->first();

							if(count($check_device)>0){

								$check_device->user_id=$user->id;
								$check_device->register_id=$request->register_id;
								$check_device->is_active=1;
								$check_device->platform=$request->platform;
								$check_device->version=$request->version;
								$check_device->save();
							}else{

							$st_device=array("register_id"=>$request->register_id,"device_id"=>$request->device_id,"user_id"=>$user->id,"is_active"=>1,"platform"=>$request->platform,"version"=>$request->version);
								$user_devices=UserDevice::create($st_device);
							}


						}else{
							$statusCode = 404;
		        			$response  = array('msg' => 'Error!');
		        			return response()->json($response, $statusCode);

						}


						DB::commit();
						$statusCode = 200;
							$customClaims=['device_id'=>$request->device_id];
							$token = JWTAuth::fromUser($user,$customClaims);
							$user->token=$token;

							#data of patient
					        $user->country = $request->country;
					        $user->city = $request->city;
					        $user->no_kids = $request->no_kids;

			        		$response  = array('msg' => 'Done.','user' => $user);
							return response()->json($response, $statusCode);
			        }catch (Exception $e) {
					    DB::rollback();
					    $statusCode = 404;
	        			$response  = array('msg' => 'Error!');
	        			return response()->json($response, $statusCode);
					    // something went wrong
					}

				}
			}

		} catch (Exception $e) {
			return response()->json(['msg' => 'Error'], 425);
		}finally{
            return response()->json($response, $statusCode);
    	}

	}
	public function logout(Request $request)
	{
		try {
				$response=array();
				$statusCode=200;

	        	$admin=$this->get_user();
	        	$token = JWTAuth::getToken();
	        	$claims = JWTAuth::getJWTProvider()->decode($token);
				$device=$claims['device_id'] ;
				$user_dev=UserDevice::where('user_id',$admin->id)->where("device_id",$device)->first();
				if(count($user_dev)>0){
					$user_dev->is_active=0;
					$user_dev->register_id = " ";
					$user_dev->save();
				}
				JWTAuth::invalidate(JWTAuth::getToken());

			$statusCode = 200;
			$response  = array('msg' => 'Logout Done!');

		}finally{
            		return response()->json($response, $statusCode);
  		}
	}

	public function login(Request $request)
	{
		$token = null;
		try {
		//echo("kgkg");

			#get all data from user
			$check_attribute="mobile";
			$check_data=$request->mobile;
			#set status code by default 200
			$statusCode=200;
			$response = array();

			if(! isset($request->mobile)){
        		$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->password)){
				$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->register_id)){
				$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->device_id)){
				$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->platform)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->version)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif($request->mobile ==""){
				$statusCode = 423;
        		$response  = array('msg' => 'Mobile required!');
        		return response()->json($response, $statusCode);

			}elseif($request->password ==""){
				$statusCode = 424;
        		$response  = array('msg' => 'password required!');
        		return response()->json($response, $statusCode);

			}else{
				$user = User::where($check_attribute,$check_data)->first();

				if(count($user) >0){

					$doctorCheck = User::where($check_attribute,$check_data)->where('type',0)->first();

					if( ! $doctorCheck){
						$statusCode = 427;
		        		$response  = array('msg' => 'you does not allow to login as a patient');
		        		return response()->json($response, $statusCode);

					}

					$userCheckPassword =password_verify($request->password ,$user->password );

					if ($userCheckPassword == true) {
						if($user->is_block == 1){
							$statusCode = 456;
        						$response  = array('msg' => 'This user is blocked from app');
        						return response()->json($response, $statusCode);

						}

						$check_device=UserDevice::where('user_id',$user->id)->where('device_id',$request->device_id)->first();

						if(count($check_device)>0){

							$check_device->user_id=$user->id;
							$check_device->register_id=$request->register_id;
							$check_device->is_active=1;
							$check_device->platform=$request->platform;
							$check_device->version=$request->version;
							$check_device->save();
						}else{

						$st_device=array("register_id"=>$request->register_id,"device_id"=>$request->device_id,"user_id"=>$user->id,"is_active"=>1,"platform"=>$request->platform,"version"=>$request->version);
							$user_devices=UserDevice::create($st_device);
						}


	  					$statusCode = 200;

						$customClaims=['device_id'=>$request->device_id];
						$token = JWTAuth::fromUser($user,$customClaims);
						$user->token=$token;
					//	$user = JWTAuth::parseToken()->authenticate()
						$patient = Patient::where('user_id',$user->id)->first();
						if($patient)
						{
							$user->country = $patient->country;
					        $user->city = $patient->city;
					        $user->no_kids = $patient->no_kids;
						}else{
							$user->country = "";
					        $user->city = "";
					        $user->no_kids = 0;
						}

						$package=  UserPackage::where('user_id',$user->id)->where('status',0)->first();
						if( !$package){
							$package = new UserPackage();
						}

					//	$user->package =$package;

		        		$response  = array('msg' => 'Done.','user' => $user);
		        		return response()->json($response, $statusCode);

					}else{
						$statusCode = 421;
						$response=array("msg"=>"This password is wrong !");
						return response()->json($response, $statusCode);
					}

				}else{
	  				$statusCode = 422;
					$response=array("msg"=>"This ".$check_attribute." does not exist !");
					return response()->json($response, $statusCode);

				}
			}
		}catch (\Exception $e) {
  			$statusCode = 426;
        	$response  = array('msg' => 'Bad Request, Error!');
            return response()->json($response, $statusCode);

  		}

	}


    public function refresh_token(Request $request){
    	try {
    		$response=array();
    		$statusCode=200;

    		if(! isset($request->device_id)){
        		$statusCode = 422;
        		$response  = array('msg' => 'Invalid Format!');

			}else{

				$token = JWTAuth::getToken();
				$claims = JWTAuth::getJWTProvider()->decode($token);

				if ($claims['device_id'] == $request->device_id) {

					$user_device=UserDevice::where('device_id',$request->device_id)->first();
					if(count($user_device)>0 ){

						$user=User::where('id',$user_device->user_id)->first();
						if (count($user)>0) {

							$customClaims=['device_id'=>$request->device_id];
							$token = JWTAuth::fromUser($user,$customClaims);


							$statusCode = 200;
							$response  = array('msg' => 'Refresh Done !','token'=> $token);
						}else{

							$statusCode = 422;
							$response  = array('msg' => 'try to login again !');

						}

					}else{
						$statusCode = 422;
						$response  = array('msg' => 'try to login again !');
					}


				}else{
					$statusCode = 422;
					$response  = array('msg' => 'try to login again !');

				}

			}


	    }
		finally{
			return response()->json($response, $statusCode);

		}
	}


	public function update_version(Request $request)
	{
		$user = $this->get_user();

		if(! $request->input('version')){
			return response()->json([
				'msg' => 'Invalid format',
			], 420);
		}

		$user->update(['version' => $request->input('version')]);
		return response()->json([
			'msg' => 'Version was updated',
			'user' => $user
		], 200);
	}


	#user update his data
	public function update_data(Request $request)
	{
		#get all data from user
		$values=$request->all();
		$check_attribute="email";
		$check_data=$request->email;

		$check_attribute1="mobile";
		$check_data1=$request->mobile;
		#set status code by default 200
		$statusCode=200;
		$response = array();
		try {

			if(! isset($request->email)){
        			$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->mobile)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format !');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->name)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->country)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->city)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->no_kids)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}else{

				#get user from email
				$userCheck = User::where($check_attribute,$check_data)->first();

				#get user from mobile
				$userCheckM = User::where($check_attribute1,$check_data1)->first();


				#get user from token
				$user=$this->get_user();


				if($user){
					if(count($userCheck)>0){
						if($userCheck->id != $user->id){
							$statusCode = 421;
		       				$response=array("msg"=>"This ".$check_attribute." has already registered with another user !");
	            			return response()->json($response, $statusCode);

						}
					}

					if(count($userCheckM)>0){
						if($userCheckM->id != $user->id){
							$statusCode = 422;
	   						$response=array("msg"=>"This ".$check_attribute1." has already registered with another user !");
	            					return response()->json($response, $statusCode);

						}
					}


			        $user->fill($values);
			        $user->save();

			        #update data of patient
			        $patient = Patient::where('user_id',$user->id)->first();
					if(! $patient)
					{
						$patient = new Patient();
					}

					$patient->country = $request->country;
			        $patient->city = $request->city;
			        $patient->no_kids = $request->no_kids;
			        $patient->save();

				    $patient = Patient::where('user_id',$user->id)->first();
					if($patient)
					{
						$user->country = $patient->country;
				        $user->city = $patient->city;
				        $user->no_kids = $patient->no_kids;
					}else{
						$user->country = "";
				        $user->city = "";
				        $user->no_kids = 0;
					}

			        $user->token="";
			        $statusCode = 200;
					$response  = array('msg' => 'Done.','user' => $user);
    				return response()->json($response, $statusCode);


			    }else{
					$statusCode = 423;
   					$response=array("msg"=>"You are not login !");
            		return response()->json($response, $statusCode);

				}

			}
		} catch (Exception $e) {
  			$statusCode = 424;
        	$response  = array('msg' => 'Bad Request, Error!');
            		return response()->json($response, $statusCode);

  		}

	   }


	public function update_password(Request $request){
		#set status code by default 200
		$statusCode=200;
		$response = array();
		try {
			if(! isset($request->password)){
        		$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');

			}elseif(! isset($request->old_password)){
				$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');

			}else{
				$user = $this->get_user();
				if($user) {

					$checkPassword=password_verify($request->old_password ,$user->password );

					if($checkPassword == false ){
						$statusCode = 421;

						$response=array("msg"=>"This password is wrong !");

					}else{
						$user->password=$request->password;
						$user->save();
						$statusCode = 200;
						$response=array("msg"=>"password is updated !");


					}
				}else{
					$statusCode = 422;
					$response=array("msg"=>"This user does not exist!");
				}
			}
		} catch (Exception $e) {
  			$statusCode = 423;
        	$response  = array('msg' => 'Bad Request, Error!');

  		}finally{
            return response()->json($response, $statusCode);
  		}

	}


	public function get_user(){
		$user = JWTAuth::parseToken()->authenticate();
		if(count($user) >0){
	    	return $user;

		}else{
			$statusCode = 455;
			$response  = array('msg' => 'This user does not exist!');
			return response()->json($response, $statusCode);
		}
	}


	#block user ^_^
	public function check_block(){
		#get user from token
		$admin=$this->get_user();
		$statusCode = 200;
		$block = (int)$admin->is_block;
		$response  = array('msg' => 'Done',"is_block"=>$block);
		return Response()->json($response,$statusCode);

	}

		#get user by id ^_^
	public function get_user_byid(Request $request){
		if(! isset($request->user_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format !');
			return response()->json($response, $statusCode);

		}else{

			#get user from token
			$admin=$this->get_user();
			$user = User::where("id",$request->user_id)->first();
			if(count($user)>0){
				$statusCode = 200;
				$response=array("msg"=>"Done!","user"=>$user);
				return response()->json($response, $statusCode);

			}else{

				$statusCode = 421;
				$response=array("msg"=>"This user does not exist!");
				return response()->json($response, $statusCode);

			}
		}

	}

	public function register_doctor(Request $request)
	{
		$values=$request->all();

		#set status code by default 200
		$statusCode=200;
		$response = array();

		try {

			if(! isset($request->email)){
        			$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->mobile)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format !');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->password)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format !');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->name)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->title)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->bio)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->no_kids)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->register_id)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->device_id)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->platform)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->version)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->image)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}else{
				#get all data from user
				$values=$request->all();

				$check_attribute="mobile";
				$check_data=$request->mobile;

				$check_attribute1="email";
				$check_data1=$request->email;

				$userCheck = User::where($check_attribute,$check_data)->first();
				$userCheck1 = User::where($check_attribute1,$check_data1)->first();

			//	var_dump($userCheck);die();
				if( count($userCheck) >0 && $check_data != "" ){
					$statusCode = 421;
					$response=array("msg"=>"This ".$check_attribute." already exist!");
					return response()->json($response, $statusCode);

				}elseif ( count($userCheck1) >0 ) {

						$statusCode = 422;
						$response=array("msg"=>"This ".$check_attribute1." already exist!");
						return response()->json($response, $statusCode);


				}else{

					$values['is_block'] = 0;

					#type == 1 as adoctor
					$values['type'] = 1;

					DB::beginTransaction();
    				try{
						$user = User::create($values);
						if(! empty($user))
						{

							$doctor = new Doctor();
					        $doctor->password = $request->password;
					        $doctor->title = $request->title;
					        $doctor->bio = $request->bio;
							$doctor->save();

							$check_device=UserDevice::where('user_id',$user->id)->where('device_id',$request->device_id)->first();

							if(count($check_device)>0){

								$check_device->user_id=$user->id;
								$check_device->register_id=$request->register_id;
								$check_device->is_active=1;
								$check_device->platform=$request->platform;
								$check_device->version=$request->version;
								$check_device->save();
							}else{

								$st_device=array("register_id"=>$request->register_id,"device_id"=>$request->device_id,"user_id"=>$user->id,"is_active"=>1,"platform"=>$request->platform,"version"=>$request->version);
								$user_devices=UserDevice::create($st_device);
							}

						}else{
							$statusCode = 404;
		        			$response  = array('msg' => 'Error!');
		        			return response()->json($response, $statusCode);

						}


						DB::commit();
						$statusCode = 200;
							$customClaims=['device_id'=>$request->device_id];
							$token = JWTAuth::fromUser($user,$customClaims);
							$user->token=$token;
			        		$response  = array('msg' => 'Done.','user' => $user);
							return response()->json($response, $statusCode);
			        }catch (Exception $e) {
					    DB::rollback();
					    $statusCode = 404;
	        			$response  = array('msg' => 'Error!');
	        			return response()->json($response, $statusCode);
					    // something went wrong
					}

				}
			}

		} catch (Exception $e) {
			return response()->json(['msg' => 'Error'], 425);
		}finally{
            return response()->json($response, $statusCode);
    	}

	}

	public function login_doctor(Request $request)
	{
		$token = null;
		try {
		//echo("kgkg");

			#get all data from user
			$check_attribute="mobile";
			$check_data=$request->mobile;
			#set status code by default 200
			$statusCode=200;
			$response = array();

			if(! isset($request->mobile)){
        		$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->password)){
				$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->register_id)){
				$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->device_id)){
				$statusCode = 420;
        		$response  = array('msg' => 'Invalid Format!');
        		return response()->json($response, $statusCode);


			}elseif(! isset($request->platform)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->version)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif($request->mobile ==""){
				$statusCode = 423;
        		$response  = array('msg' => 'Mobile required!');
        		return response()->json($response, $statusCode);

			}elseif($request->password ==""){
				$statusCode = 424;
        		$response  = array('msg' => 'password required!');
        		return response()->json($response, $statusCode);

			}else{
				$user = User::where($check_attribute,$check_data)->first();


				if(count($user) >0){
					$doctorCheck = User::where($check_attribute,$check_data)->where('type',1)->first();

					if( ! $doctorCheck){
						$statusCode = 427;
		        		$response  = array('msg' => 'you does not allow to login as a doctor');
		        		return response()->json($response, $statusCode);

					}
					$userCheckPassword =password_verify($request->password ,$user->password );

					if ($userCheckPassword == true) {
						if($user->is_block == 1){
							$statusCode = 456;
        						$response  = array('msg' => 'This user is blocked from app');
        						return response()->json($response, $statusCode);

						}

						$check_device=UserDevice::where('user_id',$user->id)->where('device_id',$request->device_id)->first();

						if(count($check_device)>0){

							$check_device->user_id=$user->id;
							$check_device->register_id=$request->register_id;
							$check_device->is_active=1;
							$check_device->platform=$request->platform;
							$check_device->version=$request->version;
							$check_device->save();
						}else{

							$st_device=array("register_id"=>$request->register_id,"device_id"=>$request->device_id,"user_id"=>$user->id,"is_active"=>1,"platform"=>$request->platform,"version"=>$request->version);
							$user_devices=UserDevice::create($st_device);
						}


	  					$statusCode = 200;

						$customClaims=['device_id'=>$request->device_id];
						$token = JWTAuth::fromUser($user,$customClaims);
						$user->token=$token;
					//	$user = JWTAuth::parseToken()->authenticate()

						$doctor          = Doctor::where('user_id',$user->id)->first();

						if($doctor)
						{

							$DoctorOrdersIds = OrderDoctor::where('user_id',$user->id)->pluck('order_id')->toArray();
							$DoctorRate      = Order::whereIn('id' , $DoctorOrdersIds )->where('status',3)->avg('rate');

							$DoctorCredit    = CreditDoctor::where('user_id' , $user->id)->value("calls_credit");
							$DoctorNumCalls  = ( (int)$DoctorCredit ) / 2;
							// $DoctorNumCalls  = Order::whereIn('id' , $DoctorOrdersIds )->where('status',3)->count();
							// $DoctorCredit    = $DoctorNumCalls * 2;

							if ($DoctorRate == null) {
								$DoctorRate = 0;
							}
								$user->bio = $doctor->bio;
						        $user->title = $doctor->title;
						        $user->clinic_address = $doctor->clinic_address;
						        $user->clinic_phone = $doctor->clinic_phone;
						        $user->website_link = $doctor->website_link;
						        $user->doctorRate = round($DoctorRate);
						        $user->doctorNumCalls = $DoctorNumCalls;
						        $user->doctorCredit = $DoctorCredit;
						}else{
								$user->bio = "";
						        $user->title = "";
						        $user->clinic_address = "";
						        $user->clinic_phone = "";
						        $user->website_link = "";
						        $user->doctorRate = "";
						        $user->doctorNumCalls = "";
						        $user->doctorCredit = "";
						}

		        		$response  = array('msg' => 'Done.','user' => $user);
		        		return response()->json($response, $statusCode);

					}else{
						$statusCode = 421;
						$response=array("msg"=>"This password is wrong !");
						return response()->json($response, $statusCode);
					}

				}else{
	  				$statusCode = 422;
					$response=array("msg"=>"This ".$check_attribute." does not exist !");
					return response()->json($response, $statusCode);

				}
			}
		}catch (\Exception $e) {
  			$statusCode = 426;
        	$response  = array('msg' => 'Bad Request, Error!');
            return response()->json($response, $statusCode);

  		}

	}

	#user update doctor data
	public function update_data_doctor(Request $request)
	{
		#get all data from user
		$values=$request->all();
		$check_attribute="email";
		$check_data=$request->email;

		$check_attribute1="mobile";
		$check_data1=$request->mobile;
		#set status code by default 200
		$statusCode=200;
		$response = array();
		try {

			if(! isset($request->email)){
        			$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->mobile)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format !');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->name)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->title)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->bio)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}elseif(! isset($request->image)){
				$statusCode = 420;
        			$response  = array('msg' => 'Invalid Format!');
        			return response()->json($response, $statusCode);

			}else{

				#get user from email
				$userCheck = User::where($check_attribute,$check_data)->first();

				#get user from mobile
				$userCheckM = User::where($check_attribute1,$check_data1)->first();


				#get user from token
				$user=$this->get_user();


				if($user){
					if(count($userCheck)>0){
						if($userCheck->id != $user->id){
							$statusCode = 421;
		       				$response=array("msg"=>"This ".$check_attribute." has already registered with another user !");
	            			return response()->json($response, $statusCode);

						}
					}

					if(count($userCheckM)>0){
						if($userCheckM->id != $user->id){
							$statusCode = 422;
	   						$response=array("msg"=>"This ".$check_attribute1." has already registered with another user !");
	            					return response()->json($response, $statusCode);

						}
					}

				    if($request->image != ""){
			        	$decoded_img = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->image ));

						$f = finfo_open();

						$mime_type = finfo_buffer($f, $decoded_img, FILEINFO_MIME_TYPE);
						$ext = explode("/", $mime_type);
						$image =time().".".$ext[1];

						file_put_contents(public_path('images/users/').$image, $decoded_img);

						$values['image']=$image;
			        }else{
			        	$values['image']=$user->image;
			        }


			        $user->fill($values);
			        $user->save();





			        #update data of doctor
			        $doctor = Doctor::where('user_id',$user->id)->first();
					if(! $doctor)
					{
						$doctor = new Doctor();
					}

					$doctor->bio = $request->bio;
			        $doctor->title = $request->title;
			        $doctor->user_id = $user->id;


			        $doctor->save();

				    $doctor = Doctor::where('user_id',$user->id)->first();
					if($doctor)
						{
							$user->bio = $doctor->bio;
					        $user->title = $doctor->title;
					        $user->clinic_address = $doctor->clinic_address;
					        $user->clinic_phone = $doctor->clinic_phone;
					        $user->website_link = $doctor->website_link;
						}else{
							$user->bio = "";
					        $user->title = "";
					        $user->clinic_address = "";
					        $user->clinic_phone = "";
					        $user->website_link = "";
						}

			        $user->token="";
			        $statusCode = 200;
					$response  = array('msg' => 'Done.','user' => $user);
    				return response()->json($response, $statusCode);


			    }else{
					$statusCode = 423;
   					$response=array("msg"=>"You are not login !");
            		return response()->json($response, $statusCode);

				}

			}
		} catch (Exception $e) {
  			$statusCode = 424;
        	$response  = array('msg' => 'Bad Request, Error!');
            		return response()->json($response, $statusCode);

  		}

	   }
}
