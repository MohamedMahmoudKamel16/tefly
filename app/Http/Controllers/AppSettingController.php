<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\AppSetting;
use Illuminate\Http\Request;

class AppSettingController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$app_settings = AppSetting::orderBy('id', 'desc')->paginate(10);

		return view('app_settings.index', compact('app_settings'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('app_settings.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$app_setting = new AppSetting();

		$app_setting->key = $request->input("key");
        	$app_setting->value = $request->input("value");

		$app_setting->save();

		return redirect()->route('app_settings.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$app_setting = AppSetting::findOrFail($id);

		return view('app_settings.show', compact('app_setting'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$app_setting = AppSetting::findOrFail($id);

		return view('app_settings.edit', compact('app_setting'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$app_setting = AppSetting::findOrFail($id);

		//$app_setting->key = $request->input("key");
        	$app_setting->value = $request->input("value");

		$app_setting->save();

		return redirect('app_settings/'.$id.'/edit')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$app_setting = AppSetting::findOrFail($id);
		$app_setting->delete();

		return redirect()->route('app_settings.index')->with('message', 'Item deleted successfully.');
	}
	public function force_update()
	{
		$android_patient_version_code = AppSetting::where("key","android_patient_version_code")->value("value");
		$android_doctor_version_code = AppSetting::where("key","android_doctor_version_code")->value("value");

		$ios_patient_version_code = AppSetting::where("key","ios_patient_version_code")->value("value");
		$ios_doctor_version_code = AppSetting::where("key","ios_doctor_version_code")->value("value");

		$url = AppSetting::where("key","url")->value("value");
	//	$shipping_cost = (float)AppSetting::where("key","shipping_cost")->value("value");

		$response = array('msg' => 'Done.',"force_update_android"=>(int)$android_patient_version_code,"android_doctor_version_code"=>(int)$android_doctor_version_code,"ios_patient_version_code"=>(int)$ios_patient_version_code,"ios_doctor_version_code"=>(int)$ios_doctor_version_code,"url"=>$url);			
		
		$statusCode = 200;
		return response()->json($response, $statusCode);
	}

	public function force_update_new()
        {
                $patient_android = AppSetting::where("key","patient_android")->value("value");
                $doctor_android = AppSetting::where("key","doctor_android")->value("value");
                $patient_ios = AppSetting::where("key","patient_ios")->value("value");
                $doctor_ios = AppSetting::where("key","doctor_ios")->value("value");
                $url = AppSetting::where("key","url")->value("value");

                $response = array(
			'msg' => 'Done.',
			"patient_android"=>(int)$patient_android,
			"doctor_android"=>(int)$doctor_android,
			"patient_ios"=>(int)$patient_ios,
			"doctor_ios"=>(int)$doctor_ios,
			"url"=>$url
		);

                $statusCode = 200;
                return response()->json($response, $statusCode);
        }

}
