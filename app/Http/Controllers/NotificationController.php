<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use App\Http\Controllers\UserDeviceController;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
class NotificationController extends Controller {

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('notifications.create');
	}

//==============================================================================================

	public function store(Request $request){

		$this->validate($request, ['title' => 'required','body' => 'required']);
		$title=$request->input("title");
		$message=$request->input("body");
		$type=$request->input("type");
		$type_not=$request->input("type_not");
		$userType=$request->input("userType");

		if($type == "1"){

			$id=$request->input("user_id");
			 $this->validate($request, [
			        'user_id' => 'required|exists:users,mobile',
			    ]);
			 $user=User::where('mobile',$id)->first();

			 $tokens= (new UserDeviceController)->get_tokens_by_id($user->id , $userType);

		}elseif($type == "3"){

			 $tokens= (new UserDeviceController)->get_tokens_by_type('android', $userType);

		}elseif($type == "4"){

			 $tokens= (new UserDeviceController)->get_tokens_by_type('ios', $userType);

		}else{
			$tokens = (new UserDeviceController)->index($userType);
			//
		}
		if(count($tokens)>0){

			$topicResponse = $this->send_notification_type($tokens,$title,$message,$type_not);

			return redirect('notifications/create')->with('message', 'Notification sent successfully !');
		}

	}


//==============================================================================================

	public function send_notification_type($tokens,$title,$message,$type)
	{
		$optionBuiler = new OptionsBuilder();
		$optionBuiler->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder($title);
		$notificationBuilder->setBody($message)
		                    ->setSound('default');

		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData(['type' => $type]);
		$data = $dataBuilder->build();


		$option = $optionBuiler->build();
		$notification = $notificationBuilder->build();

		#send null if we need to send notification body

		//var_dump($tokens);die();

		if(count($tokens)>0){

			$downstreamResponse = FCM::sendTo($tokens, null,$notification,$data);
			if($downstreamResponse->numberSuccess()){
				return true;
			}else{
				return false;
			}

		}

	}


//==============================================================================================

	public function send_notification($tokens, $title, $message, $type, $order)
	{
		// echo"send notification";
		// var_dump($tokens).'<br>'.var_dump($title).'<br>'.var_dump($message).'<br>'.var_dump($type).'<br>'.var_dump($order);

		$optionBuiler = new OptionsBuilder();
		$optionBuiler->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder($title);

		if ($type == 2) {
			$notificationBuilder->setBody($message)
			                    ->setSound('ringing.mp3'); #use this ring in new request
		}else{
			$notificationBuilder->setBody($message)
		                    ->setSound('default');
		}


		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData(['type' => $type, 'title' => $title, 'message' => $message, 'body' => $message, 'order' => $order]);
		$data = $dataBuilder->build();


		$option = $optionBuiler->build();
		$notification = $notificationBuilder->build();


		if(count($tokens)>0){

			$downstreamResponse = FCM::sendTo($tokens, null,$notification,$data);
			if($downstreamResponse->numberSuccess()){
				return true;
			}else{
				return false;
			}

		}

	}

//==============================================================================================
//==============================================================================================

	public function send_notification_queue($token,$title,$message,$type)
	{
		// echo"send notification";
		// var_dump($tokens).'<br>'.var_dump($title).'<br>'.var_dump($message).'<br>'.var_dump($type).'<br>'.var_dump($order);

		$title = str_replace("_"," ",$title);
		$message = str_replace("_"," ",$message);

		$optionBuiler = new OptionsBuilder();
		$optionBuiler->setTimeToLive(60*20);

		$notificationBuilder = new PayloadNotificationBuilder($title);

		if ($type == 2) {

			$notificationBuilder->setBody($message)
			                    ->setSound('ringing.mp3');#use this ring in new request
		}else{

			$notificationBuilder->setBody($message)
		                    ->setSound('default');
		}


		$dataBuilder = new PayloadDataBuilder();
		$dataBuilder->addData(['type' => $type , 'title' => $title  , 'message' => $message ,'body' => $message]);
		$data = $dataBuilder->build();


		$option = $optionBuiler->build();
		$notification = $notificationBuilder->build();


		if(count([$token])>0){

			$downstreamResponse = FCM::sendTo([$token], null,$notification,$data);
			if($downstreamResponse->numberSuccess()){
				echo "true";
			}else{
				echo "false";
			}

		}

	}

//===============================================================================================

}
