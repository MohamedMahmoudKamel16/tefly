<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Country;
use App\City;

class CountryCityController extends Controller
{

//================================================================================================================
	public function index()
	{
		$CountryCity = City::orderBy('id', 'desc')->get();
		return view('cities.index', compact('CountryCity'));
	}

//================================================================================================================
	public function create()
	{	
		$countries  = Country::get();
		return view( 'cities.create' , compact('cities' , 'countries') );
	}
//================================================================================================================	
	public function store(Request $request)
	{
		$CountryCity = new City();
		$CountryCity->name = $request->input("name");
        $CountryCity->country_id = $request->input("country_id");
		$CountryCity->save();

		return redirect()->route('CountryCity.index')->with('message', 'City created successfully.');
	}

//================================================================================================================
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$CountryCity = City::findOrFail($id);

		return view('cities.show', compact('CountryCity'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	
		$countries  = Country::get();
		$CountryCity = City::findOrFail($id);

		return view('cities.edit', compact('CountryCity' , 'countries'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$CountryCity = City::findOrFail($id);
		$CountryCity->name = $request->input("name");
        $CountryCity->country_id = $request->input("country_id");
		$CountryCity->save();

		return redirect()->route('CountryCity.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$CountryCity = City::findOrFail($id);
		$CountryCity->delete();

		return redirect()->route('CountryCity.index')->with('message', 'Item deleted successfully.');
	}


//=============================================================================
    public function countries(){

    	$countries  = Country::get();

		$statusCode = 200;
		$response   = array('msg' => 'Done' , 'countries' => $countries);
		return response()->json($response, $statusCode);
    }

//=============================================================================

    public function cities(Request $request){

		if(! isset($request->country_id)){

			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);	

		}else{
			
			$countryFound  = Country::where('id' , $request->country_id)->get();

	    	if (count($countryFound) > 0) {

		    	$cities  = City::where('country_id' , $request->country_id )->get();
				$statusCode = 200;
				$response   = array('msg' => 'Done' , 'cities' => $cities);
				return response()->json($response, $statusCode);

	    	}else{

	    		$statusCode = 405;
				$response   = array('msg' => 'Country not found !');
				return response()->json($response, $statusCode);

	    	}
		}

    }
//=============================================================================


//=============================================================================
}
