<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\PromoCode;
use Illuminate\Http\Request;

class PromoCodeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$promo_codes = PromoCode::orderBy('id', 'desc')->get();

		return view('promo_codes.index', compact('promo_codes'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('promo_codes.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
	        'code' => 'required|unique:promo_codes|max:255',
	        'type' => 'required',
    	]);
		$promo_code = new PromoCode();

		$promo_code->code = $request->input("code");
        $promo_code->status = $request->input("status");
        $promo_code->type = $request->input("type");
        if ( $request->input("discount") == null ) {
        	$promo_code->discount = 0;
        }else{
        	$promo_code->discount = $request->input("discount");
        }

		$promo_code->save();

		return redirect()->route('promo_codes.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$promo_code = PromoCode::findOrFail($id);

		return view('promo_codes.show', compact('promo_code'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$promo_code = PromoCode::findOrFail($id);

		return view('promo_codes.edit', compact('promo_code'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$promo_code = PromoCode::findOrFail($id);

		$promo_code->code = $request->input("code");
        $promo_code->status = $request->input("status");
        $promo_code->type = $request->input("type");
        $promo_code->discount = $request->input("discount");

		$promo_code->save();

		return redirect()->route('promo_codes.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$promo_code = PromoCode::findOrFail($id);
		$promo_code->delete();

		return redirect()->route('promo_codes.index')->with('message', 'Item deleted successfully.');
	}

}
