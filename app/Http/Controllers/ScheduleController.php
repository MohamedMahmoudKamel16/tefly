<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ScheduleDoctorsHistory;
use App\CreditDoctorHistory;
use App\CreditDoctor;
use App\Duration;
use App\Schedule;
use App\ScheduleDoctor;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
class ScheduleController extends Controller {

//==========================================================================================

	// public function intialSchedualDoctors(){


	// 	$schedules = Schedule::orderBy('id','asc')->pluck('id')->toArray();
	// 	$durations = Duration::orderBy('id','asc')->pluck('id')->toArray();

	// 	foreach ($schedules as  $schedule) {
			
	// 		foreach ($durations as $duration) {
								
	// 			$DoctorSchedule = new ScheduleDoctor();
	// 			$DoctorSchedule->user_id = 30;
	// 			$DoctorSchedule->duration_id = $duration;
	// 			$DoctorSchedule->schedule_id = $schedule;
	// 			$DoctorSchedule->save();
	// 		}	

	// 	}

	// }

//==========================================================================================

	#get schedules without time now tosend notificatio to doctors
	public function get_doctor()
	{
		$day =Carbon::now()->format('l');
		$date =Carbon::now()->format('H:i:s');
		$schedule = Schedule::where('day', $day)->first();

		$duration=Duration::where(function($e) use($date) {$e->where('start_time',"<=",$date);$e->where('end_time',">=",$date);})->first();

		$schedules = ScheduleDoctor::where('duration_id', $duration->id)->where('schedule_id',$schedule->id)->get();


		return $schedules;
	}

	#get schedules with time now tosend notificatio to doctors
	public function get_doctor_by_schedule()
	{
		/*$day =Carbon::now()->format('l');
		$date =Carbon::now()->format('H:i:s');
		$schedules = Schedule::where('day', $day)->where(function($e) use($date) {$e->where('start_time',"<=",$date);$e->where('end_time',">=",$date);})->first();
		*/
		$day =Carbon::now()->format('l');
		$date =Carbon::now()->format('H:i:s');
		//dd($date);die();
		$schedule = Schedule::where('day', $day)->first();
		//dd($schedule);die();
		$duration=Duration::where(function($e) use($date) {$e->where('start_time',"<=",$date);$e->where('end_time',">=",$date);})->first();
		// dd($duration);die();
		$schedules = ScheduleDoctor::where('duration_id', $duration->id)->where('schedule_id',$schedule->id)->first();
		//dd($schedules);die();
		return $schedules;
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	

		//$From = Carbon::create($CarbonFormate[0],$CarbonFormate[1],$CarbonFormate[2],0)->format('d-m-Y');
		//$To = Carbon::create($CarbonFormate[0],$CarbonFormate[1],$CarbonFormate[2],0)->addDays(6)->format('d-m-Y');

		$weeks = ScheduleDoctorsHistory::orderBy('weekDate', 'asc')->distinct()->pluck('weekDate')->toArray();
		$NewWeeks = $this->HandelWeeksDateFormate();
		$schedules = Schedule::orderBy('id', 'asc')->distinct('day')->paginate(10);
		return view('schedules.index', compact('schedules','NewWeeks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('schedules.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$schedule = new Schedule();

		$schedule->day = $request->input("day");
        $schedule->day_code = $request->input("day_code");
        $schedule->start_time = $request->input("start_time");
        $schedule->end_time = $request->input("end_time");

		$schedule->save();

		return redirect()->route('schedules.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$schedule = Schedule::findOrFail($id);

		return view('schedules.show', compact('schedule'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$schedule = Schedule::findOrFail($id);
		$schedules = ScheduleDoctor::where('schedule_id',$schedule->id)->orderBy('duration_id','asc')->get();

		return view('schedules.edit', compact('schedule','schedules'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{

		$new_doctor_ids = $request->input("doctor");
		$schedules = ScheduleDoctor::where('schedule_id',$id)->orderBy('duration_id','asc')->get();

		for ($i=0; $i < count($schedules); $i++) { 

			$schedules[$i]->user_id = $new_doctor_ids[$i];
			$schedules[$i]->save();
		}
			
		return redirect()->route('schedules.index')->with('message', 'schedule updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$schedule = Schedule::findOrFail($id);
		$schedule->delete();

		return redirect()->route('schedules.index')->with('message', 'Item deleted successfully.');
	}
//=========================================<< Fawzy Functions >>==================================================

	public function scheduleHistory(){

		$startweekDate = Carbon::now()->subDays(6)->format('Y-m-d');
		$today =Carbon::now()->format('Y-m-d');
		$schedules = ScheduleDoctor::get();
		//$sub = Carbon::now()->subDays(2)->format('Y-m-d');
		//$add = Carbon::now()->addDays(2)->format('Y-m-d'); // 
			foreach ($schedules as $schedule ) {

				$scheduleHistory = new ScheduleDoctorsHistory();
				$scheduleHistory->user_id = $schedule->user_id;
				$scheduleHistory->duration_id = $schedule->duration_id;
				$scheduleHistory->sort = $schedule->sort;
				$scheduleHistory->schedule_id = $schedule->schedule_id;
				$scheduleHistory->startweekDate = $startweekDate;
				$scheduleHistory->weekDate = $today;
				$scheduleHistory->save();
			
			}

	}
//===============================================================================================================
	public function creditHistory(){

		$today =Carbon::now()->format('Y-m-d');
		$credits = CreditDoctor::get();
		//$sub = Carbon::now()->subDays(2)->format('Y-m-d');
		//$add = Carbon::now()->addDays(2)->format('Y-m-d');
			foreach ($credits as $credit ) {

				$CreditDoctorHistory = new CreditDoctorHistory();
				$CreditDoctorHistory->user_id = $credit->user_id;
				$CreditDoctorHistory->calls_credit = $credit->calls_credit;
				$CreditDoctorHistory->weekDate = $today;
				$CreditDoctorHistory->save();
			}

		CreditDoctor::truncate();	
	}	
//===============================================================================================================
	public function getScheduleHistory($WeekDateHistory)
	{	
		$CarbonFormate = explode("-",$WeekDateHistory);
		$From = Carbon::create($CarbonFormate[2],$CarbonFormate[1],$CarbonFormate[0],0)->format('d-m-Y');
		$To   = Carbon::create($CarbonFormate[2],$CarbonFormate[1],$CarbonFormate[0],0)->addDays(6)->format('d-m-Y');
		$WeekDateHistory = Carbon::create($CarbonFormate[2],$CarbonFormate[1],$CarbonFormate[0],0)->addDays(6)->format('Y-m-d');
		$ScheduleDoctorsHistory = ScheduleDoctorsHistory::orderBy('schedule_id', 'asc')->where('weekDate',$WeekDateHistory)->get();
		$weeks = ScheduleDoctorsHistory::orderBy('weekDate', 'asc')->distinct()->pluck('weekDate')->toArray();
		$NewWeeks = $this->HandelWeeksDateFormate();
		$schedules = Schedule::orderBy('id', 'asc')->distinct('day')->paginate(10);
		return view('schedules.history', compact('schedules','ScheduleDoctorsHistory' , 'NewWeeks' , 'From' , 'To'));

	}

//===============================================================================================================
	public function HandelWeeksDateFormate()
	{		
		$weeks = ScheduleDoctorsHistory::orderBy('weekDate', 'asc')->distinct()->pluck('weekDate')->toArray();
		$NewWeeks = array();
			foreach ($weeks as $key => $week) {

				$WeekDate = explode("-",$week);
				$NewWeeks[$key] = Carbon::create($WeekDate[0],$WeekDate[1],$WeekDate[2],0)->subDays(6)->format('d-m-Y');
			}
		return $NewWeeks;
	}		
//===============================================================================================================

}
