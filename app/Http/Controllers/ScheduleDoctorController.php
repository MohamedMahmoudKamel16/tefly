<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use App\ScheduleDoctor;
use App\Duration;
use App\Schedule;

class ScheduleDoctorController extends Controller
{
    //get doctors id by schedule id
    public function get_doctor_schedule($id,$duration)
    {
    	$doctors = ScheduleDoctor::where('schedule_id',$id)->where('duration_id',$duration)->pluck('user_id')->toArray();
    //	dd($doctors);die();
    	return $doctors;
    }


    #get schedule by doctor
	public function get_doctor_schedules(Request $request)
	{
	
		$admin = (new AuthController)->get_user();
    	$schedules = ScheduleDoctor::where('user_id',$admin->id)->selectRaw('schedule_id')->groupBy('schedule_id')->having('schedule_id', '>', 0)->get();
        $FinalResult = array();


    	foreach ($schedules as $schedule_id) {

        	$schedulesids = ScheduleDoctor::where('user_id',$admin->id)->where('schedule_id',$schedule_id->schedule_id)->pluck('duration_id')->toArray();
            $day =Schedule::where('id',$schedule_id->schedule_id)->value('day');
            $durations =Duration::whereIn('id',$schedulesids)->get();
            $Result = array('schedule_id'=> (int)$schedule_id->schedule_id , 'day' => $day , 'durations' => $durations );

            array_push($FinalResult , $Result );
            $Result = array();

    	}

    	if(count($schedules)>0)
		{

    		$statusCode = 200;
    		$response  = array('msg' => 'Done! ','schedules'=>$FinalResult);

		}else{
		
            $statusCode = 201;
    		$response  = array('msg' => 'no data! ','schedules'=>$FinalResult);
		}
			
		return response()->json($response, $statusCode);
	}
//====================================================================================================================

}
