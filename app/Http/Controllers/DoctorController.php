<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\CreditDoctor;
use App\Doctor;
use App\ScheduleDoctor;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\ScheduleDoctorsHistory;
use App\Schedule;
use App\CreditDoctorHistory;
use App\OrderDoctor;
use App\Order;
use App\Duration;
use App\Patient;
use App\Feedback;
use App\UserPackage;
use DB;
use File;
class DoctorController extends Controller {

//=========================<< Fawzy edits >>>==============================================
	public function dashboard(){

		$AvgDoctorRate  = Order::avg('rate');
		$numPatient 	= Patient::count();
		$numDoctors 	= Doctor::count();
		$numRequests 	= Order::count();
		$numFeedbacks 	= Feedback::count();
		$numFinished   	= Order::where('status' , 3 )->count();
		$numCanceledByPatient  = Order::where('status' , 4 )->count();
		$numNotRespondByDoctor = Order::where('status' , 2 )->count();
		$numMintues 	= Order::sum('duration_call');
		$maxCallDocID 	= DB::table("order_doctors")
					    ->select("user_id" , DB::raw("COUNT(*) as count_count"))
					    ->groupBy("user_id")
					    ->where('status' , 3)
					    ->orderBy('count_count', 'DESC')
					    ->get();

		$maxCallDocName  = User::where('id' , $maxCallDocID[0]->user_id )->value("name");
		$minCallDocName  = User::where('id' , $maxCallDocID[count($maxCallDocID) - 1]->user_id  )->value("name");
		$users 			 = User::whereDate('created_at', DB::raw('CURDATE()'))->count();

		$DailySubscribe  = UserPackage::where('package_id' , 2)->whereMonth('created_at', date('m'))->distinct()->count(['user_id']);
		$MonthlySubscribe= UserPackage::where('package_id' , 1)->whereMonth('created_at', date('m'))->distinct()->count(['user_id']);

		return view('dash', compact('AvgDoctorRate','numPatient' , 'numDoctors' , 'numRequests' , 'numFeedbacks' , 'numMintues' , 'numFinished' , 'numCanceledByPatient' , 'numNotRespondByDoctor' , 'maxCallDocName' , 'minCallDocName' , 'DailySubscribe' , 'MonthlySubscribe'));
	}
//========================================================================================
	public function AjaxBlock($id){

		$User = User::where('id' , $id)->first();

		if ($User->is_block == 0) {

			$User->is_block = 1;
			$User->save();
			echo '<i class="fa fa-lock" aria-hidden="true"></i>';

		}elseif ($User->is_block == 1) {

			$User->is_block = 0;
			$User->save();
			echo '<i class="fa fa-unlock-alt" aria-hidden="true"></i>';
		}

	}
//====================================================================================================
	public function GetDoctorCredit($user_id){

		$ShiftsCount = ScheduleDoctor::where('user_id',$user_id)->count();
		$Calls = CreditDoctor::where('user_id',$user_id)->first();

		if (empty($ShiftsCount)) {

			$ShiftsCount = 0;
		}

		if (empty($Calls)) {

			$calls_credit = 0;
		}else{
			$calls_credit = $Calls->calls_credit;
		}

		$TotalCredit = $calls_credit + ($ShiftsCount * 20); #shift is 20 LE
		echo "Total Credit is ".$TotalCredit;

	}

//===========================================================================================


	#get doctor not in schedule
	public function get_doctors_not($id,$duration)
	{
		$doctors = ScheduleDoctor::where('schedule_id',$id)->where('duration_id',$duration)->pluck('user_id')->toArray();


			$doctors = User::whereNotIn('id',$doctors)->where('type',1)->pluck('id')->toArray();
			return $doctors;

	}

//===================================<< fawzy edits >>==========================================
	#get cities by lang id and country
	public function get_doctors(Request $request)
	{
		if(! isset($request->take)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->offset)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}elseif(! isset($request->user_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	        return response()->json($response, $statusCode);

		}else{

			$patient       = User::where('id', $request->user_id)->first();
			$doctors       = User::where('type',1)->skip($request->offset)->take($request->take)->get();
			$order         = Order::where('user_id' , $request->user_id )->orderBy("created_at" , "desc")->first();

			foreach ($doctors as $user) {

				$doctor = Doctor::where('user_id',$user->id)->first();

				if($doctor)
				{
					$user->bio = $doctor->bio;
			        $user->title = $doctor->title;
			        $user->website_link = $doctor->website_link;
			        $user->clinic_address = $doctor->clinic_address;
			        $user->clinic_phone = $doctor->clinic_phone;

				}else{
					$user->bio = "";
			        $user->title = "";
			        $user->website_link = "";
			        $user->clinic_address = "";
			        $user->clinic_phone = "";
				}


			}

			$response = [
				'msg' => 'Done!',
				'user_status' => (int) $patient->user_status,
				'doctors' => $doctors
			];

			if ($order && ($order->status == 7 || $order->status == 0)) { // user made an order and doctor finished marked it as finished
				$response['order_id'] = (int) $order->id;
			}

			if ($patient->user_status == 1 && $order && $order->status == 1) { // user made an order and the order is accepted
				$doctor_id = OrderDoctor::where('order_id', $order->id)->where('is_accept', 1)->first()->user_id;
				$doctor = User::find($doctor_id);
				$response['doctor_name'] = $doctor->name;
				$response['doctor_num'] = Doctor::where('user_id', $doctor->id)->first()->contact_phone;
				$response['order_id'] = (int) $order->id;
			}



			return response()->json($response, 200);
		}
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$doctors = Doctor::orderBy('id', 'desc')->get();
		return view('doctors.index', compact('doctors'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('doctors.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{

		$this->validate($request, [
	        'mobile' => 'required|unique:users|max:255',
	        'email' => 'required|unique:users|max:255',
	        'name' => 'required',
    	]);


		DB::beginTransaction();
    	try{
	   		$user = new User();
	        $user->name = $request->input("name");
	        $user->password = $request->input("password");
	        $user->email = $request->input("email");
	        $user->mobile = $request->input("mobile");
	        $user->type = 1;
	        $user->is_block = 0;

	        if($request->file("image") != "")
	       	{
	   			$extension = File::extension($request->file("image")->getClientOriginalName());
	   			$image =time().".".$extension;
	   			$user->image=$image;
				$request->file('image')->move(public_path('images/users/'), $image );


			}else
			{
				$user->image="";
			}
	   		$user->save();

	   		$doctor = new Doctor();
	        $doctor->user_id = $user->id;
	        $doctor->password = $request->input("password");
	        $doctor->title = $request->input("title");
	        $doctor->bio = $request->input("bio");
	        $doctor->website_link = $request->input("website_link");
	        $doctor->clinic_phone = $request->input("clinic_phone");
	        $doctor->contact_phone = $request->input("contact_phone");
	        $doctor->clinic_address = $request->input("clinic_address");

	        #

			$doctor->save();
			DB::commit();
			return redirect()->route('doctors.index')->with('message', 'Item created successfully.');
        }catch (Exception $e) {
		    DB::rollback();
		    return redirect()->route('doctors.index')->with('message', 'There are some problem.');
		}



	}
//==================================================================================================================================================================
	public function SomeDocDetails($id){

		$doctor 		    = Doctor::findOrFail($id);
		$user               = User::findOrFail($doctor->user_id);
		$DoctorOrdersIds    = OrderDoctor::where('user_id',$user->id)->pluck('order_id')->toArray();
		$DoctorRate         = Order::whereIn('id' , $DoctorOrdersIds )->where('status',3)->avg('rate');

		echo round( $DoctorRate );

	}
//==================================================================================================================================================================
	public function show($id)
	{
		$doctor 		  = Doctor::findOrFail($id);
		$user             = User::findOrFail($doctor->user_id);
		$DoctorOrdersIds  = OrderDoctor::where('user_id',$user->id)->pluck('order_id')->toArray();
		$DoctorRate       = Order::whereIn('id' , $DoctorOrdersIds )->where('status',3)->avg('rate');

		$TotalCallsCreditHistory = CreditDoctorHistory::where('user_id' , $user->id)->sum('calls_credit');
		$CurrentCallsCredit = CreditDoctor::where('user_id' , $user->id)->sum('calls_credit');
		$TotalCallsCredit = $TotalCallsCreditHistory + $CurrentCallsCredit;

		$ScheduleDoctorsHistoryCredit = 0;
		$ScheduleDoctorsHistoryCredit = ScheduleDoctorsHistory::where('user_id',$user->id)->count();
		$ScheduleDoctorsHistoryCredit = $ScheduleDoctorsHistoryCredit * 10;


        $schedules = ScheduleDoctor::where('user_id',$user->id)->selectRaw('schedule_id')->groupBy('schedule_id')->having('schedule_id', '>', 0)->orderBy('schedule_id', 'ASC')->get();
        $FinalResults = array();


    	foreach ($schedules as $schedule_id) {

        	$schedulesids = ScheduleDoctor::where('user_id',$user->id)->where('schedule_id',$schedule_id->schedule_id)->pluck('duration_id')->toArray();
            $day =Schedule::where('id',$schedule_id->schedule_id)->value('day');
            $durations =Duration::whereIn('id',$schedulesids)->get();
            $Result = array('schedule_id'=> (int)$schedule_id->schedule_id , 'day' => $day , 'durations' => $durations );

            array_push($FinalResults , $Result );
            $Result = array();

    	}

		return view('doctors.show', compact('doctor','user','DoctorRate' , 'TotalCallsCredit' , 'ScheduleDoctorsHistoryCredit' , 'FinalResults') );
	}
//==================================================================================================================================================================
	public function showScheduleHistory($doctor_id , $startweekDate)
	{

		if ( $startweekDate == 0) {

			$startweekDate   = ScheduleDoctorsHistory::orderBy('startweekDate', 'asc')->distinct()->value("startweekDate");
		}else{

			$startweekDateArray = explode("-",$startweekDate);
			$startweekDate = Carbon::create($startweekDateArray[2],$startweekDateArray[1],$startweekDateArray[0],0)->format('Y-m-d');
		}

		$user      = User::findOrFail($doctor_id);
        $schedules = ScheduleDoctorsHistory::where('user_id',$user->id)->where('startweekDate', $startweekDate)->selectRaw('schedule_id')->groupBy('schedule_id')->having('schedule_id', '>', 0)->orderBy('schedule_id', 'ASC')->get();
        $FinalResults = array();

    	foreach ($schedules as $schedule_id) {

        	$schedulesids = ScheduleDoctorsHistory::where('user_id',$user->id)->where('startweekDate', $startweekDate)->where('schedule_id',$schedule_id->schedule_id)->pluck('duration_id')->toArray();
            $day =Schedule::where('id',$schedule_id->schedule_id)->value('day');
            $durations =Duration::whereIn('id',$schedulesids)->get();
            $Result = array('schedule_id'=> (int)$schedule_id->schedule_id , 'day' => $day , 'durations' => $durations );

            array_push($FinalResults , $Result );
            $Result = array();

    	}

		$From = $startweekDate;
		$To   = ScheduleDoctorsHistory::where('startweekDate' ,$startweekDate)->orderBy('startweekDate', 'asc')->distinct()->value("weekDate");
		$NewWeeks = $this->HandelWeeksDateFormate();

		return view('doctors.scheduleHistory', compact('user','FinalResults' , 'NewWeeks' , 'From' , 'To') );
	}
//==================================================================================================================================================================
	public function HandelWeeksDateFormate()
	{
		$weeks = ScheduleDoctorsHistory::orderBy('startweekDate', 'asc')->distinct()->pluck('startweekDate')->toArray();
		$NewWeeks = array();
			foreach ($weeks as $key => $week) {

				$WeekDate = explode("-",$week);
				$NewWeeks[$key] = Carbon::create($WeekDate[0],$WeekDate[1],$WeekDate[2],0)->format('d-m-Y');
			}
		return $NewWeeks;
	}
//==================================================================================================================================================================

	public function previousCalls($doctor_id)
	{

		$userDetails      = User::findOrFail($doctor_id);
		$DoctorOrdersIds  = OrderDoctor::where('user_id',$doctor_id)->pluck('order_id')->toArray();
		$orders       	  = Order::whereIn('id' , $DoctorOrdersIds )->orderBy('id', 'desc')->get();
		$user 		      = $userDetails->id;
		//$orders = Order::orderBy('id', 'desc')->paginate(15);

		// return view('doctors.calls', compact('user','orders'));
		return view('orders.index', compact('user','orders'));
	}

//==================================================================================================================================================================

	public function previousCallsDetails($order_id)
	{
		$order = Order::findOrFail($order_id);

		// return view( 'doctors.callDetails', compact('order') );
		return view('orders.show', compact('order'));
	}

//==================================================================================================================================================================
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$doctor = Doctor::findOrFail($id);

		return view('doctors.edit', compact('doctor'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$this->validate($request, [
	        'mobile' => 'required|max:255|unique:users,mobile,'.$id,
	        'email' => 'required|max:255|unique:users,email,'.$id,
	        'name' => 'required',
    	]);


		DB::beginTransaction();
    	try{
	   		$user = User::where('id',$id)->first();
	        $user->name = $request->input("name");
	        $user->password = $request->input("password");
	        $user->email = $request->input("email");
	        $user->mobile = $request->input("mobile");

	        if($request->file("image") != "")
	       	{
	   			$extension = File::extension($request->file("image")->getClientOriginalName());
	   			$image =time().".".$extension;
	   			$user->image=$image;
				$request->file('image')->move(public_path('images/users/'), $image );


			}
	   		$user->save();

	   		$doctor = Doctor::where('user_id',$id)->first();
	   		if(!$doctor){
	   			$doctor = new Doctor();
	   		}
	        $doctor->user_id = $user->id;
	        $doctor->password = $request->input("password");
	        $doctor->title = $request->input("title");
	        $doctor->bio = $request->input("bio");

	        $doctor->website_link = $request->input("website_link");
	        $doctor->clinic_phone = $request->input("clinic_phone");
	        $doctor->contact_phone = $request->input("contact_phone");
	        $doctor->clinic_address = $request->input("clinic_address");

			$doctor->save();
			DB::commit();
			return redirect()->route('doctors.index')->with('message', 'Item created successfully.');
        }catch (Exception $e) {
		    DB::rollback();
		    return redirect()->route('doctors.index')->with('message', 'There are some problem.');
		}

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$user_id = Doctor::where('id' , $id)->value("user_id");
		$User = User::findOrFail($user_id);
		$User->delete();

		return redirect()->route('doctors.index')->with('message', 'Item deleted successfully.');
	}
//===========================================================================================================

	public function totalCreditHistory($doctor_id){


		$user = User::findOrFail($doctor_id);

		$credit_calls = CreditDoctorHistory::where('user_id' , $user->id)->groupBy('weekDate')->orderBy('weekDate', 'asc')->get();

		$credit_shifts = DB::table("schedule_doctors_history")
			    ->select("weekDate" , "startweekDate" , DB::raw("COUNT(*) as count_shifts"))
			    ->groupBy("weekDate")
			    ->where('user_id' , $user->id)
			    ->orderBy('weekDate', 'asc')
			    ->get();

		//dd($credit_shifts[0]->count_shifts);die();

		return view('doctors.history', compact('credit_calls','credit_shifts','user'));
	}

//===========================================================================================================

}
