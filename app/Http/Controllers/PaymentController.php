<?php namespace App\Http\Controllers;

use App\Payment;
use App\User;
use Illuminate\Http\Request;
use ACME\Services\PaymentService;

class PaymentController extends Controller
{

	/**
	 * [$catalogs description]
	 * @var [type]
	 */
	private $catalogs = [
		'60201' => 'Orange',
		'60202' => 'Vodafone',
		'60203' => 'Etisalat',
	];


	/**
	 * [create_payment description]
	 * @return [type] [description]
	 */
	public function create_payment(Request $request)
	{
		$mobile_number = $request->get('mobile');
		$type = $request->get('type');
		
		$user = User::where('mobile', $mobile_number)->first();
		
		if(! $user) {
			return 'Invalid request.';
		}
		
		return view('payment.create_payment', compact('mobile_number', 'type'));
	}

	/**
	 * [store_payment description]
	 * @param  Request        $request        [description]
	 * @param  PaymentService $paymentService [description]
	 * @return [type]                         [description]
	 */
	public function store_payment(Request $request, PaymentService $paymentService)
	{
		$data = $request->except('_token');
		
		$user = User::where('mobile', $data['mobile_number'])->first();
		
		if(! $user){
			return 'Invalid request.';
		}
		
		$data['orderInfo'] = str_random(10);
		$data['productCatalogName'] = $this->catalogs[$data['operatorCode']];

		$payment = Payment::create([
			'user_id'       => $user->id,
			'order_id'      => $data['orderInfo'],
			'mobile_number' => $data['msisdn'],
			'provider'      => $data['operatorCode'],
			'package'       => $data['productId']
		]);

		$paymentService->generateSignature($data, 'initialize');
		$response = json_decode($paymentService->initialize());

		if ($response->operationStatusCode == 10) { // success
			$payment->update([
				'transaction_id' => $response->transactionId
			]);

			return redirect()->route('get.confirm.code', ['order_id' => $payment->order_id]);
		}

		return redirect()->back();
	}

	/**
	 * [confirm_code description]
	 * @param  [type] $order_id [description]
	 * @return [type]           [description]
	 */
	public function confirm_code($order_id)
	{
		$payment = Payment::where('order_id', $order_id)->first();

		if (! $payment) {
			return 'Invalid request.';
		}

		return view('payment.confirm_code', compact('order_id'));
	}


	/**
	 * [finish_payment description]
	 * @param  Request $request  [description]
	 * @param  [type]  $order_id [description]
	 * @return [type]            [description]
	 */
	public function finish_payment(Request $request, $order_id, PaymentService $paymentService)
	{
		$data    = $request->except('_token');
		$payment = Payment::where('order_id', $order_id)->first();

		if (! $payment) {
			return 'Invalid request';
		}

		$data['transactionId'] = $payment->transaction_id;

		$paymentService->generateSignature($data, 'confirm');

		$response = json_decode($paymentService->confirm());

		if ($response->operationStatusCode == 0) { // success
			$payment->update([
				'status' => 'paid'
			]);

			$tokens = (new UserDeviceController)->get_tokens_by_ids([$payment->user_id]);
			$title   = "Payment Success";
			$message = "لقد تمت عملية الدفع بنجاح";
			$type = 10;
			
			//return ['user_id' => $payment->user_id, 'tokens' => $tokens];

			$notification = (new NotificationController)->send_notification($tokens, $title, $message, $type, []);

			return;
		}

		return redirect()->back();
	}
}
