<?php

namespace App\Http\Controllers;
use App\User;
use App\UserDevice;
use Illuminate\Http\Request;

class UserDeviceController extends Controller
{

	public function index($userType)
	{
		if ($userType == 3) {

			$users = User::where("is_block",0)->pluck("id")->toArray();
		
		}else{

			$users = User::where("is_block",0)->where("type" , $userType)->pluck("id")->toArray();
		}

		if(count($users)>0){
		$user_devices = UserDevice::whereIn('user_id', $users)->where("is_active",1)->pluck("register_id")->toArray();
		}
		return $user_devices;
	}
	
	public function get_tokens_by_id($id , $userType)
	{
	
		if ($userType == 3) {

			$users = User::where("id",$id)->where("is_block",0)->first();

		}else{

			$users = User::where("id",$id)->where("is_block",0)->where("type" , $userType)->first();
		}

		$user_devices =array();
		if(count($users)>0){
		$user_devices = UserDevice::where("user_id",$id)->where("is_active",1)->pluck("register_id")->toArray();
		}

		return $user_devices;
	}

	public function get_tokens_by_ids($ids)
	{
		$users = User::whereIn("id",$ids)->where("is_block",0)->first();
		 $user_devices =array();
		if(count($users)>0){
			$user_devices = UserDevice::whereIn("user_id",$ids)->where("is_active",1)->pluck("register_id")->toArray();
		}

		return $user_devices;
	}

	public function get_tokens_by_type($type , $userType)
	{
		// $users = User::where("id",$id)->where("is_block",0)->first();

		if ($userType == 3) {

			$user_ids = User::where("is_block",0)->pluck("id")->toArray();

		}else{

			$user_ids = User::where("is_block",0)->where("type" , $userType)->pluck("id")->toArray();
		}


		$user_devices =array();
		if(count($user_ids)>0){

			$user_devices = UserDevice::whereIn("user_id", $user_ids)->where('platform',$type)->where("is_active",1)->pluck("register_id")->toArray();
		}

		return $user_devices;
	}
	public function refresh_register_id(Request $request){
		if(! isset($request->old_register_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	                return response()->json($response, $statusCode);		


		}elseif(! isset($request->register_id)){
			$statusCode = 420;
			$response  = array('msg' => 'Invalid Format! ');
	                return response()->json($response, $statusCode);		


		}else{
		
			$admin = (new AuthController)->get_user();

			$check_device=UserDevice::where('register_id',$request->old_register_id)->first();
			if(count($check_device)>0){
				$check_device->user_id=$admin->id;
				$check_device->register_id=$request->register_id;
				$check_device->save();
			}
			$result=array("msg"=>"Done");
			return response()->json($result,200);
			
		}
		
	}
}
