<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Package;
use Illuminate\Http\Request;

class PackageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$packages = Package::orderBy('id', 'desc')->paginate(10);

		return view('packages.index', compact('packages'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('packages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$package = new Package();

		$package->name = $request->input("name");
        $package->type = $request->input("type");
        $package->price = $request->input("price");
        $package->no_calls = $request->input("no_calls");
        $package->duration = $request->input("duration");

		$package->save();

		return redirect()->route('packages.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$package = Package::findOrFail($id);

		return view('packages.show', compact('package'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$package = Package::findOrFail($id);

		return view('packages.edit', compact('package'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$package = Package::findOrFail($id);

		$package->name = $request->input("name");
        $package->type = $request->input("type");
        $package->price = $request->input("price");
        $package->no_calls = $request->input("no_calls");
        $package->duration = $request->input("duration");

		$package->save();

		return redirect()->route('packages.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$package = Package::findOrFail($id);
		$package->delete();

		return redirect()->route('packages.index')->with('message', 'Item deleted successfully.');
	}

}
