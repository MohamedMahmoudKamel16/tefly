<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleDoctorsHistory extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'schedule_doctors_history';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'duration_id' , 'sort', 'schedule_id', 'startweekDate' ,'weekDate'
    ];

    protected $appends =['time','user','day'];
    public function getTimeAttribute()
    {

       return Duration::where('id',$this->duration_id)->first();
    }

    public function getDayAttribute()
    {

       return Schedule::where('id',$this->schedule_id)->orderBy('id', 'asc')->first();
    }

    public function getUserAttribute()
    {

       return User::where('id',$this->user_id)->first();
    }

}
