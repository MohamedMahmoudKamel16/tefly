<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use App\OrderDoctor;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\UserDeviceController;
use App\Http\Controllers\NotificationController;

#
class SendNotification implements ShouldQueue
{


    use InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    protected $name;
//=========================================================================================================================
    public function __construct($id , $name)
    {
        //
        $this->id   = $id;
        $this->name = $name;

    }

//=========================================================================================================================
    public function handle()
    {

      // $order = Order::where('id' , 974)->first();
      // $order->kid_name = $this->id; //$this->id
      // $order->save();

        $order = Order::where('id',$this->id)->first();
        if ($order->status == 0) {
            #after make order we will send notification to doctor on schedule
            $doctors_ids = (new DoctorController)->get_doctors_not($order->schedule_id,$order->duration_id);

            foreach ($doctors_ids as $doctor_id) {

                $order_doctor = OrderDoctor::where('user_id',$doctor_id)->where('order_id',$order->id)->first();

                if(count($order_doctor)<1){
                    $order_doctor = new OrderDoctor();
                    $order_doctor->user_id=$doctor_id;
                    $order_doctor->order_id=$order->id;
                    #create order to two doctors
                    $order_doctor->status=0;
                    $order_doctor->save();
                }

            }

            $order->deliveryStatus = "all";
            $order->save();
            #get tokens register id 
            $tokens = (new UserDeviceController)->get_tokens_by_ids($doctors_ids);
            $patientName = $this->name;
            #send notification to doctor on sheft
			$title   = "new_request";
			$message = "{$patientName}_requested_a_consultation_from_you";
			$type = 2;
            //$notification = (new NotificationController)->send_notification($tokens,$title,$message,$type,$order);

			foreach ($tokens as $key => $token) {

				file_get_contents("http://e7gezlyapp.com/tefli/send_notification_queue/".$token."/".$title."/".$message."/2");
			}


        }

    }
//=========================================================================================================================


}
