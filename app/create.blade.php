@extends('layouts.admin')
@section('css')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
    <div class="page-header">
        <h3><i class="glyphicon glyphicon-plus"></i> Schedules / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('schedules.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('day')) has-error @endif">
                       <label class="col-sm-2 control-label" for="day-field">Day</label>
                    <input type="text" id="day-field" name="day" class="form-control" value="{{ old("day") }}"/>
                       @if($errors->has("day"))
                        <span class="help-block">{{ $errors->first("day") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('day_code')) has-error @endif">
                       <label class="col-sm-2 control-label" for="day_code-field">Day_code</label>
                    <input type="text" id="day_code-field" name="day_code" class="form-control" value="{{ old("day_code") }}"/>
                       @if($errors->has("day_code"))
                        <span class="help-block">{{ $errors->first("day_code") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('start_time')) has-error @endif">
                       <label class="col-sm-2 control-label" for="start_time-field">Start_time</label>
                    <input type="time" id="start_time-field" name="start_time" class="form-control" value="{{ old("start_time") }}"/>
                       @if($errors->has("start_time"))
                        <span class="help-block">{{ $errors->first("start_time") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('end_time')) has-error @endif">
                       <label class="col-sm-2 control-label" for="end_time-field">End_time</label>
                    <input type="time" id="end_time-field" name="end_time" class="form-control" value="{{ old("end_time") }}"/>
                       @if($errors->has("end_time"))
                        <span class="help-block">{{ $errors->first("end_time") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('schedules.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('scripts')
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
  <script>
    $('.date-picker').datepicker({
    });
  </script>
@endsection
