<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{


    protected $appends = array('doctor_1','doctor_2','doctors');

    public function getStartTimeAttribute($value)
    {

       return \Carbon\Carbon::parse($value)->format('H:i');
    }

    public function getEndTimeAttribute($value)
    {

       return \Carbon\Carbon::parse($value)->format('H:i');
    }
     public function getDoctor1Attribute($value)
    {

       return ScheduleDoctor::where('schedule_id',$this->id)->where('sort',1)->value('user_id');
    }

     public function getDoctor2Attribute($value)
    {

       return ScheduleDoctor::where('schedule_id',$this->id)->where('sort',2)->value('user_id');
    }

    public function getDoctorsAttribute($value)
    {
       return ScheduleDoctor::where('schedule_id',$this->id)->get();
    }



}
