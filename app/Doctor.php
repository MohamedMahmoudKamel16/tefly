<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    //
    protected $fillable = [
        'title', 'bio','user_id'
    ];

    protected $appends =['shifts'];

    public function info()
    {
        return   $this->belongsTo('App\User','user_id');
    }

    public function CreditDoctor()
    {
        return   $this->belongsTo('App\CreditDoctor','user_id','user_id');
    }

    public function getShiftsAttribute()
    {
       return ScheduleDoctor::where('user_id',$this->user_id)->get()->count();
    }


}
