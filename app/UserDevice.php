<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDevice extends Model
{
    //
    protected $fillable = [
        'user_id', 'register_id','device_id','is_active','platform','version'
    ];
}
