<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $primaryKey = 'id';
    protected $table = 'cities';
    protected $hidden = [ 'created_at', 'updated_at' , 'country_id'];


    public function Country()
    {
        return $this->belongsTo('App\Country' , 'country_id');
    }

}
