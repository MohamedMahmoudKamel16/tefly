<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('doctors_reports/{id}', function($id) {
  $doctor = \App\User::find($id);

  $doctor_orders  = $doctor->orders();
  $orders_clone_1 = clone $doctor_orders;
  $orders_clone_2 = clone $doctor_orders;
  $finished_orders   = $orders_clone_1->wherePivot('is_accept', 1);
  $unfinished_orders = $orders_clone_2->wherePivot('is_accept', 0);

  return [
    'total' => $doctor_orders->count(),
    'finished' => $finished_orders->count(),
    'unfinished' => $unfinished_orders->count(),
  ];
});


Route::get("executeQueue","OrderController@executeQueue");

Route::get('countries', 'CountryCityController@countries');
Route::post('cities', 'CountryCityController@cities');

Route::post('get_doctors', 'DoctorController@get_doctors');

Route::post("send_feedback","FeedbackController@send_feedback");
#force update
Route::get("force_update","AppSettingController@force_update");
Route::get("force_update_new", "AppSettingController@force_update_new");

Route::group(['prefix' => 'patient'], function () {
	#register
	Route::post('register', 'AuthController@register');
	#login
	Route::post('login', 'AuthController@login');

});

Route::group(['prefix' => 'doctor'], function () {
	#register
	Route::post('register', 'AuthController@register_doctor');
	#login
	Route::post('login', 'AuthController@login_doctor');

});

#refresh token
Route::post('refresh', 'AuthController@refresh_token');




#forget password
Route::post("forget_password","AuthController@forgetPassword");

#check if user login and autho
Route::group(['middleware' => ['jwt-auth']], function () {
	#accept_order
	Route::post("accept_order","OrderController@accept_order");

	#not_respond_order
	Route::post("not_respond_order","OrderController@not_respond_order");


	#rate_order
	Route::post("rate_order","OrderController@rate_order");

	#cancel_order
	Route::post("cancel_order","OrderController@cancel_order");

	#oncall_order
	Route::post("oncall_order","OrderController@oncall_order");

	#doctor_report_order
	Route::post("doctor_finish_order","OrderController@doctor_finish_order");
	Route::post("doctor_report_order","OrderController@doctor_report_order");



	#check_current_package
	Route::get('check_current_package', 'UserPackageController@check_current_package');

	#new request doctor
	Route::post("new_order","OrderController@new_order");


	#validate_promocode
	Route::post("validate_promocode","OrderController@validate_promocode");


	#logout
	Route::get('logout', 'AuthController@logout');

	Route::post("get_user_byid","AuthController@get_user_byid");

	Route::get("check_block","AuthController@check_block");

	//Route::post("user_update","AuthController@update_data");
	Route::post("update_password","AuthController@update_password");

	Route::group(['prefix' => 'patient'], function () {
		Route::put('version_update', 'AuthController@update_version');
		Route::post('user_update', 'AuthController@update_data');
		#get_patient_orders
		Route::post("get_patient_orders","OrderController@get_patient_orders");

	});
	Route::group(['prefix' => 'doctor'], function () {
		Route::post('user_update', 'AuthController@update_data_doctor');
		#get done order of doctor
		Route::post("get_doctor_orders","OrderController@get_doctor_orders");

		#get new order of doctor
		Route::post("get_doctor_new_orders","OrderController@get_doctor_new_orders");
		#get_doctor_schedules
		Route::post("get_doctor_schedules","ScheduleDoctorController@get_doctor_schedules");


	});


	#refresh_register_id
	Route::post("refresh_register_id","UserDeviceController@refresh_register_id");
	Route::post("test","UserDeviceController@test");

});


//============================================================================================
