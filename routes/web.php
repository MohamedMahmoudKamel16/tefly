<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('create_payment', 'PaymentController@create_payment');
Route::post('create_payment', 'PaymentController@store_payment');
Route::get('confirm_code/{order_id}', ['as' => 'get.confirm.code', 'uses' => 'PaymentController@confirm_code']);
Route::post('confirm_code/{order_id}', 'PaymentController@finish_payment');

Route::get('send_notification_queue/{token}/{title}/{message}/{type}',"NotificationController@send_notification_queue");
Route::post('notifications/store',"NotificationController@store");

Auth::routes();
Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource("schedules","ScheduleController");
	Route::resource("packages","PackageController");
	Route::resource("orders","OrderController");

	Route::resource("doctors","DoctorController");
	Route::resource("patients","PatientController");

	Route::resource("durations","DurationController");
	Route::get('/home', 'HomeController@index');

	Route::resource("days","DayController");
	Route::resource("feedback","FeedbackController");
	Route::resource("promo_codes","PromoCodeController");
	Route::resource("CountryCity","CountryCityController");

	Route::get('notifications/create', function () {
	    return view('notifications.create');
	});


	Route::get("searchCustomer/{key}","AuthController@search_users");

	//======================<< fawzy  >>=========================================
	Route::get('GetDoctorCredit/{user_id}',"DoctorController@GetDoctorCredit");
	Route::get('viewScheduleHistory/{week}',"ScheduleController@getScheduleHistory");
	//Route::get("intialSchedualDoctors","ScheduleController@intialSchedualDoctors");
	Route::get("showScheduleHistory/{doctor_id}/{startweekDate}","DoctorController@showScheduleHistory");

	Route::get("previousCalls/{doctor_id}","DoctorController@previousCalls");
	Route::get("previousCallsDetails/{order_id}","DoctorController@previousCallsDetails");
	Route::get("AjaxBlock/{key}","DoctorController@AjaxBlock");


	Route::get("patientpreviousCalls/{patient_id}","PatientController@previousCalls");
	Route::get("patientpreviousCallsDetails/{patient_id}","PatientController@previousCallsDetails");
	Route::get("patientAjaxBlock/{patient_id}","PatientController@AjaxBlock");
	Route::get("PackageHistory/{patient_id}","PatientController@PackageHistory");

	Route::get("EditPackageHistory/{package_id}","PatientController@EditPackageHistory");
	Route::post("storeEditPackageHistory","PatientController@storeEditPackageHistory");

	Route::get("filterOrders","OrderController@filterOrders");

	Route::get("AdminDashboard","DoctorController@dashboard");
	Route::get("/","DoctorController@dashboard");

});

//====================================<< History >>======================================
Route::get('scheduleHistory',"ScheduleController@scheduleHistory");
Route::get('creditHistory',"ScheduleController@creditHistory");
Route::get('totalCreditHistory/{doctor_id}',"DoctorController@totalCreditHistory");
