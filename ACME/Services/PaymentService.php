<?php namespace ACME\Services;


class PaymentService
{
  /**
   * The common URL all the fields are attached to.
   * @var [string]
   */
  private $baseUrl = 'http://staging.tpay.me/api/TPay.svc/json/';


  /**
   * [$publicKey description]
   * @var string
   */
  private $publicKey = 'cYuZL4tEKgBNuVQk77ga';


  /**
   * [$privateKey description]
   * @var string
   */
  private $privateKey = 'hi05h45kdiGqQ4p3R5JH';


  /**
   * Available parameters
   * @var [array]
   */
  private $parameters = [];


  /**
   * [$signature description]
   * @var string
   */
  private $signature = '';

  /**
   * Initialize payment parameters (must be in order)
   * @var Array
   */
  private $initialize = ['productCatalogName', 'productId', 'msisdn', 'operatorCode', 'orderInfo'];

  /**
   * * Confirm payment parameters (must be in order)
   * @var [type]
   */
  private $confirm    = ['transactionId', 'pinCode'];

  /**
   * Resend pin code payment parameters (must be in order)
   * @var array
   */
  private $resend     = ['transactionId'];


  /**
   * Set parameters.
   * @param array $parameters
   */
  public function setParameters($parameters)
  {
    $this->parameters = [];

    foreach ($parameters as $key => $value) {
      $this->setParameter($key, $value);
    }
  }


  /**
   * Set a single parameter.
   * @param string $key
   * @param string $value
   */
  private function setParameter($key, $value)
  {
    $this->parameters[$key] = $value;
  }


  /**
   * Get all parameters.
   * @return array
   */
  public function getParameters()
  {
    return $this->parameters;
  }


  /**
   * Get a single parameter.
   * @param  string $key
   * @return string
   */
  public function getParameter($key)
  {
    return $this->parameters[$key];
  }


  /**
   * Generate message for given fields.
   * @param  array $fields
   * @return string
   */
  public function generateMessage($fields)
  {
     $message = '';

    foreach ($fields as $field) {
      $message .= $this->getParameter($field);
    }

    return $message;
  }


  /**
   * Generate signature for given fields.
   * @param  array  $fileds
   * @return string
   */
  public function generateSignature($parameters, $action)
  {
    $this->setParameters($parameters);

    $message = $this->generateMessage($this->$action);

    $signature = $this->publicKey . ':' . hash_hmac('sha256', $message, $this->privateKey);

    $this->setParameter('signature', $signature);
  }


  /**
   * Make a request to the given end point.
   * @param  string $url
   */
  public function curlInit($url)
  {
    $ch = curl_init($this->baseUrl . $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->getParameters()));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json'
    ]);

    return curl_exec($ch);
  }


  /**
   * First api end point of tpay.
   */
  public function initialize()
  {
    return $this->curlInit('InitializeDirectPaymentTransaction');
  }


  /**
   * Second api end point of tpay.
   */
  public function resend()
  {
    return $this->curlInit('ResendVerificationPin');
  }


  /**
   * Third api end point of tpay.
   */
  public function confirm()
  {
    return $this->curlInit('ConfirmDirectPaymentTransaction');
  }

}

